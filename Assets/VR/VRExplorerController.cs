﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using BMJ.M360.Generic;

namespace BMJ.M360.VR {
	public class VRExplorerController : InstancedMonoBehaviour<VRExplorerController> {
		[SerializeField] Transform InteriorHolder;
		[SerializeField] GameObject LoadingProgressBarHolder;
		[SerializeField] Image LoadingProgressBar;

		List<string> InteriorRequestList = new List<string> ();

		public static void LoadInterior (string InteriorLink) {
			if (GetInstance == null) {
				Debug.Log ("VRExplorerController is null!");
			} else {
				GetInstance.ProcessLoadInterior (InteriorLink);
			}
		}

		void ProcessLoadInterior (string InteriorLink) {
			if (InteriorRequestList.Contains (InteriorLink)) {
			} else {
				InteriorRequestList.Add (InteriorLink);
				StartCoroutine (LoadInteriorRoutine (InteriorLink));
			}
		}

		IEnumerator LoadInteriorRoutine (string InteriorLink) {
			LoadingProgressBarHolder.gameObject.SetActive (true);

			LoadingProgressBar.fillAmount = 0.0f;

			UnityWebRequest LoadInteriorRequest = UnityWebRequestAssetBundle.GetAssetBundle (InteriorLink);

			yield return LoadInteriorRequest;

			while (!LoadInteriorRequest.isDone) {
				LoadingProgressBar.fillAmount = LoadInteriorRequest.downloadProgress;
				yield return null;
			}

			LoadingProgressBar.fillAmount = 1.0f;
			LoadingProgressBarHolder.SetActive (false);

			string TargetName = InteriorLink.Replace ("http://galaxybattles.deekie.com/artiz/", "");
			TargetName = TargetName.Replace (".assetbundle", "");

			if (LoadInteriorRequest.isNetworkError || LoadInteriorRequest.isHttpError) {
				Debug.Log (LoadInteriorRequest.error);
			} else {
				AssetBundle RequestedAssetBundle = DownloadHandlerAssetBundle.GetContent (LoadInteriorRequest);

				if (RequestedAssetBundle.Contains (TargetName)) {
					GameObject InteriorObject = Instantiate (RequestedAssetBundle.LoadAsset<GameObject> (TargetName));

					//InteriorObject.
				}
			}
				//

			//
		}

	}
}
