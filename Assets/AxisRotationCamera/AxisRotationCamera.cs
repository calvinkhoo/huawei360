﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

using BMJ.M360.Generic;
using BMJ.M360.Artiz;

namespace BMJ.M360.CustomCamera
{
    public class AxisRotationCamera : InstancedMonoBehaviour<AxisRotationCamera>
    {
        [Header("Camera")]
        [SerializeField] float MobileRotationMultiplier;
        [SerializeField] Camera MainCamera;
        [SerializeField] Camera AttachedCamera;
        [SerializeField] GameObject GyroSupportErrorBox;

        bool GyroSupport = false;

        private bool InCategory = false;

        [SerializeField] float HorizontalSpeed = 5.0f;
        [SerializeField] float VerticalSpeed = 5.0f;

        [SerializeField] float CameraYaw = 0.0f;
        [SerializeField] float CameraPitch = 0.0f;

        [SerializeField] float YawClamp = 50.0f;

        [SerializeField] float YClamp = 0.4f;

        [SerializeField] Transform Marker;

        [SerializeField] Transform CameraTransform;

        [SerializeField] Transform LookSphere;
        [SerializeField] Transform LookTarget;

        float LookTargetYCache = 0.0f;
        float LookTargetDistance = 0.5f;
        Vector3 LookTargetPoint = Vector3.zero;

        [SerializeField] Transform TapSphere;

        float TapTargetDistance = 0.5f;
        float TapYPosition = 0.0f;

        Quaternion TapRotationCache = Quaternion.identity;
        Vector3 TapDelta = Vector3.zero;

        [SerializeField] Transform GyroSphere;

        float GyroTargetDistance = 0.5f;
        float GyroYPosition = 0.0f;

        Quaternion GyroRotationCache = Quaternion.identity;
        Vector3 GyroDelta = Vector3.zero;

        public static Camera GetAxisRotationCamera
        {
            get
            {
                if (GetInstance == null)
                {
                    return null;
                }
                else
                {
                    return GetInstance.AttachedCamera;
                }
            }
        }

        float DefaultFOV = 60.0f;

        public static float GetDefaultFOV
        {
            get
            {
                if (GetInstance == null)
                {
                    return 0.0f;
                }
                else
                {
                    return GetInstance.DefaultFOV;
                }
            }
        }

        public static void ResetCameraRotation()
        {
            if (GetInstance == null)
            {
                Debug.LogError("AxisRotationCamera == null");
            }
            else
            {
                GetInstance.ProcessResetCameraRotation();
            }
        }

        void ProcessResetCameraRotation()
        {
            Debug.Log("Reset Camera Rotation");
            CameraYaw = 0.0f;
            CameraPitch = 0.0f;

            LookSphere.transform.rotation = Quaternion.identity;
            GyroSphere.transform.rotation = Quaternion.identity;
            TapSphere.transform.rotation = Quaternion.identity;

            GyroRotationCache = Quaternion.identity;
            TapRotationCache = Quaternion.identity;

            GyroDelta = Vector3.zero;
            TapDelta = Vector3.zero;

            LookTargetPoint = LookTarget.position;

            transform.rotation = Quaternion.identity;
            AttachedCamera.transform.localRotation = Quaternion.identity;
        }

        [Header("VR Control")]
        [SerializeField] GameObject Layout;
        [SerializeField] GameObject CardboardLayout;
        [SerializeField] GameObject VRSplash;
        [SerializeField] GameObject Dot;
        [SerializeField] GameObject VRRaycaster;

        string CurrentXRDevice = string.Empty;

        [Header("Crosshair")]
        [SerializeField] GameObject CrosshairObject;
        [SerializeField] Image CrosshairFace;
        [SerializeField] Image CrosshairRing;
        [SerializeField] Text CrosshairCountdownUI;
        [SerializeField] Color ActiveColor;
        [SerializeField] Color InactiveColor;

        bool CrosshairSupported = false;

        public static bool SetCrosshairSupport
        {
            set
            {
                if (GetInstance == null)
                {
                }
                else
                {
                    GetInstance.CrosshairSupported = value;
                }
            }
        }

        [Header("Loading")]
        [SerializeField] GameObject LoadingObject;
        [SerializeField] Image LoadingFace;
        [SerializeField] Text LoadingProgress;

        float RotationMultiplier = 1.0f;
        bool Tap = false;

        Coroutine ActiveVRRoutine = null;

        [Header("Scroll Rect")]
        [SerializeField] ScrollRect Scroll;
        [SerializeField] ScrollRect Scroll1;
        [SerializeField] ScrollRect Scroll2;

        public void ResetScrollRect(ScrollRect scrollRect)
        {
            scrollRect.horizontalNormalizedPosition = 0;
        }

        enum VRState
        {
            NONE,
            TRANSITIONING,
            VR
        }

        VRState CurrentVRState = VRState.NONE;
        VRState TargetVRState = VRState.NONE;

        public static bool ToggleVRMode
        {
            set
            {
                if (GetInstance == null)
                {
                }
                else
                {
                    GetInstance.ProcessToggleVRMode = value;
                }
            }
        }

        public bool ProcessToggleVRMode
        {
            set
            {
                GyroSupport = SystemInfo.supportsGyroscope;
                if (GyroSupport)
                {
                    switch (CurrentVRState)
                    {
                        case VRState.NONE:
                            if (value)
                            {
                                TargetVRState = VRState.TRANSITIONING;
                            }
                            break;
                        case VRState.TRANSITIONING:
                            break;
                        case VRState.VR:
                            if (value)
                            {
                            }
                            else
                            {
                                TargetVRState = VRState.NONE;
                            }
                            break;
                    }
                }
                else
                {
                    XRSettings.LoadDeviceByName("none");

                    GyroSupportErrorBox.SetActive(true);
                }
            }
        }

        void Start()
        {
            GyroSupport = false;
            StartCoroutine(MonitorVRStatusRoutine());
        }

        Coroutine ActiveCardboardRoutine = null;

        IEnumerator MonitorVRStatusRoutine()
        {
            while (gameObject)
            {
                if (TargetVRState.Equals(CurrentVRState))
                {
                    switch (CurrentVRState)
                    {
                        case VRState.NONE:
                            CurrentXRDevice = "none";
                            //VRRaycaster.SetActive(false);
                            //if (ActiveCardboardRoutine == null)
                            //{
                            //}
                            //else
                            //{
                            //    StopCoroutine(ActiveCardboardRoutine);
                            //    ActiveCardboardRoutine = null;
                            //}
                            break;

                        case VRState.TRANSITIONING:
                            if (Screen.orientation.Equals(ScreenOrientation.LandscapeLeft) || Screen.orientation.Equals(ScreenOrientation.LandscapeRight))
                            {
                                Screen.autorotateToLandscapeLeft = true;
                                Screen.autorotateToLandscapeRight = true;
                                Screen.autorotateToPortrait = false;
                                Screen.autorotateToPortraitUpsideDown = false;
                                XRSettings.LoadDeviceByName("cardboard");
                                VRSplash.SetActive(false);
                                TargetVRState = VRState.VR;
                                yield return null;
                                CurrentXRDevice = XRSettings.loadedDeviceName;
                                XRSettings.enabled = true;
                            }
                            else
                            {
                                yield return null;
                            }
                            break;

                        case VRState.VR:
                            //if (ActiveCardboardRoutine == null)
                            //{
                            //    ActiveCardboardRoutine = StartCoroutine(VRRoutine());
                            //}

                            Input.gyro.enabled = true;
                            if (XRSettings.loadedDeviceName.ToLowerInvariant().Equals("none"))
                            {
                                TargetVRState = VRState.NONE;
                            }
                            break;
                    }
                }
                else
                {
                    switch (TargetVRState)
                    {
                        case VRState.NONE:
                            Screen.autorotateToLandscapeLeft = true;
                            Screen.autorotateToLandscapeRight = true;
                            Screen.autorotateToPortrait = true;
                            Screen.autorotateToPortraitUpsideDown = true;
                            Layout.transform.localScale = Vector3.one;
                            CardboardLayout.transform.localScale = vector;
                            XRSettings.LoadDeviceByName("none");
                            yield return null;
                            XRSettings.enabled = false;
                            Dot.SetActive(false);
                            VRRaycaster.SetActive(false);
                            break;

                        case VRState.TRANSITIONING:
                            VRSplash.SetActive(true);
                            Layout.transform.localScale = vector;
                            CardboardLayout.transform.localScale = Vector3.one;
                            Screen.autorotateToLandscapeLeft = true;
                            Screen.autorotateToLandscapeRight = true;
                            Screen.autorotateToPortrait = true;
                            Screen.autorotateToPortraitUpsideDown = true;
                            Dot.SetActive(true);
                            VRRaycaster.SetActive(true);

                            Input.gyro.enabled = true;
                            if (XRSettings.loadedDeviceName.ToLowerInvariant().Equals("none"))
                            {
                                TargetVRState = VRState.NONE;
                            }
                            break;

                        case VRState.VR:
                            break;
                    }
                    CurrentVRState = TargetVRState;
                }
                yield return null;
            }
        }

        Vector3 vector = new Vector3(0.0001f, 0.0001f, 0.0001f);

        public void ToCardboard()
        {
            Dot.SetActive(true);
            VRRaycaster.SetActive(true);
        }

        IEnumerator VRRoutine()
        {
            Ray VRRayCast = new Ray();
            RaycastHit raycastHit;
            float timestamp = 0.0f;
            bool buttonhit = false;
            GameObject lastHitObject = null;

            while (CurrentVRState == VRState.VR)
            {
                VRRayCast = Camera.main.ViewportPointToRay(Vector3.one * 0.5f);
                Debug.Log("Raycasting");
                if (Physics.Raycast(VRRayCast, out raycastHit, 5000f))
                {
                    if (raycastHit.collider.gameObject.Equals(lastHitObject))
                    {

                    }
                    else
                    {
                        if (raycastHit.collider.gameObject.GetComponent<Button>() == null)
                        {

                        }
                        else
                        {
                            if (buttonhit)
                            {
                                if (Time.realtimeSinceStartup < timestamp + 3.0f)
                                {
                                }
                                else
                                {
                                    lastHitObject = raycastHit.collider.gameObject;
                                    buttonhit = false;
                                    raycastHit.collider.gameObject.GetComponent<Button>().onClick.Invoke();
                                }
                            }
                            else
                            {
                                timestamp = Time.realtimeSinceStartup;
                                buttonhit = true;
                            }
                        }
                    }
                }
                else
                {
                    Debug.Log("Nothing hit");
                }
                yield return null;
            }
        }

        /*
		IEnumerator VRRoutine (bool Toggle) {
			if (GyroSupport) {
				VRSplash.SetActive (Toggle);

				if (Toggle) {
					float Timestamp = Time.realtimeSinceStartup + 3.0f;

					while (Time.realtimeSinceStartup < Timestamp) {
						VRSplashLoading.fillAmount = (Timestamp - Time.realtimeSinceStartup) / 3.0f;
						yield return null;
					}

					VRSplash.SetActive (false);
				}

				Layout.transform.localScale = Vector3.one * (Toggle.Equals (true) ? 0.0f : 1.0f);

				if (Toggle) {
					while (Screen.orientation != ScreenOrientation.LandscapeLeft && Screen.orientation != ScreenOrientation.LandscapeRight) {
						Screen.orientation = ScreenOrientation.LandscapeLeft;

						yield return null;
					}

					Screen.autorotateToLandscapeLeft = Toggle;
					Screen.autorotateToLandscapeRight = false;
					Screen.autorotateToPortrait = !Toggle;
					Screen.autorotateToPortraitUpsideDown = false;
				} else {
					while (Screen.orientation != ScreenOrientation.Portrait && Screen.orientation != ScreenOrientation.PortraitUpsideDown) {
						Screen.orientation = ScreenOrientation.Portrait;

						yield return null;
					}

					Screen.autorotateToLandscapeLeft = true;
					Screen.autorotateToLandscapeRight = true;
					Screen.autorotateToPortrait = true;
					Screen.autorotateToPortraitUpsideDown = true;
				}

				XRSettings.LoadDeviceByName (Toggle.Equals (true) ? "cardboard" : "none");

				yield return null;

				XRSettings.enabled = Toggle;
				CurrentXRDevice = XRSettings.loadedDeviceName;
			} else {
				XRSettings.LoadDeviceByName ("none");

				GyroSupportErrorBox.SetActive (true);

				yield return null;
			}

			ActiveVRRoutine = null;
		}
		*/

        public static bool GetLoadingStatus
        {
            get
            {
                if (GetInstance == null)
                {
                    Debug.LogError("AxisRotationCamera == null");
                    return false;
                }
                else
                {
                    return GetInstance.LoadingObject.activeSelf;
                }
            }
        }

        public static void UpdateLoading(bool Toggle, float Progress = 0.0f)
        {
            if (GetInstance == null)
            {
                Debug.LogError("AxisRotationCamera == null");
            }
            else
            {
                GetInstance.ProcessUpdateLoading(Toggle, Progress);
            }
        }

        void ProcessUpdateLoading(bool Toggle, float Progress)
        {
            LoadingObject.SetActive(Toggle);
            if (Toggle)
            {
                LoadingProgress.text = (Progress * 100.0f).ToString("00") + "%";
                LoadingFace.fillAmount = Progress;
            }
        }

        public static bool ToggleCrosshair
        {
            set
            {
                //print ("AxisRotationCamera.ToggleCrosshair called [" + value + "]");
                if (GetInstance == null)
                {
                    //print ("AxisRotationCamera = null");
                }
                else
                {
                    if (value)
                    {
                        if (GetInstance.CrosshairSupported)
                        {
                            GetInstance.CrosshairObject.SetActive(value);
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        GetInstance.CrosshairObject.SetActive(value);
                    }
                }
            }
        }

        public static bool IsCrosshairActive
        {
            get
            {
                if (GetInstance == null)
                {
                    Debug.LogError("AxisRotationCamera == null");
                    return false;
                }
                else
                {
                    return GetInstance.CrosshairObject.activeSelf;
                }
            }
        }

        public static float AdjustCrosshairRing
        {
            set
            {
                if (GetInstance == null)
                {
                    Debug.LogError("AxisRotationCamera == null");
                }
                else
                {
                    GetInstance.CrosshairRing.fillAmount = Mathf.Clamp(value, 0.0f, 1.0f);
                    GetInstance.CrosshairCountdownUI.text = (Mathf.Clamp(value, 0.0f, 1.0f) * 2.0f).ToString("0.0");
                }
            }
        }

        public static bool AdjustCrosshairColor
        {
            set
            {
                if (GetInstance == null)
                {
                    Debug.LogError("AxisRotationCamera == null");
                }
                else
                {
                    GetInstance.CrosshairFace.color = value.Equals(true) ? GetInstance.ActiveColor : GetInstance.InactiveColor;
                    GetInstance.CrosshairCountdownUI.gameObject.SetActive(value);
                }
            }
        }

        public void GyroTrue()
        {
            GyroSupport = true;
        }

        public void GyroFalse()
        {
            GyroSupport = false;
        }

        public void CategoryTrue()
        {
            InCategory = true;
        }

        public void CategoryFalse()
        {
            InCategory = false;
        }

        protected override void OnAwake()
        {
#if !UNITY_EDITOR
			RotationMultiplier = MobileRotationMultiplier;
			GyroSupport = Input.isGyroAvailable;
#endif
            DefaultFOV = AttachedCamera.fieldOfView;
            CrosshairFace.color = InactiveColor;

            Marker.gameObject.SetActive(false);
            //LookTargetDistance = Vector3.Distance (LookTarget.position, LookSphere.position);
        }

        bool tapCorrection = false;

        void Update()
        {
            //print ("GyroSupport : " + GyroSupport);
            if (GyroSupport && (string.IsNullOrEmpty(XRSettings.loadedDeviceName) || XRSettings.loadedDeviceName.Equals("none")))
            {
                Input.gyro.enabled = true;

                GyroSphere.rotation = Input.gyro.attitude;
                GyroSphere.Rotate(0f, 0f, 180f, Space.Self);
                GyroSphere.Rotate(90f, 180f, 0f, Space.World);

                GyroDelta = GyroRotationCache.eulerAngles - GyroSphere.eulerAngles;
                GyroRotationCache = GyroSphere.rotation;
            }
            else
            {
                Input.gyro.enabled = false;
            }

            if (Input.GetMouseButton(0))
            {
                print("GET");
                if (InCategory == true)
                {
                    if (Camera.main.ScreenToViewportPoint(Input.mousePosition).y > 0.2f)
                    {
                        if (tapCorrection)
                        {
                            if (Tap)
                            {
                                CameraYaw -= HorizontalSpeed * Input.GetAxis("Mouse X") * RotationMultiplier;
                                CameraPitch = Mathf.Clamp(CameraPitch + (VerticalSpeed * Input.GetAxis("Mouse Y") * RotationMultiplier), Mathf.Abs(YawClamp) * -1.0f, Mathf.Abs(YawClamp));

                                TapSphere.rotation = Quaternion.Euler(new Vector3(CameraPitch, CameraYaw, 0.0f));

                                TapDelta = TapRotationCache.eulerAngles - TapSphere.eulerAngles;
                                TapRotationCache = TapSphere.rotation;
                            }
                            else
                            {
                                Tap = (Mathf.Abs(Input.GetAxis("Mouse X")) > 0.0f) && (Mathf.Abs(Input.GetAxis("Mouse Y")) > 0.0f);
                            }
                        }
                        else
                        {
                            if (Input.GetAxis("Mouse X") > 10 && Input.GetAxis("Mouse Y") > 10)
                            {

                            }
                            else
                            {
                                tapCorrection = true;
                            }
                        }
                    }
                    else
                    {
                        Tap = false;
                        tapCorrection = false;
                    }
                }
                else
                {
                    Tap = false;
                    tapCorrection = false;
                }
            }
            else
            {
                tapCorrection = false;
            }

            LookSphere.eulerAngles = LookSphere.eulerAngles - GyroDelta - TapDelta;
            GyroDelta = Vector3.zero;
            TapDelta = Vector3.zero;

            LookTargetPoint = LookTarget.position;

            /*
			LookTargetPoint.y = Mathf.Clamp (LookTargetPoint.y, LookTarget.position.y - YClamp, LookTarget.position.y + YClamp);
			LookTargetYCache = LookTargetPoint.y;
			LookTargetPoint = (LookTargetPoint - (LookSphere.position + (Vector3.up * LookTargetYCache))).normalized * LookTargetDistance;
			LookTargetPoint.y = LookTargetYCache;

			Marker.position = LookTargetPoint;
			*/

            CameraTransform.LookAt(LookTargetPoint, Vector3.up);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (XRSettings.loadedDeviceName.Equals("none"))
                {

                }
                else
                {
                    TargetVRState = VRState.NONE;
                    GyroSupport = false;
                }
            }

            if (string.IsNullOrEmpty(CurrentXRDevice) || CurrentXRDevice.Equals("none"))
            {
            }
            else
            {
                AttachedCamera.transform.localRotation = Quaternion.identity;
                transform.eulerAngles = MainCamera.transform.eulerAngles;
            }
        }
    }
}
