﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using BMJ.M360.Generic;
using BMJ.M360.CustomCamera;

namespace BMJ.M360.Artiz {
	public class ToggleViewController : InstancedMonoBehaviour<ToggleViewController> {
		[SerializeField] CustomFitScroller AttachedCustomFitScroller;
		[SerializeField] GameObject ToggleDownButtonObject;
		[SerializeField] Button ToggleUpButton;
		[SerializeField] GameObject VRModeButton;
		[SerializeField] AxisRotationCamera AttachedAxisRotationCamera;

		void Start () {
			VRModeButton.SetActive (false);
		}

		public static void ManualToggleOff () {
			if (GetInstance == null) {
			} else {
				GetInstance.Toggle = true;
			}
		}

		public bool Toggle {
			set {
				AttachedCustomFitScroller.Toggle = value;
				ToggleDownButtonObject.SetActive (!value);
				ToggleUpButton.interactable = value;
				AttachedAxisRotationCamera.enabled = value;
				AxisRotationCamera.ToggleCrosshair = value;
				VRModeButton.SetActive (value);
			}
		}
	}
}