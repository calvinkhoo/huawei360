﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace BMJ.M360.Generic {
	public class InstancedMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour {
		static T Instance;

		public static void ClearInstance () {
			Instance = null;
		}

		protected static T SetInstance {
			set {
				Instance = value;
			}
		}

		protected static T GetInstance {
			get {
				if (Instance == null) {
					print ("Instance of <" + typeof (T).ToString () + "> is null!");
					return null;
				} else {
					return Instance;
				}
			}
		}

		void Awake () {
			if (Instance == null) {
				Instance = gameObject.GetComponent<T> ();
				OnAwake ();
			} else {
				Destroy (this);
			}
		}

		protected virtual void OnAwake () {
			
		}

		void OnDestroy () {
			Instance = null;
		}

		public static bool Exists {
			get {
				return !(Instance == null);
			}
		}
	}
}