﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace BMJ.M360.Artiz {
	[System.Serializable]
	public class ArtizPreviewProperty {
		public string UniqueID = "UniqueID";
		public string DisplayName = "DisplayName";
		public string Description = "Description";
		public List<string> SpriteThumbnailLink = new List<string> () { "SpriteThumbnailLink" };
		public int StartIndex = 0;
		public string SpriteHDRILink = "SpriteHDRILink";
		public int CategoryIndex = 0;
		public Vector3 HDRIRotationOffset = Vector3.zero;
	}
}
