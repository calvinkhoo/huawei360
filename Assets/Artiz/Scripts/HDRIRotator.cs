﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.M360.Artiz {
	public class HDRIRotator : MonoBehaviour {
		[SerializeField] Transform PrimarySphere;
		[SerializeField] Transform FinalTargetPoint;
		[SerializeField] Transform CurrentTargetPoint;
		[SerializeField] float RotationRate;

		Vector3 MovementVector = Vector3.zero;
		Vector3 StartPoint = Vector3.zero;

		void Start () {
			StartCoroutine (UpdateRoutine ());
		}

		IEnumerator UpdateRoutine () {
			while (gameObject) {
				if (Input.GetMouseButtonDown (0)) {
					StartPoint = Camera.main.ScreenToViewportPoint (Input.mousePosition);
				}

				if (Input.GetMouseButtonUp (0)) {
				}

				if (Input.GetMouseButton (0)) {
					MovementVector = StartPoint - Camera.main.ScreenToViewportPoint (Input.mousePosition);
					//CurrentTargetPoint.localPosition 
				}

				CurrentTargetPoint.position = Vector3.MoveTowards (CurrentTargetPoint.position, FinalTargetPoint.position, RotationRate);
				PrimarySphere.LookAt (CurrentTargetPoint);
				yield return null;
			}
		}

		IEnumerator RotateCamera () {
			bool Rotate = true;
			while (Rotate) {
				yield return null;
			}
		}
	}
}
