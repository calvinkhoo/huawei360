﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace BMJ.M360.Artiz {
	public class CustomFitScroller : MonoBehaviour {
		[SerializeField] RectTransform TargetRectTransform;

		[SerializeField] RectTransform StartRectTransform;
		[SerializeField] RectTransform EndRectTransform;

		Vector2 TargetAnchor = Vector2.zero;

		[SerializeField] [Range (100.0f, 1000.0f)] float Step = 500.0f;

		bool Initial = true;

		void Start () {
			if (TargetRectTransform == null) {
				Debug.LogError ("TargetRectTransform == null");
				return;
			}

			if (StartRectTransform == null) {
				Debug.LogError ("StartRectTransform == null");
				return;
			}

			if (EndRectTransform == null) {
				Debug.LogError ("EndRectTransform == null");
				return;
			}

			TargetAnchor = StartRectTransform.anchoredPosition;

			StartCoroutine (UpdateRoutine ());
		}

		public bool Toggle {
			set {
				if (Initial) {
					Initial = false;

					EndRectTransform.anchoredPosition = StartRectTransform.anchoredPosition + (0.575f * (Vector2.down * (Mathf.Abs (StartRectTransform.rect.yMin) + Mathf.Abs (StartRectTransform.rect.yMax))));
				}

				if (value) {
					TargetAnchor = EndRectTransform.anchoredPosition;
				} else {
					TargetAnchor = StartRectTransform.anchoredPosition;
				}
			}
		}

		IEnumerator UpdateRoutine () {
			while (gameObject) {
				TargetRectTransform.anchoredPosition = Vector2.MoveTowards (TargetRectTransform.anchoredPosition, TargetAnchor, Step * Time.deltaTime);
				yield return null;
			}
		}
	}
}
