﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

using BMJ.M360.Generic;
using BMJ.M360.Artiz.Scroller;
using System;

namespace BMJ.M360.Artiz {
	public class AssetsManager : InstancedMonoBehaviour<AssetsManager> {
		[SerializeField] GameObject SplashCanvas;
        [SerializeField] GameObject HomePage;
		[SerializeField] bool EditorClearCache = false;

		List<ArtizPreviewProperty> PreviewPropertyList = new List<ArtizPreviewProperty> ();

		[System.Serializable]
		public class SerializablePreviewProperties {
			public int Version;
			public List<ArtizPreviewProperty> PreviewPropertyList;
		}

		SerializablePreviewProperties SerializablePreviewPropertiesObject = new SerializablePreviewProperties ();

		Coroutine ActiveRoutine = null;
        Coroutine ActiveRoutine2 = null;

        string ConfigurationRequestURL = string.Empty;

		readonly string AssetsVersion = "AssetsVersion";

		void Start () {
#if UNITY_ANDROID
            //ConfigurationRequestURL = "http://galaxybattles.deekie.com/artiz/android/ArtizAssetConfiguration.txt";
            ConfigurationRequestURL = "http://galaxybattles.deekie.com/huaweiVR/android/huaweiConfiguration.txt";
#elif UNITY_IOS
            //ConfigurationRequestURL = "http://galaxybattles.deekie.com/artiz/ios/ArtizAssetConfiguration.txt";
            ConfigurationRequestURL = "http://galaxybattles.deekie.com/huaweiVR/ios/huaweiConfiguration.txt";
#endif

#if UNITY_EDITOR
            if (EditorClearCache) {
				Caching.ClearCache ();
			}
#endif
			SplashCanvas.SetActive (true);

			Resources.UnloadUnusedAssets ();

			if (PreviewButtonsManager.Exists) {
				LoadPreviewButtons (
					(SetArtizPreviewProperty) => {
						PreviewButtonsManager.PreloadButton (SetArtizPreviewProperty);
					},
					() => {
						int Result = ScrollingController.Initialize;

						if (Result == 200) 
                        {
                        } 
                        else {
							Debug.LogError ("ScrollingController.Initialize [" + Result + "]");
						}
					}
				);
			} else {
				Debug.LogError ("PreviewButtonsManager == null");
			}

            if (PreviewButtonsManager2.Exists)
            {
                LoadPreviewButtons2(
                    (SetArtizPreviewProperty) => {
                        PreviewButtonsManager2.PreloadButton(SetArtizPreviewProperty);
                    },
                    () => {
                        int Result2 = ScrollingController2.Initialize;

                        if (Result2 == 200)
                        {
                        }
                        else
                        {
                            Debug.LogError("ScrollingController.Initialize [" + Result2 + "]");
                        }
                    }
                );
            }
            else
            {
                Debug.LogError("PreviewButtonsManager2 == null");
            }
        }

		IEnumerator DebugUpdateRoutine () {
			float Timestamp = Time.realtimeSinceStartup;
			while (true) {
				if (Time.realtimeSinceStartup > Timestamp + 3.0f) {
					Timestamp = Time.realtimeSinceStartup;
					Debug.Log (Timestamp);
				}
				yield return null;
			}
		}

		public static void InitialPreviewAction () {
			if (GetInstance == null) {
			} else {
				GetInstance.ProcessInitialPreviewAction ();
			}
		}

        void ProcessInitialPreviewAction() {
            Invoke("DelayedProcessInitialPreviewAction", 1.0f);
        }

        void DelayedProcessInitialPreviewAction()
        {
            SplashCanvas.SetActive(false);
            HomePage.SetActive(true);
        }

        public static void InterHDRITraverse (string TargetHDRI, int SetCustomStartIndex) {
			ArtizPreviewButton CachedArtizPreviewButton = null;
			bool Match = false;
			foreach (ArtizPreviewButton ParsedPreviewButton in FindObjectsOfType<ArtizPreviewButton> ()) {
				if (ParsedPreviewButton.GetUniqueID.Equals (TargetHDRI)) {
					print ("Checking : " + ParsedPreviewButton.GetUniqueID + " - " + TargetHDRI);
					if (ParsedPreviewButton.StartIndex.Equals (SetCustomStartIndex)) {
						CachedArtizPreviewButton = ParsedPreviewButton;
						Match = true;
						print ("Matched CachedArtizPreviewButton Assign : " + CachedArtizPreviewButton);
						break;
					}

					if (!Match && ParsedPreviewButton.StartIndex.Equals (0)) {
						CachedArtizPreviewButton = ParsedPreviewButton;
						print ("Unmatched CachedArtizPreviewButton Assign : " + CachedArtizPreviewButton);
					}
				}
			}
			PreviewManager.CustomStartIndex = Match ? SetCustomStartIndex : 0;
			Debug.Log ("Setting CustomStartIndex : " + PreviewManager.CustomStartIndex);
			CachedArtizPreviewButton.Click ();
		}

		void LoadPreviewButtons (UnityAction<ArtizPreviewProperty> PreloadButtonAction, UnityAction LoadPreviewAssetsAction) {
			if (ActiveRoutine == null) {
				ActiveRoutine = StartCoroutine (LoadPreviewButtonsRoutine (PreloadButtonAction, LoadPreviewAssetsAction));
                Debug.Log("Loaded1");
			} else {
				Debug.LogError ("!ActiveRoutine == null");
			}
		}

        void LoadPreviewButtons2(UnityAction<ArtizPreviewProperty> PreloadButtonAction, UnityAction LoadPreviewAssetsAction)
        {
            if (ActiveRoutine2 == null)
            {
                ActiveRoutine2 = StartCoroutine(LoadPreviewButtonsRoutine(PreloadButtonAction, LoadPreviewAssetsAction));
                Debug.Log("Loaded2");
            }
            else
            {
                Debug.LogError("!ActiveRoutine2 == null");
            }
        }

        IEnumerator LoadPreviewButtonsRoutine (UnityAction<ArtizPreviewProperty> PreloadButtonAction, UnityAction LoadPreviewAssetsAction) {
			UnityWebRequest GetAssetConfigurationRequest = UnityWebRequest.Get (ConfigurationRequestURL);

			yield return GetAssetConfigurationRequest.SendWebRequest ();

			if (GetAssetConfigurationRequest.isNetworkError || GetAssetConfigurationRequest.isHttpError) {
				Debug.Log (GetAssetConfigurationRequest.error);
			} else {
				Debug.Log ("WWW.Response : " + GetAssetConfigurationRequest.downloadHandler.text);
				int UpdatedVersion = JsonUtility.FromJson<SerializablePreviewProperties> (GetAssetConfigurationRequest.downloadHandler.text).Version;

				if (UpdatedVersion > PlayerPrefs.GetInt (AssetsVersion, 0)) {
					if (Caching.ClearCache ()) {
						Debug.Log ("ClearCache (), updating to latest assets");
						PlayerPrefs.SetInt (AssetsVersion, UpdatedVersion);
						PlayerPrefs.Save ();
					} else {
						Debug.LogError ("ClearCache () failed");
					}
				} else {
					Debug.Log ("Assets up to date. [" + UpdatedVersion + "]");
				}

				PreviewPropertyList = JsonUtility.FromJson<SerializablePreviewProperties> (GetAssetConfigurationRequest.downloadHandler.text).PreviewPropertyList;

				foreach (ArtizPreviewProperty PreviewProperty in GetInstance.PreviewPropertyList) {
					PreloadButtonAction.Invoke (PreviewProperty);
				}

				LoadPreviewAssetsAction.Invoke ();
			}
		}

		public static int GetAssetVersion {
			get {
				return GetInstance == null ? 0 : PlayerPrefs.GetInt (GetInstance.AssetsVersion, 0);
			}
		}
	}
}
