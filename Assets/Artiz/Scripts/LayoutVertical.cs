﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace BMJ.M360.Artiz {
	[ExecuteInEditMode]
	public class LayoutVertical : MonoBehaviour {
		[SerializeField] float Spacing = 0.0f;

		RectTransform SourceTransform = null;
		RectTransform TargetTransform = null;

		Vector3 LocalPosition = Vector3.zero;
		bool SetupComplete = false;

		Transform ChildTransform = null;
		LayoutVertical ChildLayoutVertical = null;

		bool DestroyFlag = false;

		public float GetOverallHeight {
			get {
				return SourceTransform.sizeDelta.y + Spacing - TargetTransform.localPosition.y;
			}
		}

		void Start () {
			SourceTransform = null;
			TargetTransform = null;

			LocalPosition = Vector3.zero;
			SetupComplete = false;

			ChildTransform = null;
			ChildLayoutVertical = null;

			OnRectTransformDimensionsChange ();
		}

		public void Setup () {
			OnRectTransformDimensionsChange ();
		}

		void UpdateLayout () {
			if (SourceTransform == null || TargetTransform == null) {
				SetupComplete = false;
				if (SourceTransform == null) {
					if (transform.GetSiblingIndex () > 0) {
						SourceTransform = transform.parent.GetChild (transform.GetSiblingIndex () - 1).GetComponent<RectTransform>();
					}
				}

				if (TargetTransform == null) {
					TargetTransform = transform.GetComponent<RectTransform> ();
				}
			} else {
				SetupComplete = true;
			}

			if (SetupComplete) {
				LocalPosition = TargetTransform.localPosition;
				LocalPosition.y = SourceTransform.localPosition.y - SourceTransform.sizeDelta.y - Spacing;

				TargetTransform.localPosition = LocalPosition;

				OnRectTransformDimensionsChange ();
			}
		}

		void OnRectTransformDimensionsChange () {
			if (ChildTransform == null) {
				if (transform.parent == null) {
				} else {
					if (transform.GetSiblingIndex () + 1 < transform.parent.childCount) {
						ChildTransform = transform.parent.GetChild (transform.GetSiblingIndex () + 1);
						OnRectTransformDimensionsChange ();
					}
				}
			} else {
				if (ChildLayoutVertical == null) {
					ChildLayoutVertical = ChildTransform.GetComponent<LayoutVertical> ();

					if (ChildLayoutVertical == null) {
						Debug.LogError ("ChildLayoutVertical : null");
					} else {
						OnRectTransformDimensionsChange ();
					}
				} else {
					ChildLayoutVertical.UpdateLayout ();
				}
			}
		}
	}
}
