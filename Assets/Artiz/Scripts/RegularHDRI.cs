﻿using System;

using UnityEngine;

using BMJ.M360.CustomCamera;

namespace BMJ.M360.Artiz {
	[RequireComponent (typeof(MeshRenderer))]
	public class RegularHDRI : MonoBehaviour {
		void Start () {
			AxisRotationCamera.SetCrosshairSupport = false;
			AxisRotationCamera.ToggleCrosshair = false;

			AxisRotationCamera.ResetCameraRotation ();

			//Debug.Log ("RegularHDRI [" + gameObject.name + "]");
			MeshRenderer HDRISphere = GetComponent<MeshRenderer> ();
			HDRISphere.material.shader = PreviewManager.GetCubemapShader;//Shader.Find (HDRISphere.material.shader.name);

			//Debug.Log ("Shader [" + HDRISphere.material.shader.name + "]");
		}
	}
}
