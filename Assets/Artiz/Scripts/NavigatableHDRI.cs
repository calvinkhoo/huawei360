﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using BMJ.M360.CustomCamera;

namespace BMJ.M360.Artiz {
	public class NavigatableHDRI : MonoBehaviour {
		[SerializeField] List<MeshRenderer> HDRISpheres;
		[SerializeField] float SpeedModifier = 1.0f;
		[SerializeField] int StartIndex = 0;
		[SerializeField] Texture2D ActiveArrowTexture;
		[SerializeField] Texture2D InactiveArrowTexture;

		int CurrentIndex = 0;
		int TargetIndex = 0;

		void Start () {
			AxisRotationCamera.SetCrosshairSupport = true;
			AxisRotationCamera.ToggleCrosshair = true;
			//Debug.Log ("NavigatableHDRI [" + gameObject.name + "]");

			TargetIndex = ((PreviewManager.CustomStartIndex >= 0) && (PreviewManager.CustomStartIndex < HDRISpheres.Count)) ? PreviewManager.CustomStartIndex : StartIndex;

			Debug.Log ("PreviewManager.AccessCustomStartIndex : " + PreviewManager.CustomStartIndex);
			Debug.Log ("Target Index : " + TargetIndex);

			if (CurrentIndex.Equals (TargetIndex)) {
				CurrentIndex++;
			}

			foreach (MeshRenderer ParsedRenderer in HDRISpheres) {
				ParsedRenderer.material.shader = PreviewManager.GetCubemapShader;//Shader.Find (ParsedRenderer.material.shader.name);
				foreach (MeshRenderer ChildParsedRenderer in ParsedRenderer.gameObject.GetComponentsInChildren<MeshRenderer> ()) {
					if (ChildParsedRenderer.Equals (ParsedRenderer)) {
					} else {
						ChildParsedRenderer.material.shader = PreviewManager.GetUnlitTransparentShader;
					}
				}
			}

			for (int a = 0; a < HDRISpheres.Count; a++) {
				HDRISpheres [a].gameObject.SetActive (a.Equals (TargetIndex));
			}

			StartCoroutine (UpdateRoutine ());
		}

		public void Traverse (int OriginIndex, int DestinationIndex) {
			CurrentIndex = OriginIndex;
			TargetIndex = DestinationIndex;

			for (int a = 0; a < HDRISpheres.Count; a++) {
				if (a.Equals (OriginIndex) || a.Equals (DestinationIndex)) {
					HDRISpheres [a].gameObject.SetActive (true);
				} else {
					HDRISpheres [a].gameObject.SetActive (false);
				}
			}

			foreach (Transform ParsedNavigationButton in HDRISpheres [CurrentIndex].transform) {
				ParsedNavigationButton.gameObject.SetActive (false);
			}

			HDRISpheres [CurrentIndex].material.SetFloat ("_Exposure", 0.5f);
			HDRISpheres [TargetIndex].material.SetFloat ("_Exposure", 0.0f);
		}

		IEnumerator UpdateRoutine () {
			bool NavigationButtonHit = false;
			float CountdownTimestamp = 0.0f;
			float CurrentTimestamp = 0.0f;

			Ray Raycast;
			RaycastHit HitInfo;
			NavigationButton HitNavigationButton = null;

			while (gameObject) {
				HDRISpheres [CurrentIndex].material.SetFloat ("_Exposure", Mathf.MoveTowards (HDRISpheres [CurrentIndex].material.GetFloat ("_Exposure"), 0.0f, Time.deltaTime * SpeedModifier));
				HDRISpheres [TargetIndex].material.SetFloat ("_Exposure", Mathf.MoveTowards (HDRISpheres [TargetIndex].material.GetFloat ("_Exposure"), 0.5f, Time.deltaTime * SpeedModifier));

				if (HDRISpheres [CurrentIndex].material.GetFloat ("_Exposure") > 0.25f) {
					HDRISpheres [CurrentIndex].enabled = true;
					HDRISpheres [TargetIndex].enabled = false;

					if (AxisRotationCamera.GetAxisRotationCamera == null) {
					} else {
						AxisRotationCamera.GetAxisRotationCamera.fieldOfView = Mathf.Lerp (AxisRotationCamera.GetDefaultFOV * 0.3f, AxisRotationCamera.GetDefaultFOV, HDRISpheres [CurrentIndex].material.GetFloat ("_Exposure") * 2.0f);
					}
				} else {
					HDRISpheres [CurrentIndex].enabled = false;
					HDRISpheres [TargetIndex].enabled = true;

					if (AxisRotationCamera.GetAxisRotationCamera == null) {
					} else {
						AxisRotationCamera.GetAxisRotationCamera.fieldOfView = Mathf.Lerp (AxisRotationCamera.GetDefaultFOV * 0.3f, AxisRotationCamera.GetDefaultFOV, HDRISpheres [TargetIndex].material.GetFloat ("_Exposure") * 2.0f);
					}
				}

				if (HDRISpheres [CurrentIndex].material.GetFloat ("_Exposure") > 0.01f) {
				} else {
					foreach (Transform ParsedNavigationButton in HDRISpheres [TargetIndex].transform) {
						ParsedNavigationButton.gameObject.SetActive (true);
					}
				}

				Raycast = AxisRotationCamera.GetAxisRotationCamera.ViewportPointToRay (Vector3.one * 0.5f);

				if (Physics.Raycast (Raycast, out HitInfo, 1000.0f)) {
					HitNavigationButton = HitInfo.collider.gameObject.GetComponent<NavigationButton> ();
					if (HitNavigationButton == null) {
						NavigationButtonHit = false;
						AxisRotationCamera.AdjustCrosshairRing = 1.0f;
					} else {
						if (NavigationButtonHit) {
							CurrentTimestamp = Time.realtimeSinceStartup;

							if (CurrentTimestamp < CountdownTimestamp) {
								if (AxisRotationCamera.IsCrosshairActive) {
									AxisRotationCamera.AdjustCrosshairRing = (CountdownTimestamp - CurrentTimestamp) / 2.0f;
								} else {
									NavigationButtonHit = false;
								}
							} else {
								NavigationButtonHit = false;

								if (string.IsNullOrEmpty (HitNavigationButton.GetTargetHDRI)) {
									CurrentIndex = TargetIndex;
									TargetIndex = HitNavigationButton.GetTargetIndex;

									Traverse (CurrentIndex, TargetIndex);
								} else {
									HitInfo.collider.enabled = false;
									AssetsManager.InterHDRITraverse (HitNavigationButton.GetTargetHDRI, HitNavigationButton.GetTargetIndex);
								}
							}
						} else {
							NavigationButtonHit = true;
							CountdownTimestamp = Time.realtimeSinceStartup + 2.0f;

							HitNavigationButton.ApplyTexture = ActiveArrowTexture;
							AxisRotationCamera.AdjustCrosshairColor = true;
						}
					}
				} else {
					NavigationButtonHit = false;
					AxisRotationCamera.AdjustCrosshairRing = 1.0f;
					AxisRotationCamera.AdjustCrosshairColor = false;

					if (HitNavigationButton == null) {
					} else {
						HitNavigationButton.ApplyTexture = InactiveArrowTexture;
					}
				}

				yield return null;
			}
		}
	}
}
