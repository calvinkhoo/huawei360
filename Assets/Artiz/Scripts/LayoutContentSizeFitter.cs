﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.M360.Artiz {
	[ExecuteInEditMode][RequireComponent(typeof(LayoutElement))]
	public class LayoutContentSizeFitter : MonoBehaviour {
		[SerializeField] int ChildCount = 0;

		LayoutElement AttachedLayoutElement = null;

		void OnEnable () {
			ChildCount = 0;
		}

		void Update () {
			if (ChildCount.Equals (transform.childCount)) {
			} else {
				ChildCount = transform.childCount;
				UpdateContentSize ();
			}
		}

		void UpdateContentSize () {
			if (AttachedLayoutElement == null) {
				AttachedLayoutElement = GetComponent<LayoutElement> ();
			}

			if (AttachedLayoutElement == null) {
			} else {
				for (int a = ChildCount - 1; a > 0; a--) {
					if (transform.GetChild (a).GetComponent<LayoutVertical> () == null) {
					} else {
						AttachedLayoutElement.preferredHeight = transform.GetChild (a).GetComponent<LayoutVertical> ().GetOverallHeight;
						break;
					}
				}
			}
		}
	}
}
