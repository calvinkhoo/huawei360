﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace BMJ.M360.Artiz {
	[RequireComponent(typeof(MeshRenderer))]
	public class NavigationButton : MonoBehaviour {
		[SerializeField] string TargetHDRI = string.Empty;
		[SerializeField] int TargetIndex;

		MeshRenderer AttachedMeshRenderer;

		void Start () {
			AttachedMeshRenderer = GetComponent<MeshRenderer> ();
			AttachedMeshRenderer.material.shader = Shader.Find (AttachedMeshRenderer.material.shader.name);
		}

		public string GetTargetHDRI {
			get {
				return TargetHDRI;
			}
		}

		public int GetTargetIndex {
			get {
				return TargetIndex;
			}
		}

		public Texture2D ApplyTexture {
			set {
				if (AttachedMeshRenderer == null) {
				} else {
					AttachedMeshRenderer.material.SetTexture ("_MainTex", value);
				}
			}
		}
	}
}
