﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

using BMJ.M360.CustomCamera;

namespace BMJ.M360.Artiz {
	public class ArtizPreviewButton : MonoBehaviour {
		[SerializeField] Text DisplayNameText;
		[SerializeField] Text DescriptionText;
		[SerializeField] Image PreviewImage;
		[SerializeField] GameObject Hourglass;
        [SerializeField] GameObject PreviewHolder;
        [SerializeField] Image LoadingBar;

        string UniqueID = string.Empty;
		string SpriteThumbnailLink = string.Empty;
		string SpriteHDRILink = string.Empty;
		int CategoryIndex = 0;
		public int StartIndex = 0;

		Vector3 HDRIRotationOffset = Vector3.zero;

		Coroutine ActiveRoutine = null;

		UnityAction OnClickAction = null;

		bool InitialButtonFlag = false;

		void Start () {
			LoadingBar.fillAmount = 0.0f;
		}

		public string GetUniqueID {
			get {
				return UniqueID;
			}
		}

		public int GetCategoryIndex {
			get {
				return CategoryIndex;
			}
		}

		public void LoadProperty (ArtizPreviewProperty SetArtizProperty, bool SetInitialButtonFlag, string SetThumbnailLink) {
			if (Hourglass.activeSelf) {
				DisplayNameText.text = SetArtizProperty.DisplayName;
				DescriptionText.text = SetArtizProperty.Description;

				UniqueID = SetArtizProperty.UniqueID;
				SpriteThumbnailLink = SetThumbnailLink;
				SpriteHDRILink = SetArtizProperty.SpriteHDRILink;
				CategoryIndex = SetArtizProperty.CategoryIndex;
				StartIndex = SetArtizProperty.SpriteThumbnailLink.IndexOf (SetThumbnailLink);
				print ("Index of : " + gameObject.name + " - " + StartIndex);
				HDRIRotationOffset = SetArtizProperty.HDRIRotationOffset;

				InitialButtonFlag = SetInitialButtonFlag;
			}
		}

		public void LoadThumbnail () {
			if (ActiveRoutine == null && Hourglass.activeSelf) {
				StartCoroutine (LoadThumbnailRoutine ());
			}
		}

		IEnumerator LoadThumbnailRoutine () {
			UnityWebRequest ThumbnailRequest = UnityWebRequestTexture.GetTexture (SpriteThumbnailLink);

			yield return ThumbnailRequest.SendWebRequest ();

			while (!ThumbnailRequest.isDone) {
				LoadingBar.fillAmount = ThumbnailRequest.downloadProgress;
				yield return null;
			}

			LoadingBar.fillAmount = 1.0f;

			if (ThumbnailRequest.isNetworkError || ThumbnailRequest.isHttpError) {
				Debug.Log ("[" + SpriteThumbnailLink + "]" + ThumbnailRequest.error);
			} else {
				Texture2D RequestedTexture = DownloadHandlerTexture.GetContent (ThumbnailRequest);

				Sprite SpriteCache = Sprite.Create (RequestedTexture, new Rect (0.0f, 0.0f, RequestedTexture.width, RequestedTexture.height), Vector2.zero);

				if (SpriteCache == null) {
					Debug.LogError ("SpriteCache == null");
				} else {
					PreviewImage.sprite = SpriteCache;
					Hourglass.SetActive (false);
				}

				if (InitialButtonFlag) {
					Click ();
					InitialButtonFlag = false;
				}
			}
		}

		public void Click () {
			if (Hourglass.activeSelf) {
			} else if (AxisRotationCamera.GetLoadingStatus) {
				Debug.LogError ("Preview still loading...");
			} else {
				Debug.Log ("Clicked! Loading... [" + SpriteHDRILink + "] - [" + StartIndex + "]");
				PreviewManager.Preview (SpriteHDRILink, HDRIRotationOffset, transform, StartIndex);
			}
            PreviewHolder.GetComponentInParent<CardboardUI>().LogoOn();
        }

        public void PreloadClick()
        {
            if (AxisRotationCamera.GetLoadingStatus)
            {
                Debug.LogError("Preview still loading...");
            }
            else
            {
                Debug.Log("Clicked! Loading... [" + SpriteHDRILink + "] - [" + StartIndex + "]");
                PreviewManager.Preview(SpriteHDRILink, HDRIRotationOffset, transform, StartIndex);
            }
        }
    }
}