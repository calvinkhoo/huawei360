﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.M360.Artiz {
	public class InfoPanelController : MonoBehaviour {
		[SerializeField] GameObject InfoPanelObject;
		[SerializeField] GameObject PreviewImageObject;
		[SerializeField] Image PreviewImage;

		readonly string FacebookPageLink = "https://www.facebook.com/ArtizStudio.MY/";
		readonly string WebsitePageLink = "http://www.artizstudio.co.kr/";
		readonly string InstagramPageLink = "https://www.instagram.com/artizstudio/";

		public void OpenFacebookPageLink () {
			Application.OpenURL (FacebookPageLink);
		}

		public void OpenWebsitePageLink () {
			Application.OpenURL (WebsitePageLink);
		}

		public void OpenInstagramPageLink () {
			Application.OpenURL (InstagramPageLink);
		}

		void Start () {
			StartCoroutine (SetupRoutine ());
		}

		IEnumerator SetupRoutine () {
			yield return new WaitForEndOfFrame ();
			foreach (LayoutVertical ParsedLayoutVertical in FindObjectsOfType<LayoutVertical> ()) {
				ParsedLayoutVertical.Setup ();
			}
			yield return new WaitForEndOfFrame ();
			ToggleInfoPanel = false;
		}

		public bool ToggleInfoPanel {
			set {
				InfoPanelObject.SetActive (value);

				if (value) {
					foreach (LayoutVertical ParsedLayoutVertical in FindObjectsOfType<LayoutVertical> ()) {
						ParsedLayoutVertical.Setup ();
					}
				}
			}
		}

		public void ShowPreviewPanel (Image SourceImage) {
			PreviewImage.sprite = Resources.Load<Sprite> (SourceImage.sprite.name);

			PreviewImageObject.SetActive (true);

			Resources.UnloadUnusedAssets ();
		}

		public void HidePreviewPanel () {
			PreviewImageObject.SetActive (false);

			Resources.UnloadUnusedAssets ();
		}
	}
}