﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using BMJ.M360.Generic;

namespace BMJ.M360.Artiz {
	public class FractionController : InstancedMonoBehaviour<FractionController> {
		[SerializeField] Text TopFraction;
		[SerializeField] Text BottomFraction;

		public static void UpdateFraction (int TopFractionValue, int BottomFractionValue) {
			if (GetInstance == null) {
				Debug.LogError ("ChildCountIndicator == null");
			} else {
				GetInstance.ProcessUpdateFraction (TopFractionValue, BottomFractionValue);
			}
		}

		void ProcessUpdateFraction (int TopFractionValue, int BottomFractionValue) {
			TopFraction.text = TopFractionValue.ToString ();
			BottomFraction.text = BottomFractionValue.ToString ();
		}
	}
}
