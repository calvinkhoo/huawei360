﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using BMJ.M360.Generic;

namespace BMJ.M360.Artiz.Scroller
{
    public class ScrollingController2 : InstancedMonoBehaviour<ScrollingController2>
    {
        [System.Serializable]
        public class CategorySelector
        {
            [SerializeField] GameObject OnButton;
            [SerializeField] GameObject OffButton;
            [SerializeField] GameObject Scroller;

            public Transform GetScrollerTransform
            {
                get
                {
                    return Scroller.transform;
                }
            }

            public bool Toggle
            {
                set
                {
                    OnButton.SetActive(value);
                    OffButton.SetActive(!value);
                    Scroller.SetActive(value);

                    if (value)
                    {
                        //FractionController.UpdateFraction (1, Scroller.GetComponent<ScrollRect> ().content.childCount);
                        PreviewButtonsManager2.LoadThumbnails(Scroller.transform);
                    }
                }
            }
        }

        [SerializeField] List<CategorySelector> CategorySelectors;

        public static int Initialize
        {
            get
            {
                if (GetInstance == null)
                {
                    return 300;
                }
                else
                {
                    return GetInstance.ProcessInitialize;
                }
            }
        }

        int ProcessInitialize
        {
            get
            {
                if (CategorySelectors.Count > 0)
                {
                    ToggleOnCategory(0);
                    return 200;
                }
                else
                {
                    return 301;
                }
            }
        }

        public void ToggleOnCategory(int Index)
        {
            foreach (CategorySelector ParsedCategorySelector in CategorySelectors)
            {
                ParsedCategorySelector.Toggle = CategorySelectors.IndexOf(ParsedCategorySelector).Equals(Mathf.Clamp(Index, 0, CategorySelectors.Count));
            }
        }

        public static Transform GetScroller(int Index)
        {
            if (GetInstance == null)
            {
                return null;
            }
            else
            {
                return GetInstance.CategorySelectors[Mathf.Clamp(Index, 0, GetInstance.CategorySelectors.Count)].GetScrollerTransform;
            }
        }
    }
}
