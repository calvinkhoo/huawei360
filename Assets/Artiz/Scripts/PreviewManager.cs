﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR;

using BMJ.M360.Generic;
using BMJ.M360.CustomCamera;

namespace BMJ.M360.Artiz {
	public class PreviewManager : InstancedMonoBehaviour<PreviewManager> {
		[SerializeField] Transform HDRIContentHolder;
		[SerializeField] GameObject HDRIPrefab;
		[SerializeField] GameObject LoadingHDRIPrefab;
		[SerializeField] Material HDRIMaterial;
		[SerializeField] Shader CubemapShader;
		[SerializeField] Shader UnlitTransparentShader;

		bool InitialPreview = true;

		public static int CustomStartIndex = -1;

		Dictionary<string, AssetBundle> HDRIReference = new Dictionary<string, AssetBundle> ();

		List<string> LoadingReferenceTable = new List<string> ();

		public static Shader GetCubemapShader {
			get {
				if (GetInstance == null) {
					return null;
				} else {
					return GetInstance.CubemapShader;
				}
			}
		}

		public static Shader GetUnlitTransparentShader {
			get {
				if (GetInstance == null) {
					return null;
				} else {
					return GetInstance.UnlitTransparentShader;
				}
			}
		}

		void Start () {
			AxisRotationCamera.UpdateLoading (false);

			//Instantiate (HDRIPrefab, HDRIContentHolder);
		}
		
		public void SwitchToVR () {
			XRSettings.LoadDeviceByName ("OpenVR");
		}

		public static void Preview (string SpriteHDRILink, Vector3 HDRIRotationOffset, Transform PreviewButton, int StartIndex = 0) {
			print ("Set Custom Index : " + StartIndex);
			if (GetInstance == null) {
				Debug.LogError ("PreviewManager.GetInstance == null");
			} else {
				GetInstance.ProcessPreview (SpriteHDRILink, HDRIRotationOffset, PreviewButton, StartIndex);
			}
		}

		void ProcessPreview (string SpriteHDRILink, Vector3 HDRIRotationOffset, Transform PreviewButton, int StartIndex) {
			GC.Collect ();

			AxisRotationCamera.ToggleCrosshair = false;

			if (HDRIReference.ContainsKey (SpriteHDRILink)) {
				string PrefabAssetName = string.Empty;
				PrefabAssetName = SpriteHDRILink.Replace ("http://galaxybattles.deekie.com/huaweiVR/", "");
				PrefabAssetName = PrefabAssetName.Replace ("android/", "");
				PrefabAssetName = PrefabAssetName.Replace ("ios/", "");
				PrefabAssetName = PrefabAssetName.Replace ("hdri/", "");
				PrefabAssetName = PrefabAssetName.Replace (".assetbundle", "");
				PrefabAssetName += ".prefab";


                Debug.Log("PrefabAssetName : " + PrefabAssetName);

                foreach (string AssetName in HDRIReference [SpriteHDRILink].GetAllAssetNames ()) {
					print ("AssetName : " + AssetName);
				}

				GameObject TargetPrefab = HDRIReference [SpriteHDRILink].LoadAsset (PrefabAssetName) as GameObject;

				if (TargetPrefab == null)
                {

                    Debug.Log("TargetPrefab == null");
                } else {
					foreach (Transform Child in HDRIContentHolder) {
						Destroy (Child.gameObject);
					}

					GameObject HDRISphere = Instantiate (TargetPrefab, HDRIContentHolder);

                    Debug.Log("HDRI loaded");

					CustomStartIndex = StartIndex;

					HDRISphere.transform.localPosition = Vector3.zero;
					HDRISphere.transform.localRotation = Quaternion.Euler (HDRIRotationOffset);
					HDRISphere.transform.localScale = Vector3.one * 5.0f;

					if (InitialPreview) {
						InitialPreview = false;
						AssetsManager.InitialPreviewAction ();
					} else {
						ToggleViewController.ManualToggleOff ();
					}
				}
			} else {
				if (LoadingReferenceTable.Contains (SpriteHDRILink)) {
				} else {
					LoadingReferenceTable.Add (SpriteHDRILink);

					//Instantiate (LoadingHDRIPrefab, HDRIContentHolder);

					StartCoroutine (LoadHDRI (SpriteHDRILink, HDRIRotationOffset, PreviewButton, StartIndex));
				}
			}

			//FractionController.UpdateFraction (PreviewButton.GetSiblingIndex () + 1, PreviewButton.parent.childCount);
		}

        public void SetHDRI(string TargetHDRI)
        {
            foreach (ArtizPreviewButton ParsedPreviewButton in FindObjectsOfType<ArtizPreviewButton>())
            {
                if (ParsedPreviewButton.GetUniqueID.Equals(TargetHDRI))
                {
                    print("Checking : " + ParsedPreviewButton.GetUniqueID + " - " + TargetHDRI);
                    ParsedPreviewButton.PreloadClick();
                    break;
                }
                else
                {
                    Debug.Log("UID error");
                }
            }
        }

        IEnumerator LoadHDRI (string SpriteHDRILink, Vector3 HDRIRotationOffset, Transform PreviewButton, int StartIndex) {
			while (!Caching.ready) {
				yield return null;
			}

			WWW HDRIRequest = WWW.LoadFromCacheOrDownload (SpriteHDRILink, AssetsManager.GetAssetVersion);

			while (!HDRIRequest.isDone) {
				AxisRotationCamera.UpdateLoading (true, HDRIRequest.progress);
				yield return null;
			}

			AxisRotationCamera.UpdateLoading (false);

			if (!string.IsNullOrEmpty (HDRIRequest.error)) {
				Debug.LogError (HDRIRequest.error);
			} else {
				AssetBundle RequestedAssetBundle = HDRIRequest.assetBundle;

				HDRIReference[SpriteHDRILink]= RequestedAssetBundle;

				ProcessPreview (SpriteHDRILink, HDRIRotationOffset, PreviewButton, StartIndex);

				LoadingReferenceTable.Remove (SpriteHDRILink);
			}
		}
	}
}
