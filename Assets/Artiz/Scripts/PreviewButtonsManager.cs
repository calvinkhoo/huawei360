﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using BMJ.M360.Artiz.Scroller;
using BMJ.M360.Generic;

namespace BMJ.M360.Artiz
{
    public class PreviewButtonsManager : InstancedMonoBehaviour<PreviewButtonsManager>
    {
        [SerializeField] GameObject PreviewPrefab;
        [SerializeField] GameObject PreviewSpacerPrefab;
        [SerializeField] Transform PreviewHolder;
        [SerializeField] RectTransform HeightReference;

        bool InitialButtonFlag = true;

        public static void PreloadButton(ArtizPreviewProperty SetArtizProperty)
        {
            if (GetInstance == null)
            {
            }
            else
            {
                GetInstance.ProcessPreloadButton(SetArtizProperty);
            }
        }

        public void ProcessPreloadButton(ArtizPreviewProperty SetArtizProperty)
        {
            if (ScrollingController.GetScroller(SetArtizProperty.CategoryIndex) == null)
            {
                Debug.LogError("PreloadButton failed, scroller = null");
            }
            else
            {
                foreach (string SpriteThumbnail in SetArtizProperty.SpriteThumbnailLink)
                {
                    GameObject PreviewButton = Instantiate(PreviewPrefab, ScrollingController.GetScroller(SetArtizProperty.CategoryIndex).GetComponent<ScrollRect>().content.transform);
                    PreviewButton.GetComponent<LayoutElement>().preferredWidth = 260;

                    PreviewButton.GetComponent<ArtizPreviewButton>().LoadProperty(SetArtizProperty, InitialButtonFlag, SpriteThumbnail);
                }
                InitialButtonFlag = false;
            }
        }

        public static void LoadThumbnails(Transform Scroller)
        {
            if (GetInstance == null)
            {
            }
            else
            {
                GetInstance.ProcessLoadThumbnails(Scroller);
            }
        }

        void ProcessLoadThumbnails(Transform Scroller)
        {
            foreach (ArtizPreviewButton PreviewButton in Scroller.GetComponentsInChildren<ArtizPreviewButton>())
            {
                PreviewButton.LoadThumbnail();
            }

            Scroller.GetComponent<SnapScrollRect>().Initialize();
        }
    }
}
