﻿//using System.Collections;
//using System.IO;
//using System.Net;
//using UnityEngine;
//using UnityEngine.Events;
//using UnityEngine.UI;
//using ZXing;
//using ZXing.QrCode;

//// Screen Recorder will save individual images of active scene in any resolution and of a specific image format
//// including raw, jpg, png, and ppm.  Raw and PPM are the fastest image formats for saving.
////
//// You can compile these images into a video using ffmpeg:
//// ffmpeg -i screen_3840x2160_%d.ppm -y test.avi

//public class ScreenRecorder : MonoBehaviour
//{
//    [SerializeField] Image QRImage;
//    Texture2D QRTexture;
//    bool isDone = false;
//    bool videoOn = false;
//    string guid = "";

//    // 4k = 3840 x 2160   1080p = 1920 x 1080
//    public int captureWidth;
//    public int captureHeight;

//    // optional game object to hide during screenshots (usually your scene canvas hud)
//    public GameObject hideGameObject;

//    // optimize for many screenshots will not destroy any objects so future screenshots will be fast
//    public bool optimizeForManyScreenshots = true;

//    // configure with raw, jpg, png, or ppm (simple raw format)
//    public enum Format { RAW, JPG, PNG, PPM };
//    public Format format = Format.PPM;

//    // folder to write output (defaults to data path)
//    public string folder;

//    // private vars for screenshot
//    private Rect rect;
//    private RenderTexture renderTexture;
//    private Texture2D screenShot;
//    private int counter = 0; // image #

//    // commands
//    private bool captureScreenshot = false;
//    private bool captureVideo = false;

//    public UnityEvent onLoad;

//    public VideoRecordBehaviour VideoRecordBehaviour;

//    ScreenOrientation orientation;

//    private void Awake()
//    {
//        orientation = Screen.orientation;
//        Orientation();
//    }

//    private void Potrait()
//    {
//        captureWidth = 540;
//        captureHeight = 960;
//    }

//    private void Landscape()
//    {
//        captureWidth = 960;
//        captureHeight = 540;
//    }

//    private void Orientation()
//    {
//        if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
//        {
//            Potrait();
//        }
//        else
//        {
//            Landscape();
//        }
//    }

//    // create a unique filename using a one-up variable
//    private string uniqueFilename(int width, int height)
//    {

//        // if folder not specified by now use a good default
//        //if (folder == null || folder.Length == 0)
//        //{
//        //    folder = Application.dataPath;
//        //    if (Application.isEditor)
//        //    {
//        //        // put screenshots in folder above asset path so unity doesn't index the files
//        //        var stringPath = folder + "/..";
//        //        folder = Path.GetFullPath(stringPath);
//        //    }
//        //    folder += "/screenshots";
//        //
//        //    // make sure directoroy exists
//        //    Debug.Log(System.IO.Directory.CreateDirectory(folder));
//        //
//        //    // count number of files of specified format in folder
//        //    string mask = string.Format("screen_{0}x{1}*.{2}", width, height, format.ToString().ToLower());
//        //    counter = Directory.GetFiles(folder, mask, SearchOption.TopDirectoryOnly).Length;
//        //}

//        // use width, height, and counter for unique file name
//        var filename = string.Format("{0}/screen_{1}.{2}", folder, counter.ToString("D3"), format.ToString().ToLower());

//        //  Debug.Log(filename);
//        // up counter for next call
//        ++counter;

//        // return unique filename
//        return filename;
//    }

//    public void CaptureScreenshot()
//    {
//        captureScreenshot = true;
//    }


//    public void StartTakePhoto()
//    {
//        StartCoroutine(takePhoto());
//    }
//    public IEnumerator takePhoto()
//    {
//        elapsed = 0;
//        counter = 0;
//        folder = Application.persistentDataPath + "/GIF-" + System.Guid.NewGuid().ToString();
//        Directory.CreateDirectory(folder);
//        captureVideo = true;
//        yield return new WaitForSeconds(1f);
//        onLoad.Invoke();
//        captureVideo = false;
//        yield return GetComponent<Uploader>().UploadImage(folder);
//    }
//    public float recordDuration = 0;
//    float previousTime = 0;

//    public void StartTakeVideo()
//    {
//        videoOn = false;
//        if (videoOn == false)
//        {
//            guid = System.Guid.NewGuid().ToString();
//            VideoRecordBehaviour.REC();
//            videoOn = true;
//        }
//        else
//        {
//            Debug.Log("Video is recording");
//        }
//    }

//    public void StopTakeVideo()
//    {
//        if (videoOn == true)
//        {
//            VideoRecordBehaviour.REC();
//            StartCoroutine(ExecuteAfterTime(5));
//        }
//        else
//        {
//            Debug.Log("No video");
//        }
//    }

//    Coroutine ActiveRoutine = null;

//    public void UploadVideoftp()
//    {

//        ftp fileServer = new ftp("ftp://35.185.190.169", "tom", "tom888");

//        if (fileServer.upload("/galaxybattles.deekie.com/GIF/" + string.Format("Video-{0}.mp4", guid), Application.persistentDataPath + "/AR_tempVideo.mov"))
//        {
//            print("Upload success");
//            isDone = true;
//        }
//        else
//        {
//            print("Upload fail");
//        }
//    }

//    public void VideoQR()
//    {
//        QRTexture = GenerateQR("http://galaxybattles.deekie.com/GIF/Video-" + guid + ".mp4");
//        QRImage.sprite = Sprite.Create(QRTexture, new Rect(Vector2.zero, Vector2.one * 256.0f), Vector2.one * 0.5f);
//    }

//    public void ShareVideo()
//    {
//        string v = Application.persistentDataPath + "/AR_tempVideo.mov";
//        new NativeShare().AddFile(v).SetSubject("").SetText("").Share();
//    }

//    IEnumerator ExecuteAfterTime(float time)
//    {
//        yield return new WaitForSeconds(time);

//        GetVideo();
//    }

//    IEnumerator ExecuteAfterTimee(float time)
//    {
//        yield return new WaitForSeconds(time);

//        VideoQR();
//    }

//    public void GetVideo()
//    {
//        Debug.Log("here");
//        UploadVideoftp();

//        if (isDone == true)
//        {
//            Debug.Log("or here");
//            if (URLExists("http:/" + "/galaxybattles.deekie.com/GIF/" + string.Format("Video-{0}.mp4", guid)))
//            {
//                if (ActiveRoutine == null)
//                {
//                    ActiveRoutine = StartCoroutine(ExecuteAfterTimee(2));
//                }
//                else
//                {
//                    print("Active routine not null");
//                }
//            }
//            else
//            {
//            }
//        }
//    }

//    public Texture2D GenerateQR(string text)
//    {
//        var encoded = new Texture2D(256, 256);
//        var color32 = Encode(text, encoded.width, encoded.height);
//        encoded.SetPixels32(color32);
//        encoded.Apply();
//        return encoded;
//    }

//    private static Color32[] Encode(string textForEncoding, int width, int height)
//    {
//        var writer = new BarcodeWriter
//        {
//            Format = BarcodeFormat.QR_CODE,
//            Options = new QrCodeEncodingOptions
//            {
//                Height = height,
//                Width = width
//            }
//        };
//        return writer.Write(textForEncoding);
//    }

//    public bool URLExists(string url)
//    {
//        bool result = false;

//        WebRequest webRequest = WebRequest.Create(url);
//        webRequest.Timeout = 5000; // miliseconds
//        webRequest.Method = "HEAD";

//        HttpWebResponse response = null;

//        try
//        {
//            response = (HttpWebResponse)webRequest.GetResponse();
//            result = true;
//        }
//        catch (WebException webException)
//        {
//            Debug.Log(url + " doesn't exist: " + webException.Message);
//        }
//        finally
//        {
//            if (response != null)
//            {
//                response.Close();
//            }
//        }

//        return result;
//    }


//    float elapsed = 0;
//    private void Update()
//    {

//        if (orientation != Screen.orientation)
//        {
//            Orientation();
//            orientation = Screen.orientation;
//        }

//        // check keyboard 'k' for one time screenshot capture and holding down 'v' for continious screenshots
//        // captureScreenshot |= Input.GetKeyDown("k");
//        // captureVideo = Input.GetKey("v");

//        if (captureScreenshot || captureVideo)
//        {

//            elapsed += Time.deltaTime;

//            if (elapsed < 0.1) return;

//            elapsed = 0;
//            captureScreenshot = false;

//            // hide optional game object if set
//            if (hideGameObject != null) hideGameObject.SetActive(false);

//            // create screenshot objects if needed
//            if (renderTexture == null)
//            {
//                // creates off-screen render texture that can rendered into
//                rect = new Rect(0, 0, captureWidth, captureHeight);
//                renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
//                screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);
//            }

//            // get main camera and manually render scene into rt
//            Camera camera = this.GetComponent<Camera>(); // NOTE: added because there was no reference to camera in original script; must add this script to Camera
//            camera.targetTexture = renderTexture;
//            camera.Render();

//            // read pixels will read from the currently active render texture so make our offscreen 
//            // render texture active and then read the pixels
//            RenderTexture.active = renderTexture;
//            screenShot.ReadPixels(rect, 0, 0);

//            // reset active camera texture and render texture
//            camera.targetTexture = null;
//            RenderTexture.active = null;

//            // get our unique filename
//            string filename = uniqueFilename((int)rect.width, (int)rect.height);

//            // pull in our file header/data bytes for the specified image format (has to be done from main thread)
//            byte[] fileHeader = null;
//            byte[] fileData = null;
//            if (format == Format.RAW)
//            {
//                fileData = screenShot.GetRawTextureData();
//            }
//            else if (format == Format.PNG)
//            {
//                fileData = screenShot.EncodeToPNG();
//            }
//            else if (format == Format.JPG)
//            {
//                fileData = screenShot.EncodeToJPG();
//            }
//            else // ppm
//            {
//                // create a file header for ppm formatted file
//                string headerStr = string.Format("P6\n{0} {1}\n255\n", rect.width, rect.height);
//                fileHeader = System.Text.Encoding.ASCII.GetBytes(headerStr);
//                fileData = screenShot.GetRawTextureData();
//            }

//            // create new thread to save the image to file (only operation that can be done in background)
//            new System.Threading.Thread(() =>
//            {
//                // create file and write optional header with image bytes
//                var f = File.Create(filename);
//                if (fileHeader != null) f.Write(fileHeader, 0, fileHeader.Length);
//                f.Write(fileData, 0, fileData.Length);
//                f.Close();
//                //  Debug.Log(string.Format("Wrote screenshot {0} of size {1}", filename, fileData.Length));
//            }).Start();

//            // unhide optional game object if set
//            if (hideGameObject != null) hideGameObject.SetActive(true);

//            // cleanup if needed
//            if (optimizeForManyScreenshots == false)
//            {
//                Destroy(renderTexture);
//                renderTexture = null;
//                screenShot = null;
//            }
//        }
//    }
//}