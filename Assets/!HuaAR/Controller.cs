﻿using System;
using System.Collections;
using System.Collections.Generic;
using BMJ.M360.CustomCamera;
using UnityEngine;

public class Controller : MonoBehaviour {

    [SerializeField] GameObject Splash;
    [SerializeField] GameObject HomePage;
    [SerializeField] GameObject LandscapeSplash;
    [SerializeField] GameObject LandscapePanel;

    [SerializeField] RectTransform ScrollView;

    Vector3 MinCache = Vector3.zero;
    Vector3 MaxCache = Vector3.zero;

    private bool Toggle = false;

    ScreenOrientation orientation;

    private void Awake()
    {
        orientation = Screen.orientation;
        Orientation();
    }

    private void Start()
    {
    
    }

    private void Orientation()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            LandscapePanel.SetActive(true);
            LandscapeSplash.SetActive(true);
        }
        else
        {
            LandscapePanel.SetActive(false);
            LandscapeSplash.SetActive(false);
        }
    }

    IEnumerator SplashToHome(float time)
    {
        yield return new WaitForSeconds(time);

        Splash.SetActive(false);
        HomePage.SetActive(true);
    }

    public void ToggleUp()
    {
        Toggle = false;
    }

    public void ToggleDown()
    {
        Toggle = true;
    }

    // Update is called once per frame
    private void Update () 
    {
        if (orientation != Screen.orientation)
        {
            Orientation();
            orientation = Screen.orientation;
        }

        if (Toggle == true)
        {
            MinCache = ScrollView.localPosition;
            MinCache -= new Vector3(0, 25f, 0);
            MinCache.y = Mathf.Clamp(MinCache.y, -420.0f, 0.0f);

            ScrollView.localPosition = MinCache;
        }
        else
        {
            MaxCache = ScrollView.localPosition;
            MaxCache += new Vector3(0, 25f, 0);
            MaxCache.y = Mathf.Clamp(MaxCache.y, -420.0f, 0.0f);

            ScrollView.localPosition = MaxCache;
        }
    }
}
