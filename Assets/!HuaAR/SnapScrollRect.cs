﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(ScrollRect))]
public class SnapScrollRect : MonoBehaviour
{
    [SerializeField] [Range(1.0f, 100.0f)] float Step = 1.0f;

    ScrollRect AttachedScrollRect;
    int ContentAmount = 1;
    Vector2 TargetNormalizedPosition = Vector2.zero;

    Coroutine ActiveUpdateRoutine = null;

    void Awake()
    {
        AttachedScrollRect = GetComponent<ScrollRect>();
    }

    public void Initialize()
    {
        ContentAmount = AttachedScrollRect.content.childCount;

        TargetNormalizedPosition.x = Mathf.Clamp(Mathf.RoundToInt(0.0f * (ContentAmount)) / (float)(ContentAmount), 0.0f, 1.0f - (1.0f / ContentAmount));
        TargetNormalizedPosition.y = AttachedScrollRect.normalizedPosition.y;
    }

    void OnDisable()
    {
        if (ActiveUpdateRoutine == null)
        {
        }
        else
        {
            StopCoroutine(UpdateRoutine());
        }
    }

    void OnEnable()
    {
        AttachedScrollRect.onValueChanged.RemoveAllListeners();
        AttachedScrollRect.onValueChanged.AddListener(
            (UpdatedPosition) =>
            {
                ContentAmount = AttachedScrollRect.content.childCount;

                if (ContentAmount > 0)
                {
                    TargetNormalizedPosition.x = Mathf.Clamp(Mathf.RoundToInt(AttachedScrollRect.normalizedPosition.x * (ContentAmount)) / (float)(ContentAmount), 0.0f, 1.0f - (1.0f / ContentAmount));
                    TargetNormalizedPosition.y = AttachedScrollRect.normalizedPosition.y;
                }
            }
        );

        ActiveUpdateRoutine = StartCoroutine(UpdateRoutine());
    }

    IEnumerator UpdateRoutine()
    {
        while (true)
        {
            if (Input.GetMouseButton(0))
            {
            }
            else
            {
                AttachedScrollRect.normalizedPosition = Vector2.MoveTowards(AttachedScrollRect.normalizedPosition, TargetNormalizedPosition, Step * Time.deltaTime);
            }

            yield return null;
        }
    }
}

