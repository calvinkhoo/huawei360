﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardboardUI : MonoBehaviour {

    [SerializeField] GameObject LogoImage;
    [SerializeField] GameObject Selection;

    Vector3 vector = new Vector3(0.0001f, 0.0001f, 0.0001f);
    Vector3 vectors = new Vector3(0, 1000, 0);

    public void LogoOn()
    {
        LogoImage.SetActive(true);
        Selection.transform.localScale = vector;
        Selection.transform.localPosition = vectors;
        Debug.Log("Turned");
    }

    public void LogoOff()
    {
        LogoImage.SetActive(false);
        Selection.transform.localScale = Vector3.one;
        Selection.transform.localPosition = Vector3.zero;
        Debug.Log("Turned");
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
