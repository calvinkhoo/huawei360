﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Collections;

public class VRRaycaster : MonoBehaviour
{
    [SerializeField] GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    [SerializeField] EventSystem m_EventSystem;
    float timestamp = 0.0f;
    bool buttonhit = false;
    GameObject lastHitObject = null;

    [SerializeField] Image countdownImage;
    float waitTime = 1.5f;

    void Start()
    {
        //Fetch the Raycaster from the GameObject (the Canvas)
        //m_Raycaster = GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        //m_EventSystem = GetComponent<EventSystem>();
        countdownImage.fillAmount = 0;
    }

    bool clear = true;

    void Update()
    {
        //Check if the left Mouse button is clicked
        //Set up the new Pointer Event
        m_PointerEventData = new PointerEventData(m_EventSystem);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = Camera.main.ViewportToScreenPoint(Vector3.one * 0.5f);

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        m_Raycaster.Raycast(m_PointerEventData, results);

        countdownImage.fillAmount = 0;

        clear = true;

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {

            if (result.gameObject.Equals(lastHitObject))
            {
                countdownImage.fillAmount = 0;
            }
            else
            {
                if (result.gameObject.GetComponent<Button>() == null)
                {


                }
                else
                {
                    clear = false;
                    Debug.Log("Hit " + result.gameObject.name);
                    if (buttonhit)
                    {
                        //countdownImage.fillAmount += 1.0f / waitTime * Time.deltaTime;
                        countdownImage.fillAmount = 1.0f - ((timestamp + 1.5f - Time.realtimeSinceStartup) / 1.5f);

                        if (Time.realtimeSinceStartup < timestamp + 1.5f)
                        {

                        }
                        else
                        {
                            lastHitObject = result.gameObject;
                            countdownImage.fillAmount = 0;
                            buttonhit = false;
                            result.gameObject.GetComponent<Button>().onClick.Invoke();
                        }
                    }
                    else
                    {
                        countdownImage.fillAmount = 0;
                        timestamp = Time.realtimeSinceStartup;
                        buttonhit = true;
                    }
                }
            }
        }

        if (clear)
        {
            buttonhit = false;
        }
    }
}