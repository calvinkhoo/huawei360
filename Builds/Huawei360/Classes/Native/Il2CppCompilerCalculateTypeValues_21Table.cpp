﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// BMJ.M360.VR.VRExplorerController
struct VRExplorerController_t1496324211;
// Controller
struct Controller_t2994601017;
// BMJ.M360.Artiz.SnappingScrollRect
struct SnappingScrollRect_t125492878;
// SnapScrollRect
struct SnapScrollRect_t3165954182;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// BMJ.M360.Artiz.ArtizPreviewButton
struct ArtizPreviewButton_t1230103917;
// System.Collections.Generic.List`1<BMJ.M360.Artiz.ArtizPreviewProperty>
struct List_1_t2968708851;
// UnityEngine.Events.UnityAction`1<BMJ.M360.Artiz.ArtizPreviewProperty>
struct UnityAction_1_t2081470250;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// BMJ.M360.Artiz.AssetsManager
struct AssetsManager_t3069477283;
// BMJ.M360.Artiz.LayoutVertical[]
struct LayoutVerticalU5BU5D_t1847752332;
// BMJ.M360.Artiz.InfoPanelController
struct InfoPanelController_t539477300;
// BMJ.M360.Artiz.CustomFitScroller
struct CustomFitScroller_t3272853734;
// BMJ.M360.Artiz.HDRIRotator
struct HDRIRotator_t2414495732;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// UnityEngine.Object
struct Object_t631007953;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.Transform
struct Transform_t3600365921;
// BMJ.M360.Artiz.PreviewManager
struct PreviewManager_t2255819439;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// BMJ.M360.CustomCamera.AxisRotationCamera
struct AxisRotationCamera_t2455264440;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// BMJ.M360.Artiz.NavigationButton
struct NavigationButton_t3491644213;
// BMJ.M360.Artiz.NavigatableHDRI
struct NavigatableHDRI_t1695953284;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// BMJ.M360.Artiz.PreviewButtonsManager
struct PreviewButtonsManager_t4268419265;
// BMJ.M360.Artiz.PreviewButtonsManager2
struct PreviewButtonsManager2_t2759123039;
// BMJ.M360.Artiz.Scroller.ScrollingController
struct ScrollingController_t1982690304;
// BMJ.M360.Artiz.Scroller.ScrollingController2
struct ScrollingController2_t2836624384;
// BMJ.M360.Artiz.ToggleViewController
struct ToggleViewController_t44480253;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// BMJ.M360.Artiz.FractionController
struct FractionController_t2625551139;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t2999697109;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Collections.Generic.List`1<UnityEngine.MeshRenderer>
struct List_1_t2059084002;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t4207451803;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// UnityEngine.UI.Button
struct Button_t4055032469;
// BMJ.M360.Artiz.AssetsManager/SerializablePreviewProperties
struct SerializablePreviewProperties_t119401764;
// System.Collections.Generic.List`1<BMJ.M360.Artiz.Scroller.ScrollingController/CategorySelector>
struct List_1_t3540556495;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Shader
struct Shader_t4151988712;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle>
struct Dictionary_2_t939163551;
// System.Collections.Generic.List`1<BMJ.M360.Artiz.Scroller.ScrollingController2/CategorySelector>
struct List_1_t1950397391;
// UnityEngine.Camera
struct Camera_t4157153871;




#ifndef U3CMODULEU3E_T692745562_H
#define U3CMODULEU3E_T692745562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745562 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745562_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef U3CLOADINTERIORROUTINEU3EC__ITERATOR0_T1972925413_H
#define U3CLOADINTERIORROUTINEU3EC__ITERATOR0_T1972925413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0
struct  U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413  : public RuntimeObject
{
public:
	// System.String BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::InteriorLink
	String_t* ___InteriorLink_0;
	// UnityEngine.Networking.UnityWebRequest BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::<LoadInteriorRequest>__0
	UnityWebRequest_t463507806 * ___U3CLoadInteriorRequestU3E__0_1;
	// System.String BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::<TargetName>__0
	String_t* ___U3CTargetNameU3E__0_2;
	// BMJ.M360.VR.VRExplorerController BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::$this
	VRExplorerController_t1496324211 * ___U24this_3;
	// System.Object BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 BMJ.M360.VR.VRExplorerController/<LoadInteriorRoutine>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_InteriorLink_0() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___InteriorLink_0)); }
	inline String_t* get_InteriorLink_0() const { return ___InteriorLink_0; }
	inline String_t** get_address_of_InteriorLink_0() { return &___InteriorLink_0; }
	inline void set_InteriorLink_0(String_t* value)
	{
		___InteriorLink_0 = value;
		Il2CppCodeGenWriteBarrier((&___InteriorLink_0), value);
	}

	inline static int32_t get_offset_of_U3CLoadInteriorRequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___U3CLoadInteriorRequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CLoadInteriorRequestU3E__0_1() const { return ___U3CLoadInteriorRequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CLoadInteriorRequestU3E__0_1() { return &___U3CLoadInteriorRequestU3E__0_1; }
	inline void set_U3CLoadInteriorRequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CLoadInteriorRequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadInteriorRequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CTargetNameU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___U3CTargetNameU3E__0_2)); }
	inline String_t* get_U3CTargetNameU3E__0_2() const { return ___U3CTargetNameU3E__0_2; }
	inline String_t** get_address_of_U3CTargetNameU3E__0_2() { return &___U3CTargetNameU3E__0_2; }
	inline void set_U3CTargetNameU3E__0_2(String_t* value)
	{
		___U3CTargetNameU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetNameU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___U24this_3)); }
	inline VRExplorerController_t1496324211 * get_U24this_3() const { return ___U24this_3; }
	inline VRExplorerController_t1496324211 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(VRExplorerController_t1496324211 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINTERIORROUTINEU3EC__ITERATOR0_T1972925413_H
#ifndef U3CSPLASHTOHOMEU3EC__ITERATOR0_T3519462395_H
#define U3CSPLASHTOHOMEU3EC__ITERATOR0_T3519462395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller/<SplashToHome>c__Iterator0
struct  U3CSplashToHomeU3Ec__Iterator0_t3519462395  : public RuntimeObject
{
public:
	// System.Single Controller/<SplashToHome>c__Iterator0::time
	float ___time_0;
	// Controller Controller/<SplashToHome>c__Iterator0::$this
	Controller_t2994601017 * ___U24this_1;
	// System.Object Controller/<SplashToHome>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Controller/<SplashToHome>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Controller/<SplashToHome>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CSplashToHomeU3Ec__Iterator0_t3519462395, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSplashToHomeU3Ec__Iterator0_t3519462395, ___U24this_1)); }
	inline Controller_t2994601017 * get_U24this_1() const { return ___U24this_1; }
	inline Controller_t2994601017 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Controller_t2994601017 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSplashToHomeU3Ec__Iterator0_t3519462395, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSplashToHomeU3Ec__Iterator0_t3519462395, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSplashToHomeU3Ec__Iterator0_t3519462395, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPLASHTOHOMEU3EC__ITERATOR0_T3519462395_H
#ifndef U3CUPDATEROUTINEU3EC__ITERATOR0_T4291033512_H
#define U3CUPDATEROUTINEU3EC__ITERATOR0_T4291033512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.SnappingScrollRect/<UpdateRoutine>c__Iterator0
struct  U3CUpdateRoutineU3Ec__Iterator0_t4291033512  : public RuntimeObject
{
public:
	// BMJ.M360.Artiz.SnappingScrollRect BMJ.M360.Artiz.SnappingScrollRect/<UpdateRoutine>c__Iterator0::$this
	SnappingScrollRect_t125492878 * ___U24this_0;
	// System.Object BMJ.M360.Artiz.SnappingScrollRect/<UpdateRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BMJ.M360.Artiz.SnappingScrollRect/<UpdateRoutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BMJ.M360.Artiz.SnappingScrollRect/<UpdateRoutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t4291033512, ___U24this_0)); }
	inline SnappingScrollRect_t125492878 * get_U24this_0() const { return ___U24this_0; }
	inline SnappingScrollRect_t125492878 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SnappingScrollRect_t125492878 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t4291033512, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t4291033512, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t4291033512, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEROUTINEU3EC__ITERATOR0_T4291033512_H
#ifndef U3CUPDATEROUTINEU3EC__ITERATOR0_T2386763820_H
#define U3CUPDATEROUTINEU3EC__ITERATOR0_T2386763820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnapScrollRect/<UpdateRoutine>c__Iterator0
struct  U3CUpdateRoutineU3Ec__Iterator0_t2386763820  : public RuntimeObject
{
public:
	// SnapScrollRect SnapScrollRect/<UpdateRoutine>c__Iterator0::$this
	SnapScrollRect_t3165954182 * ___U24this_0;
	// System.Object SnapScrollRect/<UpdateRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SnapScrollRect/<UpdateRoutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SnapScrollRect/<UpdateRoutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t2386763820, ___U24this_0)); }
	inline SnapScrollRect_t3165954182 * get_U24this_0() const { return ___U24this_0; }
	inline SnapScrollRect_t3165954182 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SnapScrollRect_t3165954182 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t2386763820, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t2386763820, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t2386763820, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEROUTINEU3EC__ITERATOR0_T2386763820_H
#ifndef U3CROTATECAMERAU3EC__ITERATOR1_T31197773_H
#define U3CROTATECAMERAU3EC__ITERATOR1_T31197773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.HDRIRotator/<RotateCamera>c__Iterator1
struct  U3CRotateCameraU3Ec__Iterator1_t31197773  : public RuntimeObject
{
public:
	// System.Boolean BMJ.M360.Artiz.HDRIRotator/<RotateCamera>c__Iterator1::<Rotate>__0
	bool ___U3CRotateU3E__0_0;
	// System.Object BMJ.M360.Artiz.HDRIRotator/<RotateCamera>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BMJ.M360.Artiz.HDRIRotator/<RotateCamera>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 BMJ.M360.Artiz.HDRIRotator/<RotateCamera>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U3CRotateU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRotateCameraU3Ec__Iterator1_t31197773, ___U3CRotateU3E__0_0)); }
	inline bool get_U3CRotateU3E__0_0() const { return ___U3CRotateU3E__0_0; }
	inline bool* get_address_of_U3CRotateU3E__0_0() { return &___U3CRotateU3E__0_0; }
	inline void set_U3CRotateU3E__0_0(bool value)
	{
		___U3CRotateU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRotateCameraU3Ec__Iterator1_t31197773, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRotateCameraU3Ec__Iterator1_t31197773, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRotateCameraU3Ec__Iterator1_t31197773, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CROTATECAMERAU3EC__ITERATOR1_T31197773_H
#ifndef CATEGORYSELECTOR_T478322649_H
#define CATEGORYSELECTOR_T478322649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.Scroller.ScrollingController2/CategorySelector
struct  CategorySelector_t478322649  : public RuntimeObject
{
public:
	// UnityEngine.GameObject BMJ.M360.Artiz.Scroller.ScrollingController2/CategorySelector::OnButton
	GameObject_t1113636619 * ___OnButton_0;
	// UnityEngine.GameObject BMJ.M360.Artiz.Scroller.ScrollingController2/CategorySelector::OffButton
	GameObject_t1113636619 * ___OffButton_1;
	// UnityEngine.GameObject BMJ.M360.Artiz.Scroller.ScrollingController2/CategorySelector::Scroller
	GameObject_t1113636619 * ___Scroller_2;

public:
	inline static int32_t get_offset_of_OnButton_0() { return static_cast<int32_t>(offsetof(CategorySelector_t478322649, ___OnButton_0)); }
	inline GameObject_t1113636619 * get_OnButton_0() const { return ___OnButton_0; }
	inline GameObject_t1113636619 ** get_address_of_OnButton_0() { return &___OnButton_0; }
	inline void set_OnButton_0(GameObject_t1113636619 * value)
	{
		___OnButton_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnButton_0), value);
	}

	inline static int32_t get_offset_of_OffButton_1() { return static_cast<int32_t>(offsetof(CategorySelector_t478322649, ___OffButton_1)); }
	inline GameObject_t1113636619 * get_OffButton_1() const { return ___OffButton_1; }
	inline GameObject_t1113636619 ** get_address_of_OffButton_1() { return &___OffButton_1; }
	inline void set_OffButton_1(GameObject_t1113636619 * value)
	{
		___OffButton_1 = value;
		Il2CppCodeGenWriteBarrier((&___OffButton_1), value);
	}

	inline static int32_t get_offset_of_Scroller_2() { return static_cast<int32_t>(offsetof(CategorySelector_t478322649, ___Scroller_2)); }
	inline GameObject_t1113636619 * get_Scroller_2() const { return ___Scroller_2; }
	inline GameObject_t1113636619 ** get_address_of_Scroller_2() { return &___Scroller_2; }
	inline void set_Scroller_2(GameObject_t1113636619 * value)
	{
		___Scroller_2 = value;
		Il2CppCodeGenWriteBarrier((&___Scroller_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORYSELECTOR_T478322649_H
#ifndef U3CLOADTHUMBNAILROUTINEU3EC__ITERATOR0_T1787701164_H
#define U3CLOADTHUMBNAILROUTINEU3EC__ITERATOR0_T1787701164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.ArtizPreviewButton/<LoadThumbnailRoutine>c__Iterator0
struct  U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BMJ.M360.Artiz.ArtizPreviewButton/<LoadThumbnailRoutine>c__Iterator0::<ThumbnailRequest>__0
	UnityWebRequest_t463507806 * ___U3CThumbnailRequestU3E__0_0;
	// BMJ.M360.Artiz.ArtizPreviewButton BMJ.M360.Artiz.ArtizPreviewButton/<LoadThumbnailRoutine>c__Iterator0::$this
	ArtizPreviewButton_t1230103917 * ___U24this_1;
	// System.Object BMJ.M360.Artiz.ArtizPreviewButton/<LoadThumbnailRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean BMJ.M360.Artiz.ArtizPreviewButton/<LoadThumbnailRoutine>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 BMJ.M360.Artiz.ArtizPreviewButton/<LoadThumbnailRoutine>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CThumbnailRequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164, ___U3CThumbnailRequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CThumbnailRequestU3E__0_0() const { return ___U3CThumbnailRequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CThumbnailRequestU3E__0_0() { return &___U3CThumbnailRequestU3E__0_0; }
	inline void set_U3CThumbnailRequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CThumbnailRequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CThumbnailRequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164, ___U24this_1)); }
	inline ArtizPreviewButton_t1230103917 * get_U24this_1() const { return ___U24this_1; }
	inline ArtizPreviewButton_t1230103917 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ArtizPreviewButton_t1230103917 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADTHUMBNAILROUTINEU3EC__ITERATOR0_T1787701164_H
#ifndef SERIALIZABLEPREVIEWPROPERTIES_T119401764_H
#define SERIALIZABLEPREVIEWPROPERTIES_T119401764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.AssetsManager/SerializablePreviewProperties
struct  SerializablePreviewProperties_t119401764  : public RuntimeObject
{
public:
	// System.Int32 BMJ.M360.Artiz.AssetsManager/SerializablePreviewProperties::Version
	int32_t ___Version_0;
	// System.Collections.Generic.List`1<BMJ.M360.Artiz.ArtizPreviewProperty> BMJ.M360.Artiz.AssetsManager/SerializablePreviewProperties::PreviewPropertyList
	List_1_t2968708851 * ___PreviewPropertyList_1;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(SerializablePreviewProperties_t119401764, ___Version_0)); }
	inline int32_t get_Version_0() const { return ___Version_0; }
	inline int32_t* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(int32_t value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_PreviewPropertyList_1() { return static_cast<int32_t>(offsetof(SerializablePreviewProperties_t119401764, ___PreviewPropertyList_1)); }
	inline List_1_t2968708851 * get_PreviewPropertyList_1() const { return ___PreviewPropertyList_1; }
	inline List_1_t2968708851 ** get_address_of_PreviewPropertyList_1() { return &___PreviewPropertyList_1; }
	inline void set_PreviewPropertyList_1(List_1_t2968708851 * value)
	{
		___PreviewPropertyList_1 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewPropertyList_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPREVIEWPROPERTIES_T119401764_H
#ifndef U3CDEBUGUPDATEROUTINEU3EC__ITERATOR0_T3894649762_H
#define U3CDEBUGUPDATEROUTINEU3EC__ITERATOR0_T3894649762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.AssetsManager/<DebugUpdateRoutine>c__Iterator0
struct  U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762  : public RuntimeObject
{
public:
	// System.Single BMJ.M360.Artiz.AssetsManager/<DebugUpdateRoutine>c__Iterator0::<Timestamp>__0
	float ___U3CTimestampU3E__0_0;
	// System.Object BMJ.M360.Artiz.AssetsManager/<DebugUpdateRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BMJ.M360.Artiz.AssetsManager/<DebugUpdateRoutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BMJ.M360.Artiz.AssetsManager/<DebugUpdateRoutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U3CTimestampU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762, ___U3CTimestampU3E__0_0)); }
	inline float get_U3CTimestampU3E__0_0() const { return ___U3CTimestampU3E__0_0; }
	inline float* get_address_of_U3CTimestampU3E__0_0() { return &___U3CTimestampU3E__0_0; }
	inline void set_U3CTimestampU3E__0_0(float value)
	{
		___U3CTimestampU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEBUGUPDATEROUTINEU3EC__ITERATOR0_T3894649762_H
#ifndef U3CLOADPREVIEWBUTTONSROUTINEU3EC__ITERATOR1_T1217096687_H
#define U3CLOADPREVIEWBUTTONSROUTINEU3EC__ITERATOR1_T1217096687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1
struct  U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::<GetAssetConfigurationRequest>__0
	UnityWebRequest_t463507806 * ___U3CGetAssetConfigurationRequestU3E__0_0;
	// UnityEngine.Events.UnityAction`1<BMJ.M360.Artiz.ArtizPreviewProperty> BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::PreloadButtonAction
	UnityAction_1_t2081470250 * ___PreloadButtonAction_1;
	// UnityEngine.Events.UnityAction BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::LoadPreviewAssetsAction
	UnityAction_t3245792599 * ___LoadPreviewAssetsAction_2;
	// BMJ.M360.Artiz.AssetsManager BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::$this
	AssetsManager_t3069477283 * ___U24this_3;
	// System.Object BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 BMJ.M360.Artiz.AssetsManager/<LoadPreviewButtonsRoutine>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CGetAssetConfigurationRequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___U3CGetAssetConfigurationRequestU3E__0_0)); }
	inline UnityWebRequest_t463507806 * get_U3CGetAssetConfigurationRequestU3E__0_0() const { return ___U3CGetAssetConfigurationRequestU3E__0_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CGetAssetConfigurationRequestU3E__0_0() { return &___U3CGetAssetConfigurationRequestU3E__0_0; }
	inline void set_U3CGetAssetConfigurationRequestU3E__0_0(UnityWebRequest_t463507806 * value)
	{
		___U3CGetAssetConfigurationRequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetAssetConfigurationRequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_PreloadButtonAction_1() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___PreloadButtonAction_1)); }
	inline UnityAction_1_t2081470250 * get_PreloadButtonAction_1() const { return ___PreloadButtonAction_1; }
	inline UnityAction_1_t2081470250 ** get_address_of_PreloadButtonAction_1() { return &___PreloadButtonAction_1; }
	inline void set_PreloadButtonAction_1(UnityAction_1_t2081470250 * value)
	{
		___PreloadButtonAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___PreloadButtonAction_1), value);
	}

	inline static int32_t get_offset_of_LoadPreviewAssetsAction_2() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___LoadPreviewAssetsAction_2)); }
	inline UnityAction_t3245792599 * get_LoadPreviewAssetsAction_2() const { return ___LoadPreviewAssetsAction_2; }
	inline UnityAction_t3245792599 ** get_address_of_LoadPreviewAssetsAction_2() { return &___LoadPreviewAssetsAction_2; }
	inline void set_LoadPreviewAssetsAction_2(UnityAction_t3245792599 * value)
	{
		___LoadPreviewAssetsAction_2 = value;
		Il2CppCodeGenWriteBarrier((&___LoadPreviewAssetsAction_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___U24this_3)); }
	inline AssetsManager_t3069477283 * get_U24this_3() const { return ___U24this_3; }
	inline AssetsManager_t3069477283 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(AssetsManager_t3069477283 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPREVIEWBUTTONSROUTINEU3EC__ITERATOR1_T1217096687_H
#ifndef U3CSETUPROUTINEU3EC__ITERATOR0_T1251959289_H
#define U3CSETUPROUTINEU3EC__ITERATOR0_T1251959289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0
struct  U3CSetupRoutineU3Ec__Iterator0_t1251959289  : public RuntimeObject
{
public:
	// BMJ.M360.Artiz.LayoutVertical[] BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0::$locvar0
	LayoutVerticalU5BU5D_t1847752332* ___U24locvar0_0;
	// System.Int32 BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// BMJ.M360.Artiz.InfoPanelController BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0::$this
	InfoPanelController_t539477300 * ___U24this_2;
	// System.Object BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 BMJ.M360.Artiz.InfoPanelController/<SetupRoutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CSetupRoutineU3Ec__Iterator0_t1251959289, ___U24locvar0_0)); }
	inline LayoutVerticalU5BU5D_t1847752332* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline LayoutVerticalU5BU5D_t1847752332** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(LayoutVerticalU5BU5D_t1847752332* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CSetupRoutineU3Ec__Iterator0_t1251959289, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSetupRoutineU3Ec__Iterator0_t1251959289, ___U24this_2)); }
	inline InfoPanelController_t539477300 * get_U24this_2() const { return ___U24this_2; }
	inline InfoPanelController_t539477300 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(InfoPanelController_t539477300 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSetupRoutineU3Ec__Iterator0_t1251959289, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSetupRoutineU3Ec__Iterator0_t1251959289, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSetupRoutineU3Ec__Iterator0_t1251959289, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETUPROUTINEU3EC__ITERATOR0_T1251959289_H
#ifndef U3CUPDATEROUTINEU3EC__ITERATOR0_T3289854625_H
#define U3CUPDATEROUTINEU3EC__ITERATOR0_T3289854625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.CustomFitScroller/<UpdateRoutine>c__Iterator0
struct  U3CUpdateRoutineU3Ec__Iterator0_t3289854625  : public RuntimeObject
{
public:
	// BMJ.M360.Artiz.CustomFitScroller BMJ.M360.Artiz.CustomFitScroller/<UpdateRoutine>c__Iterator0::$this
	CustomFitScroller_t3272853734 * ___U24this_0;
	// System.Object BMJ.M360.Artiz.CustomFitScroller/<UpdateRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BMJ.M360.Artiz.CustomFitScroller/<UpdateRoutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BMJ.M360.Artiz.CustomFitScroller/<UpdateRoutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t3289854625, ___U24this_0)); }
	inline CustomFitScroller_t3272853734 * get_U24this_0() const { return ___U24this_0; }
	inline CustomFitScroller_t3272853734 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CustomFitScroller_t3272853734 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t3289854625, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t3289854625, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t3289854625, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEROUTINEU3EC__ITERATOR0_T3289854625_H
#ifndef CATEGORYSELECTOR_T2068481753_H
#define CATEGORYSELECTOR_T2068481753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.Scroller.ScrollingController/CategorySelector
struct  CategorySelector_t2068481753  : public RuntimeObject
{
public:
	// UnityEngine.GameObject BMJ.M360.Artiz.Scroller.ScrollingController/CategorySelector::OnButton
	GameObject_t1113636619 * ___OnButton_0;
	// UnityEngine.GameObject BMJ.M360.Artiz.Scroller.ScrollingController/CategorySelector::OffButton
	GameObject_t1113636619 * ___OffButton_1;
	// UnityEngine.GameObject BMJ.M360.Artiz.Scroller.ScrollingController/CategorySelector::Scroller
	GameObject_t1113636619 * ___Scroller_2;

public:
	inline static int32_t get_offset_of_OnButton_0() { return static_cast<int32_t>(offsetof(CategorySelector_t2068481753, ___OnButton_0)); }
	inline GameObject_t1113636619 * get_OnButton_0() const { return ___OnButton_0; }
	inline GameObject_t1113636619 ** get_address_of_OnButton_0() { return &___OnButton_0; }
	inline void set_OnButton_0(GameObject_t1113636619 * value)
	{
		___OnButton_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnButton_0), value);
	}

	inline static int32_t get_offset_of_OffButton_1() { return static_cast<int32_t>(offsetof(CategorySelector_t2068481753, ___OffButton_1)); }
	inline GameObject_t1113636619 * get_OffButton_1() const { return ___OffButton_1; }
	inline GameObject_t1113636619 ** get_address_of_OffButton_1() { return &___OffButton_1; }
	inline void set_OffButton_1(GameObject_t1113636619 * value)
	{
		___OffButton_1 = value;
		Il2CppCodeGenWriteBarrier((&___OffButton_1), value);
	}

	inline static int32_t get_offset_of_Scroller_2() { return static_cast<int32_t>(offsetof(CategorySelector_t2068481753, ___Scroller_2)); }
	inline GameObject_t1113636619 * get_Scroller_2() const { return ___Scroller_2; }
	inline GameObject_t1113636619 ** get_address_of_Scroller_2() { return &___Scroller_2; }
	inline void set_Scroller_2(GameObject_t1113636619 * value)
	{
		___Scroller_2 = value;
		Il2CppCodeGenWriteBarrier((&___Scroller_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATEGORYSELECTOR_T2068481753_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef U3CUPDATEROUTINEU3EC__ITERATOR0_T1920255547_H
#define U3CUPDATEROUTINEU3EC__ITERATOR0_T1920255547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.HDRIRotator/<UpdateRoutine>c__Iterator0
struct  U3CUpdateRoutineU3Ec__Iterator0_t1920255547  : public RuntimeObject
{
public:
	// BMJ.M360.Artiz.HDRIRotator BMJ.M360.Artiz.HDRIRotator/<UpdateRoutine>c__Iterator0::$this
	HDRIRotator_t2414495732 * ___U24this_0;
	// System.Object BMJ.M360.Artiz.HDRIRotator/<UpdateRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BMJ.M360.Artiz.HDRIRotator/<UpdateRoutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BMJ.M360.Artiz.HDRIRotator/<UpdateRoutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1920255547, ___U24this_0)); }
	inline HDRIRotator_t2414495732 * get_U24this_0() const { return ___U24this_0; }
	inline HDRIRotator_t2414495732 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(HDRIRotator_t2414495732 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1920255547, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1920255547, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1920255547, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEROUTINEU3EC__ITERATOR0_T1920255547_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef U3CLOADHDRIU3EC__ITERATOR0_T21638791_H
#define U3CLOADHDRIU3EC__ITERATOR0_T21638791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0
struct  U3CLoadHDRIU3Ec__Iterator0_t21638791  : public RuntimeObject
{
public:
	// System.String BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::SpriteHDRILink
	String_t* ___SpriteHDRILink_0;
	// UnityEngine.WWW BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::<HDRIRequest>__0
	WWW_t3688466362 * ___U3CHDRIRequestU3E__0_1;
	// UnityEngine.Vector3 BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::HDRIRotationOffset
	Vector3_t3722313464  ___HDRIRotationOffset_2;
	// UnityEngine.Transform BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::PreviewButton
	Transform_t3600365921 * ___PreviewButton_3;
	// System.Int32 BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::StartIndex
	int32_t ___StartIndex_4;
	// BMJ.M360.Artiz.PreviewManager BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::$this
	PreviewManager_t2255819439 * ___U24this_5;
	// System.Object BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 BMJ.M360.Artiz.PreviewManager/<LoadHDRI>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_SpriteHDRILink_0() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___SpriteHDRILink_0)); }
	inline String_t* get_SpriteHDRILink_0() const { return ___SpriteHDRILink_0; }
	inline String_t** get_address_of_SpriteHDRILink_0() { return &___SpriteHDRILink_0; }
	inline void set_SpriteHDRILink_0(String_t* value)
	{
		___SpriteHDRILink_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteHDRILink_0), value);
	}

	inline static int32_t get_offset_of_U3CHDRIRequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___U3CHDRIRequestU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CHDRIRequestU3E__0_1() const { return ___U3CHDRIRequestU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CHDRIRequestU3E__0_1() { return &___U3CHDRIRequestU3E__0_1; }
	inline void set_U3CHDRIRequestU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CHDRIRequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHDRIRequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_HDRIRotationOffset_2() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___HDRIRotationOffset_2)); }
	inline Vector3_t3722313464  get_HDRIRotationOffset_2() const { return ___HDRIRotationOffset_2; }
	inline Vector3_t3722313464 * get_address_of_HDRIRotationOffset_2() { return &___HDRIRotationOffset_2; }
	inline void set_HDRIRotationOffset_2(Vector3_t3722313464  value)
	{
		___HDRIRotationOffset_2 = value;
	}

	inline static int32_t get_offset_of_PreviewButton_3() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___PreviewButton_3)); }
	inline Transform_t3600365921 * get_PreviewButton_3() const { return ___PreviewButton_3; }
	inline Transform_t3600365921 ** get_address_of_PreviewButton_3() { return &___PreviewButton_3; }
	inline void set_PreviewButton_3(Transform_t3600365921 * value)
	{
		___PreviewButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewButton_3), value);
	}

	inline static int32_t get_offset_of_StartIndex_4() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___StartIndex_4)); }
	inline int32_t get_StartIndex_4() const { return ___StartIndex_4; }
	inline int32_t* get_address_of_StartIndex_4() { return &___StartIndex_4; }
	inline void set_StartIndex_4(int32_t value)
	{
		___StartIndex_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___U24this_5)); }
	inline PreviewManager_t2255819439 * get_U24this_5() const { return ___U24this_5; }
	inline PreviewManager_t2255819439 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(PreviewManager_t2255819439 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CLoadHDRIU3Ec__Iterator0_t21638791, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADHDRIU3EC__ITERATOR0_T21638791_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef VRSTATE_T4021279362_H
#define VRSTATE_T4021279362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.CustomCamera.AxisRotationCamera/VRState
struct  VRState_t4021279362 
{
public:
	// System.Int32 BMJ.M360.CustomCamera.AxisRotationCamera/VRState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VRState_t4021279362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRSTATE_T4021279362_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef ARTIZPREVIEWPROPERTY_T1496634109_H
#define ARTIZPREVIEWPROPERTY_T1496634109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.ArtizPreviewProperty
struct  ArtizPreviewProperty_t1496634109  : public RuntimeObject
{
public:
	// System.String BMJ.M360.Artiz.ArtizPreviewProperty::UniqueID
	String_t* ___UniqueID_0;
	// System.String BMJ.M360.Artiz.ArtizPreviewProperty::DisplayName
	String_t* ___DisplayName_1;
	// System.String BMJ.M360.Artiz.ArtizPreviewProperty::Description
	String_t* ___Description_2;
	// System.Collections.Generic.List`1<System.String> BMJ.M360.Artiz.ArtizPreviewProperty::SpriteThumbnailLink
	List_1_t3319525431 * ___SpriteThumbnailLink_3;
	// System.Int32 BMJ.M360.Artiz.ArtizPreviewProperty::StartIndex
	int32_t ___StartIndex_4;
	// System.String BMJ.M360.Artiz.ArtizPreviewProperty::SpriteHDRILink
	String_t* ___SpriteHDRILink_5;
	// System.Int32 BMJ.M360.Artiz.ArtizPreviewProperty::CategoryIndex
	int32_t ___CategoryIndex_6;
	// UnityEngine.Vector3 BMJ.M360.Artiz.ArtizPreviewProperty::HDRIRotationOffset
	Vector3_t3722313464  ___HDRIRotationOffset_7;

public:
	inline static int32_t get_offset_of_UniqueID_0() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___UniqueID_0)); }
	inline String_t* get_UniqueID_0() const { return ___UniqueID_0; }
	inline String_t** get_address_of_UniqueID_0() { return &___UniqueID_0; }
	inline void set_UniqueID_0(String_t* value)
	{
		___UniqueID_0 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueID_0), value);
	}

	inline static int32_t get_offset_of_DisplayName_1() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___DisplayName_1)); }
	inline String_t* get_DisplayName_1() const { return ___DisplayName_1; }
	inline String_t** get_address_of_DisplayName_1() { return &___DisplayName_1; }
	inline void set_DisplayName_1(String_t* value)
	{
		___DisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___DisplayName_1), value);
	}

	inline static int32_t get_offset_of_Description_2() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___Description_2)); }
	inline String_t* get_Description_2() const { return ___Description_2; }
	inline String_t** get_address_of_Description_2() { return &___Description_2; }
	inline void set_Description_2(String_t* value)
	{
		___Description_2 = value;
		Il2CppCodeGenWriteBarrier((&___Description_2), value);
	}

	inline static int32_t get_offset_of_SpriteThumbnailLink_3() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___SpriteThumbnailLink_3)); }
	inline List_1_t3319525431 * get_SpriteThumbnailLink_3() const { return ___SpriteThumbnailLink_3; }
	inline List_1_t3319525431 ** get_address_of_SpriteThumbnailLink_3() { return &___SpriteThumbnailLink_3; }
	inline void set_SpriteThumbnailLink_3(List_1_t3319525431 * value)
	{
		___SpriteThumbnailLink_3 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteThumbnailLink_3), value);
	}

	inline static int32_t get_offset_of_StartIndex_4() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___StartIndex_4)); }
	inline int32_t get_StartIndex_4() const { return ___StartIndex_4; }
	inline int32_t* get_address_of_StartIndex_4() { return &___StartIndex_4; }
	inline void set_StartIndex_4(int32_t value)
	{
		___StartIndex_4 = value;
	}

	inline static int32_t get_offset_of_SpriteHDRILink_5() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___SpriteHDRILink_5)); }
	inline String_t* get_SpriteHDRILink_5() const { return ___SpriteHDRILink_5; }
	inline String_t** get_address_of_SpriteHDRILink_5() { return &___SpriteHDRILink_5; }
	inline void set_SpriteHDRILink_5(String_t* value)
	{
		___SpriteHDRILink_5 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteHDRILink_5), value);
	}

	inline static int32_t get_offset_of_CategoryIndex_6() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___CategoryIndex_6)); }
	inline int32_t get_CategoryIndex_6() const { return ___CategoryIndex_6; }
	inline int32_t* get_address_of_CategoryIndex_6() { return &___CategoryIndex_6; }
	inline void set_CategoryIndex_6(int32_t value)
	{
		___CategoryIndex_6 = value;
	}

	inline static int32_t get_offset_of_HDRIRotationOffset_7() { return static_cast<int32_t>(offsetof(ArtizPreviewProperty_t1496634109, ___HDRIRotationOffset_7)); }
	inline Vector3_t3722313464  get_HDRIRotationOffset_7() const { return ___HDRIRotationOffset_7; }
	inline Vector3_t3722313464 * get_address_of_HDRIRotationOffset_7() { return &___HDRIRotationOffset_7; }
	inline void set_HDRIRotationOffset_7(Vector3_t3722313464  value)
	{
		___HDRIRotationOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTIZPREVIEWPROPERTY_T1496634109_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef U3CMONITORVRSTATUSROUTINEU3EC__ITERATOR0_T1319999632_H
#define U3CMONITORVRSTATUSROUTINEU3EC__ITERATOR0_T1319999632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0
struct  U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632  : public RuntimeObject
{
public:
	// BMJ.M360.CustomCamera.AxisRotationCamera/VRState BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0::$locvar0
	int32_t ___U24locvar0_0;
	// BMJ.M360.CustomCamera.AxisRotationCamera/VRState BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// BMJ.M360.CustomCamera.AxisRotationCamera BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0::$this
	AxisRotationCamera_t2455264440 * ___U24this_2;
	// System.Object BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 BMJ.M360.CustomCamera.AxisRotationCamera/<MonitorVRStatusRoutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632, ___U24locvar0_0)); }
	inline int32_t get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline int32_t* get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(int32_t value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632, ___U24this_2)); }
	inline AxisRotationCamera_t2455264440 * get_U24this_2() const { return ___U24this_2; }
	inline AxisRotationCamera_t2455264440 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(AxisRotationCamera_t2455264440 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMONITORVRSTATUSROUTINEU3EC__ITERATOR0_T1319999632_H
#ifndef U3CVRROUTINEU3EC__ITERATOR1_T2077243264_H
#define U3CVRROUTINEU3EC__ITERATOR1_T2077243264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1
struct  U3CVRRoutineU3Ec__Iterator1_t2077243264  : public RuntimeObject
{
public:
	// UnityEngine.Ray BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::<VRRayCast>__0
	Ray_t3785851493  ___U3CVRRayCastU3E__0_0;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::<timestamp>__0
	float ___U3CtimestampU3E__0_1;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::<buttonhit>__0
	bool ___U3CbuttonhitU3E__0_2;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::<lastHitObject>__0
	GameObject_t1113636619 * ___U3ClastHitObjectU3E__0_3;
	// UnityEngine.RaycastHit BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::<raycastHit>__1
	RaycastHit_t1056001966  ___U3CraycastHitU3E__1_4;
	// BMJ.M360.CustomCamera.AxisRotationCamera BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::$this
	AxisRotationCamera_t2455264440 * ___U24this_5;
	// System.Object BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 BMJ.M360.CustomCamera.AxisRotationCamera/<VRRoutine>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CVRRayCastU3E__0_0() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U3CVRRayCastU3E__0_0)); }
	inline Ray_t3785851493  get_U3CVRRayCastU3E__0_0() const { return ___U3CVRRayCastU3E__0_0; }
	inline Ray_t3785851493 * get_address_of_U3CVRRayCastU3E__0_0() { return &___U3CVRRayCastU3E__0_0; }
	inline void set_U3CVRRayCastU3E__0_0(Ray_t3785851493  value)
	{
		___U3CVRRayCastU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtimestampU3E__0_1() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U3CtimestampU3E__0_1)); }
	inline float get_U3CtimestampU3E__0_1() const { return ___U3CtimestampU3E__0_1; }
	inline float* get_address_of_U3CtimestampU3E__0_1() { return &___U3CtimestampU3E__0_1; }
	inline void set_U3CtimestampU3E__0_1(float value)
	{
		___U3CtimestampU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonhitU3E__0_2() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U3CbuttonhitU3E__0_2)); }
	inline bool get_U3CbuttonhitU3E__0_2() const { return ___U3CbuttonhitU3E__0_2; }
	inline bool* get_address_of_U3CbuttonhitU3E__0_2() { return &___U3CbuttonhitU3E__0_2; }
	inline void set_U3CbuttonhitU3E__0_2(bool value)
	{
		___U3CbuttonhitU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3ClastHitObjectU3E__0_3() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U3ClastHitObjectU3E__0_3)); }
	inline GameObject_t1113636619 * get_U3ClastHitObjectU3E__0_3() const { return ___U3ClastHitObjectU3E__0_3; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastHitObjectU3E__0_3() { return &___U3ClastHitObjectU3E__0_3; }
	inline void set_U3ClastHitObjectU3E__0_3(GameObject_t1113636619 * value)
	{
		___U3ClastHitObjectU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastHitObjectU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CraycastHitU3E__1_4() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U3CraycastHitU3E__1_4)); }
	inline RaycastHit_t1056001966  get_U3CraycastHitU3E__1_4() const { return ___U3CraycastHitU3E__1_4; }
	inline RaycastHit_t1056001966 * get_address_of_U3CraycastHitU3E__1_4() { return &___U3CraycastHitU3E__1_4; }
	inline void set_U3CraycastHitU3E__1_4(RaycastHit_t1056001966  value)
	{
		___U3CraycastHitU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U24this_5)); }
	inline AxisRotationCamera_t2455264440 * get_U24this_5() const { return ___U24this_5; }
	inline AxisRotationCamera_t2455264440 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(AxisRotationCamera_t2455264440 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CVRRoutineU3Ec__Iterator1_t2077243264, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVRROUTINEU3EC__ITERATOR1_T2077243264_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef U3CUPDATEROUTINEU3EC__ITERATOR0_T1392784015_H
#define U3CUPDATEROUTINEU3EC__ITERATOR0_T1392784015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0
struct  U3CUpdateRoutineU3Ec__Iterator0_t1392784015  : public RuntimeObject
{
public:
	// System.Boolean BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::<NavigationButtonHit>__0
	bool ___U3CNavigationButtonHitU3E__0_0;
	// System.Single BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::<CountdownTimestamp>__0
	float ___U3CCountdownTimestampU3E__0_1;
	// System.Single BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::<CurrentTimestamp>__0
	float ___U3CCurrentTimestampU3E__0_2;
	// BMJ.M360.Artiz.NavigationButton BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::<HitNavigationButton>__0
	NavigationButton_t3491644213 * ___U3CHitNavigationButtonU3E__0_3;
	// UnityEngine.Ray BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::<Raycast>__1
	Ray_t3785851493  ___U3CRaycastU3E__1_4;
	// UnityEngine.RaycastHit BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::<HitInfo>__1
	RaycastHit_t1056001966  ___U3CHitInfoU3E__1_5;
	// BMJ.M360.Artiz.NavigatableHDRI BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::$this
	NavigatableHDRI_t1695953284 * ___U24this_6;
	// System.Object BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 BMJ.M360.Artiz.NavigatableHDRI/<UpdateRoutine>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CNavigationButtonHitU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U3CNavigationButtonHitU3E__0_0)); }
	inline bool get_U3CNavigationButtonHitU3E__0_0() const { return ___U3CNavigationButtonHitU3E__0_0; }
	inline bool* get_address_of_U3CNavigationButtonHitU3E__0_0() { return &___U3CNavigationButtonHitU3E__0_0; }
	inline void set_U3CNavigationButtonHitU3E__0_0(bool value)
	{
		___U3CNavigationButtonHitU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CCountdownTimestampU3E__0_1() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U3CCountdownTimestampU3E__0_1)); }
	inline float get_U3CCountdownTimestampU3E__0_1() const { return ___U3CCountdownTimestampU3E__0_1; }
	inline float* get_address_of_U3CCountdownTimestampU3E__0_1() { return &___U3CCountdownTimestampU3E__0_1; }
	inline void set_U3CCountdownTimestampU3E__0_1(float value)
	{
		___U3CCountdownTimestampU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentTimestampU3E__0_2() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U3CCurrentTimestampU3E__0_2)); }
	inline float get_U3CCurrentTimestampU3E__0_2() const { return ___U3CCurrentTimestampU3E__0_2; }
	inline float* get_address_of_U3CCurrentTimestampU3E__0_2() { return &___U3CCurrentTimestampU3E__0_2; }
	inline void set_U3CCurrentTimestampU3E__0_2(float value)
	{
		___U3CCurrentTimestampU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CHitNavigationButtonU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U3CHitNavigationButtonU3E__0_3)); }
	inline NavigationButton_t3491644213 * get_U3CHitNavigationButtonU3E__0_3() const { return ___U3CHitNavigationButtonU3E__0_3; }
	inline NavigationButton_t3491644213 ** get_address_of_U3CHitNavigationButtonU3E__0_3() { return &___U3CHitNavigationButtonU3E__0_3; }
	inline void set_U3CHitNavigationButtonU3E__0_3(NavigationButton_t3491644213 * value)
	{
		___U3CHitNavigationButtonU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHitNavigationButtonU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CRaycastU3E__1_4() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U3CRaycastU3E__1_4)); }
	inline Ray_t3785851493  get_U3CRaycastU3E__1_4() const { return ___U3CRaycastU3E__1_4; }
	inline Ray_t3785851493 * get_address_of_U3CRaycastU3E__1_4() { return &___U3CRaycastU3E__1_4; }
	inline void set_U3CRaycastU3E__1_4(Ray_t3785851493  value)
	{
		___U3CRaycastU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CHitInfoU3E__1_5() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U3CHitInfoU3E__1_5)); }
	inline RaycastHit_t1056001966  get_U3CHitInfoU3E__1_5() const { return ___U3CHitInfoU3E__1_5; }
	inline RaycastHit_t1056001966 * get_address_of_U3CHitInfoU3E__1_5() { return &___U3CHitInfoU3E__1_5; }
	inline void set_U3CHitInfoU3E__1_5(RaycastHit_t1056001966  value)
	{
		___U3CHitInfoU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U24this_6)); }
	inline NavigatableHDRI_t1695953284 * get_U24this_6() const { return ___U24this_6; }
	inline NavigatableHDRI_t1695953284 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(NavigatableHDRI_t1695953284 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CUpdateRoutineU3Ec__Iterator0_t1392784015, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEROUTINEU3EC__ITERATOR0_T1392784015_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T520384403_H
#define INSTANCEDMONOBEHAVIOUR_1_T520384403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.AssetsManager>
struct  InstancedMonoBehaviour_1_t520384403  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t520384403_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	AssetsManager_t3069477283 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t520384403_StaticFields, ___Instance_2)); }
	inline AssetsManager_t3069477283 * get_Instance_2() const { return ___Instance_2; }
	inline AssetsManager_t3069477283 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(AssetsManager_t3069477283 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T520384403_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T4001693855_H
#define INSTANCEDMONOBEHAVIOUR_1_T4001693855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.PreviewManager>
struct  InstancedMonoBehaviour_1_t4001693855  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t4001693855_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	PreviewManager_t2255819439 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t4001693855_StaticFields, ___Instance_2)); }
	inline PreviewManager_t2255819439 * get_Instance_2() const { return ___Instance_2; }
	inline PreviewManager_t2255819439 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(PreviewManager_t2255819439 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T4001693855_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T1719326385_H
#define INSTANCEDMONOBEHAVIOUR_1_T1719326385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.PreviewButtonsManager>
struct  InstancedMonoBehaviour_1_t1719326385  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t1719326385_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	PreviewButtonsManager_t4268419265 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t1719326385_StaticFields, ___Instance_2)); }
	inline PreviewButtonsManager_t4268419265 * get_Instance_2() const { return ___Instance_2; }
	inline PreviewButtonsManager_t4268419265 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(PreviewButtonsManager_t4268419265 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T1719326385_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T210030159_H
#define INSTANCEDMONOBEHAVIOUR_1_T210030159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.PreviewButtonsManager2>
struct  InstancedMonoBehaviour_1_t210030159  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t210030159_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	PreviewButtonsManager2_t2759123039 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t210030159_StaticFields, ___Instance_2)); }
	inline PreviewButtonsManager2_t2759123039 * get_Instance_2() const { return ___Instance_2; }
	inline PreviewButtonsManager2_t2759123039 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(PreviewButtonsManager2_t2759123039 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T210030159_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T3728564720_H
#define INSTANCEDMONOBEHAVIOUR_1_T3728564720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.Scroller.ScrollingController>
struct  InstancedMonoBehaviour_1_t3728564720  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t3728564720_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	ScrollingController_t1982690304 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t3728564720_StaticFields, ___Instance_2)); }
	inline ScrollingController_t1982690304 * get_Instance_2() const { return ___Instance_2; }
	inline ScrollingController_t1982690304 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(ScrollingController_t1982690304 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T3728564720_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T287531504_H
#define INSTANCEDMONOBEHAVIOUR_1_T287531504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.Scroller.ScrollingController2>
struct  InstancedMonoBehaviour_1_t287531504  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t287531504_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	ScrollingController2_t2836624384 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t287531504_StaticFields, ___Instance_2)); }
	inline ScrollingController2_t2836624384 * get_Instance_2() const { return ___Instance_2; }
	inline ScrollingController2_t2836624384 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(ScrollingController2_t2836624384 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T287531504_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T1790354669_H
#define INSTANCEDMONOBEHAVIOUR_1_T1790354669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.ToggleViewController>
struct  InstancedMonoBehaviour_1_t1790354669  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t1790354669_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	ToggleViewController_t44480253 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t1790354669_StaticFields, ___Instance_2)); }
	inline ToggleViewController_t44480253 * get_Instance_2() const { return ___Instance_2; }
	inline ToggleViewController_t44480253 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(ToggleViewController_t44480253 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T1790354669_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T4201138856_H
#define INSTANCEDMONOBEHAVIOUR_1_T4201138856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.CustomCamera.AxisRotationCamera>
struct  InstancedMonoBehaviour_1_t4201138856  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t4201138856_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	AxisRotationCamera_t2455264440 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t4201138856_StaticFields, ___Instance_2)); }
	inline AxisRotationCamera_t2455264440 * get_Instance_2() const { return ___Instance_2; }
	inline AxisRotationCamera_t2455264440 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(AxisRotationCamera_t2455264440 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T4201138856_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_3)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T3242198627_H
#define INSTANCEDMONOBEHAVIOUR_1_T3242198627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.VR.VRExplorerController>
struct  InstancedMonoBehaviour_1_t3242198627  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t3242198627_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	VRExplorerController_t1496324211 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t3242198627_StaticFields, ___Instance_2)); }
	inline VRExplorerController_t1496324211 * get_Instance_2() const { return ___Instance_2; }
	inline VRExplorerController_t1496324211 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(VRExplorerController_t1496324211 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T3242198627_H
#ifndef INSTANCEDMONOBEHAVIOUR_1_T76458259_H
#define INSTANCEDMONOBEHAVIOUR_1_T76458259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.InstancedMonoBehaviour`1<BMJ.M360.Artiz.FractionController>
struct  InstancedMonoBehaviour_1_t76458259  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstancedMonoBehaviour_1_t76458259_StaticFields
{
public:
	// T BMJ.M360.Generic.InstancedMonoBehaviour`1::Instance
	FractionController_t2625551139 * ___Instance_2;

public:
	inline static int32_t get_offset_of_Instance_2() { return static_cast<int32_t>(offsetof(InstancedMonoBehaviour_1_t76458259_StaticFields, ___Instance_2)); }
	inline FractionController_t2625551139 * get_Instance_2() const { return ___Instance_2; }
	inline FractionController_t2625551139 ** get_address_of_Instance_2() { return &___Instance_2; }
	inline void set_Instance_2(FractionController_t2625551139 * value)
	{
		___Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEDMONOBEHAVIOUR_1_T76458259_H
#ifndef CONTROLLER_T2994601017_H
#define CONTROLLER_T2994601017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller
struct  Controller_t2994601017  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Controller::Splash
	GameObject_t1113636619 * ___Splash_2;
	// UnityEngine.GameObject Controller::HomePage
	GameObject_t1113636619 * ___HomePage_3;
	// UnityEngine.GameObject Controller::LandscapeSplash
	GameObject_t1113636619 * ___LandscapeSplash_4;
	// UnityEngine.GameObject Controller::LandscapePanel
	GameObject_t1113636619 * ___LandscapePanel_5;
	// UnityEngine.RectTransform Controller::ScrollView
	RectTransform_t3704657025 * ___ScrollView_6;
	// UnityEngine.Vector3 Controller::MinCache
	Vector3_t3722313464  ___MinCache_7;
	// UnityEngine.Vector3 Controller::MaxCache
	Vector3_t3722313464  ___MaxCache_8;
	// System.Boolean Controller::Toggle
	bool ___Toggle_9;
	// UnityEngine.ScreenOrientation Controller::orientation
	int32_t ___orientation_10;

public:
	inline static int32_t get_offset_of_Splash_2() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___Splash_2)); }
	inline GameObject_t1113636619 * get_Splash_2() const { return ___Splash_2; }
	inline GameObject_t1113636619 ** get_address_of_Splash_2() { return &___Splash_2; }
	inline void set_Splash_2(GameObject_t1113636619 * value)
	{
		___Splash_2 = value;
		Il2CppCodeGenWriteBarrier((&___Splash_2), value);
	}

	inline static int32_t get_offset_of_HomePage_3() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___HomePage_3)); }
	inline GameObject_t1113636619 * get_HomePage_3() const { return ___HomePage_3; }
	inline GameObject_t1113636619 ** get_address_of_HomePage_3() { return &___HomePage_3; }
	inline void set_HomePage_3(GameObject_t1113636619 * value)
	{
		___HomePage_3 = value;
		Il2CppCodeGenWriteBarrier((&___HomePage_3), value);
	}

	inline static int32_t get_offset_of_LandscapeSplash_4() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___LandscapeSplash_4)); }
	inline GameObject_t1113636619 * get_LandscapeSplash_4() const { return ___LandscapeSplash_4; }
	inline GameObject_t1113636619 ** get_address_of_LandscapeSplash_4() { return &___LandscapeSplash_4; }
	inline void set_LandscapeSplash_4(GameObject_t1113636619 * value)
	{
		___LandscapeSplash_4 = value;
		Il2CppCodeGenWriteBarrier((&___LandscapeSplash_4), value);
	}

	inline static int32_t get_offset_of_LandscapePanel_5() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___LandscapePanel_5)); }
	inline GameObject_t1113636619 * get_LandscapePanel_5() const { return ___LandscapePanel_5; }
	inline GameObject_t1113636619 ** get_address_of_LandscapePanel_5() { return &___LandscapePanel_5; }
	inline void set_LandscapePanel_5(GameObject_t1113636619 * value)
	{
		___LandscapePanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___LandscapePanel_5), value);
	}

	inline static int32_t get_offset_of_ScrollView_6() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___ScrollView_6)); }
	inline RectTransform_t3704657025 * get_ScrollView_6() const { return ___ScrollView_6; }
	inline RectTransform_t3704657025 ** get_address_of_ScrollView_6() { return &___ScrollView_6; }
	inline void set_ScrollView_6(RectTransform_t3704657025 * value)
	{
		___ScrollView_6 = value;
		Il2CppCodeGenWriteBarrier((&___ScrollView_6), value);
	}

	inline static int32_t get_offset_of_MinCache_7() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___MinCache_7)); }
	inline Vector3_t3722313464  get_MinCache_7() const { return ___MinCache_7; }
	inline Vector3_t3722313464 * get_address_of_MinCache_7() { return &___MinCache_7; }
	inline void set_MinCache_7(Vector3_t3722313464  value)
	{
		___MinCache_7 = value;
	}

	inline static int32_t get_offset_of_MaxCache_8() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___MaxCache_8)); }
	inline Vector3_t3722313464  get_MaxCache_8() const { return ___MaxCache_8; }
	inline Vector3_t3722313464 * get_address_of_MaxCache_8() { return &___MaxCache_8; }
	inline void set_MaxCache_8(Vector3_t3722313464  value)
	{
		___MaxCache_8 = value;
	}

	inline static int32_t get_offset_of_Toggle_9() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___Toggle_9)); }
	inline bool get_Toggle_9() const { return ___Toggle_9; }
	inline bool* get_address_of_Toggle_9() { return &___Toggle_9; }
	inline void set_Toggle_9(bool value)
	{
		___Toggle_9 = value;
	}

	inline static int32_t get_offset_of_orientation_10() { return static_cast<int32_t>(offsetof(Controller_t2994601017, ___orientation_10)); }
	inline int32_t get_orientation_10() const { return ___orientation_10; }
	inline int32_t* get_address_of_orientation_10() { return &___orientation_10; }
	inline void set_orientation_10(int32_t value)
	{
		___orientation_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER_T2994601017_H
#ifndef NAVIGATIONBUTTON_T3491644213_H
#define NAVIGATIONBUTTON_T3491644213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.NavigationButton
struct  NavigationButton_t3491644213  : public MonoBehaviour_t3962482529
{
public:
	// System.String BMJ.M360.Artiz.NavigationButton::TargetHDRI
	String_t* ___TargetHDRI_2;
	// System.Int32 BMJ.M360.Artiz.NavigationButton::TargetIndex
	int32_t ___TargetIndex_3;
	// UnityEngine.MeshRenderer BMJ.M360.Artiz.NavigationButton::AttachedMeshRenderer
	MeshRenderer_t587009260 * ___AttachedMeshRenderer_4;

public:
	inline static int32_t get_offset_of_TargetHDRI_2() { return static_cast<int32_t>(offsetof(NavigationButton_t3491644213, ___TargetHDRI_2)); }
	inline String_t* get_TargetHDRI_2() const { return ___TargetHDRI_2; }
	inline String_t** get_address_of_TargetHDRI_2() { return &___TargetHDRI_2; }
	inline void set_TargetHDRI_2(String_t* value)
	{
		___TargetHDRI_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetHDRI_2), value);
	}

	inline static int32_t get_offset_of_TargetIndex_3() { return static_cast<int32_t>(offsetof(NavigationButton_t3491644213, ___TargetIndex_3)); }
	inline int32_t get_TargetIndex_3() const { return ___TargetIndex_3; }
	inline int32_t* get_address_of_TargetIndex_3() { return &___TargetIndex_3; }
	inline void set_TargetIndex_3(int32_t value)
	{
		___TargetIndex_3 = value;
	}

	inline static int32_t get_offset_of_AttachedMeshRenderer_4() { return static_cast<int32_t>(offsetof(NavigationButton_t3491644213, ___AttachedMeshRenderer_4)); }
	inline MeshRenderer_t587009260 * get_AttachedMeshRenderer_4() const { return ___AttachedMeshRenderer_4; }
	inline MeshRenderer_t587009260 ** get_address_of_AttachedMeshRenderer_4() { return &___AttachedMeshRenderer_4; }
	inline void set_AttachedMeshRenderer_4(MeshRenderer_t587009260 * value)
	{
		___AttachedMeshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedMeshRenderer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATIONBUTTON_T3491644213_H
#ifndef HDRIROTATOR_T2414495732_H
#define HDRIROTATOR_T2414495732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.HDRIRotator
struct  HDRIRotator_t2414495732  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform BMJ.M360.Artiz.HDRIRotator::PrimarySphere
	Transform_t3600365921 * ___PrimarySphere_2;
	// UnityEngine.Transform BMJ.M360.Artiz.HDRIRotator::FinalTargetPoint
	Transform_t3600365921 * ___FinalTargetPoint_3;
	// UnityEngine.Transform BMJ.M360.Artiz.HDRIRotator::CurrentTargetPoint
	Transform_t3600365921 * ___CurrentTargetPoint_4;
	// System.Single BMJ.M360.Artiz.HDRIRotator::RotationRate
	float ___RotationRate_5;
	// UnityEngine.Vector3 BMJ.M360.Artiz.HDRIRotator::MovementVector
	Vector3_t3722313464  ___MovementVector_6;
	// UnityEngine.Vector3 BMJ.M360.Artiz.HDRIRotator::StartPoint
	Vector3_t3722313464  ___StartPoint_7;

public:
	inline static int32_t get_offset_of_PrimarySphere_2() { return static_cast<int32_t>(offsetof(HDRIRotator_t2414495732, ___PrimarySphere_2)); }
	inline Transform_t3600365921 * get_PrimarySphere_2() const { return ___PrimarySphere_2; }
	inline Transform_t3600365921 ** get_address_of_PrimarySphere_2() { return &___PrimarySphere_2; }
	inline void set_PrimarySphere_2(Transform_t3600365921 * value)
	{
		___PrimarySphere_2 = value;
		Il2CppCodeGenWriteBarrier((&___PrimarySphere_2), value);
	}

	inline static int32_t get_offset_of_FinalTargetPoint_3() { return static_cast<int32_t>(offsetof(HDRIRotator_t2414495732, ___FinalTargetPoint_3)); }
	inline Transform_t3600365921 * get_FinalTargetPoint_3() const { return ___FinalTargetPoint_3; }
	inline Transform_t3600365921 ** get_address_of_FinalTargetPoint_3() { return &___FinalTargetPoint_3; }
	inline void set_FinalTargetPoint_3(Transform_t3600365921 * value)
	{
		___FinalTargetPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___FinalTargetPoint_3), value);
	}

	inline static int32_t get_offset_of_CurrentTargetPoint_4() { return static_cast<int32_t>(offsetof(HDRIRotator_t2414495732, ___CurrentTargetPoint_4)); }
	inline Transform_t3600365921 * get_CurrentTargetPoint_4() const { return ___CurrentTargetPoint_4; }
	inline Transform_t3600365921 ** get_address_of_CurrentTargetPoint_4() { return &___CurrentTargetPoint_4; }
	inline void set_CurrentTargetPoint_4(Transform_t3600365921 * value)
	{
		___CurrentTargetPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentTargetPoint_4), value);
	}

	inline static int32_t get_offset_of_RotationRate_5() { return static_cast<int32_t>(offsetof(HDRIRotator_t2414495732, ___RotationRate_5)); }
	inline float get_RotationRate_5() const { return ___RotationRate_5; }
	inline float* get_address_of_RotationRate_5() { return &___RotationRate_5; }
	inline void set_RotationRate_5(float value)
	{
		___RotationRate_5 = value;
	}

	inline static int32_t get_offset_of_MovementVector_6() { return static_cast<int32_t>(offsetof(HDRIRotator_t2414495732, ___MovementVector_6)); }
	inline Vector3_t3722313464  get_MovementVector_6() const { return ___MovementVector_6; }
	inline Vector3_t3722313464 * get_address_of_MovementVector_6() { return &___MovementVector_6; }
	inline void set_MovementVector_6(Vector3_t3722313464  value)
	{
		___MovementVector_6 = value;
	}

	inline static int32_t get_offset_of_StartPoint_7() { return static_cast<int32_t>(offsetof(HDRIRotator_t2414495732, ___StartPoint_7)); }
	inline Vector3_t3722313464  get_StartPoint_7() const { return ___StartPoint_7; }
	inline Vector3_t3722313464 * get_address_of_StartPoint_7() { return &___StartPoint_7; }
	inline void set_StartPoint_7(Vector3_t3722313464  value)
	{
		___StartPoint_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HDRIROTATOR_T2414495732_H
#ifndef LAYOUTVERTICAL_T1167249937_H
#define LAYOUTVERTICAL_T1167249937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.LayoutVertical
struct  LayoutVertical_t1167249937  : public MonoBehaviour_t3962482529
{
public:
	// System.Single BMJ.M360.Artiz.LayoutVertical::Spacing
	float ___Spacing_2;
	// UnityEngine.RectTransform BMJ.M360.Artiz.LayoutVertical::SourceTransform
	RectTransform_t3704657025 * ___SourceTransform_3;
	// UnityEngine.RectTransform BMJ.M360.Artiz.LayoutVertical::TargetTransform
	RectTransform_t3704657025 * ___TargetTransform_4;
	// UnityEngine.Vector3 BMJ.M360.Artiz.LayoutVertical::LocalPosition
	Vector3_t3722313464  ___LocalPosition_5;
	// System.Boolean BMJ.M360.Artiz.LayoutVertical::SetupComplete
	bool ___SetupComplete_6;
	// UnityEngine.Transform BMJ.M360.Artiz.LayoutVertical::ChildTransform
	Transform_t3600365921 * ___ChildTransform_7;
	// BMJ.M360.Artiz.LayoutVertical BMJ.M360.Artiz.LayoutVertical::ChildLayoutVertical
	LayoutVertical_t1167249937 * ___ChildLayoutVertical_8;
	// System.Boolean BMJ.M360.Artiz.LayoutVertical::DestroyFlag
	bool ___DestroyFlag_9;

public:
	inline static int32_t get_offset_of_Spacing_2() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___Spacing_2)); }
	inline float get_Spacing_2() const { return ___Spacing_2; }
	inline float* get_address_of_Spacing_2() { return &___Spacing_2; }
	inline void set_Spacing_2(float value)
	{
		___Spacing_2 = value;
	}

	inline static int32_t get_offset_of_SourceTransform_3() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___SourceTransform_3)); }
	inline RectTransform_t3704657025 * get_SourceTransform_3() const { return ___SourceTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_SourceTransform_3() { return &___SourceTransform_3; }
	inline void set_SourceTransform_3(RectTransform_t3704657025 * value)
	{
		___SourceTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___SourceTransform_3), value);
	}

	inline static int32_t get_offset_of_TargetTransform_4() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___TargetTransform_4)); }
	inline RectTransform_t3704657025 * get_TargetTransform_4() const { return ___TargetTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_TargetTransform_4() { return &___TargetTransform_4; }
	inline void set_TargetTransform_4(RectTransform_t3704657025 * value)
	{
		___TargetTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTransform_4), value);
	}

	inline static int32_t get_offset_of_LocalPosition_5() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___LocalPosition_5)); }
	inline Vector3_t3722313464  get_LocalPosition_5() const { return ___LocalPosition_5; }
	inline Vector3_t3722313464 * get_address_of_LocalPosition_5() { return &___LocalPosition_5; }
	inline void set_LocalPosition_5(Vector3_t3722313464  value)
	{
		___LocalPosition_5 = value;
	}

	inline static int32_t get_offset_of_SetupComplete_6() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___SetupComplete_6)); }
	inline bool get_SetupComplete_6() const { return ___SetupComplete_6; }
	inline bool* get_address_of_SetupComplete_6() { return &___SetupComplete_6; }
	inline void set_SetupComplete_6(bool value)
	{
		___SetupComplete_6 = value;
	}

	inline static int32_t get_offset_of_ChildTransform_7() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___ChildTransform_7)); }
	inline Transform_t3600365921 * get_ChildTransform_7() const { return ___ChildTransform_7; }
	inline Transform_t3600365921 ** get_address_of_ChildTransform_7() { return &___ChildTransform_7; }
	inline void set_ChildTransform_7(Transform_t3600365921 * value)
	{
		___ChildTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___ChildTransform_7), value);
	}

	inline static int32_t get_offset_of_ChildLayoutVertical_8() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___ChildLayoutVertical_8)); }
	inline LayoutVertical_t1167249937 * get_ChildLayoutVertical_8() const { return ___ChildLayoutVertical_8; }
	inline LayoutVertical_t1167249937 ** get_address_of_ChildLayoutVertical_8() { return &___ChildLayoutVertical_8; }
	inline void set_ChildLayoutVertical_8(LayoutVertical_t1167249937 * value)
	{
		___ChildLayoutVertical_8 = value;
		Il2CppCodeGenWriteBarrier((&___ChildLayoutVertical_8), value);
	}

	inline static int32_t get_offset_of_DestroyFlag_9() { return static_cast<int32_t>(offsetof(LayoutVertical_t1167249937, ___DestroyFlag_9)); }
	inline bool get_DestroyFlag_9() const { return ___DestroyFlag_9; }
	inline bool* get_address_of_DestroyFlag_9() { return &___DestroyFlag_9; }
	inline void set_DestroyFlag_9(bool value)
	{
		___DestroyFlag_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTVERTICAL_T1167249937_H
#ifndef REGULARHDRI_T2998726886_H
#define REGULARHDRI_T2998726886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.RegularHDRI
struct  RegularHDRI_t2998726886  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGULARHDRI_T2998726886_H
#ifndef CARDBOARDUI_T365955905_H
#define CARDBOARDUI_T365955905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardUI
struct  CardboardUI_t365955905  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CardboardUI::LogoImage
	GameObject_t1113636619 * ___LogoImage_2;
	// UnityEngine.GameObject CardboardUI::Selection
	GameObject_t1113636619 * ___Selection_3;
	// UnityEngine.Vector3 CardboardUI::vector
	Vector3_t3722313464  ___vector_4;
	// UnityEngine.Vector3 CardboardUI::vectors
	Vector3_t3722313464  ___vectors_5;

public:
	inline static int32_t get_offset_of_LogoImage_2() { return static_cast<int32_t>(offsetof(CardboardUI_t365955905, ___LogoImage_2)); }
	inline GameObject_t1113636619 * get_LogoImage_2() const { return ___LogoImage_2; }
	inline GameObject_t1113636619 ** get_address_of_LogoImage_2() { return &___LogoImage_2; }
	inline void set_LogoImage_2(GameObject_t1113636619 * value)
	{
		___LogoImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___LogoImage_2), value);
	}

	inline static int32_t get_offset_of_Selection_3() { return static_cast<int32_t>(offsetof(CardboardUI_t365955905, ___Selection_3)); }
	inline GameObject_t1113636619 * get_Selection_3() const { return ___Selection_3; }
	inline GameObject_t1113636619 ** get_address_of_Selection_3() { return &___Selection_3; }
	inline void set_Selection_3(GameObject_t1113636619 * value)
	{
		___Selection_3 = value;
		Il2CppCodeGenWriteBarrier((&___Selection_3), value);
	}

	inline static int32_t get_offset_of_vector_4() { return static_cast<int32_t>(offsetof(CardboardUI_t365955905, ___vector_4)); }
	inline Vector3_t3722313464  get_vector_4() const { return ___vector_4; }
	inline Vector3_t3722313464 * get_address_of_vector_4() { return &___vector_4; }
	inline void set_vector_4(Vector3_t3722313464  value)
	{
		___vector_4 = value;
	}

	inline static int32_t get_offset_of_vectors_5() { return static_cast<int32_t>(offsetof(CardboardUI_t365955905, ___vectors_5)); }
	inline Vector3_t3722313464  get_vectors_5() const { return ___vectors_5; }
	inline Vector3_t3722313464 * get_address_of_vectors_5() { return &___vectors_5; }
	inline void set_vectors_5(Vector3_t3722313464  value)
	{
		___vectors_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDBOARDUI_T365955905_H
#ifndef ARTIZPREVIEWBUTTON_T1230103917_H
#define ARTIZPREVIEWBUTTON_T1230103917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.ArtizPreviewButton
struct  ArtizPreviewButton_t1230103917  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text BMJ.M360.Artiz.ArtizPreviewButton::DisplayNameText
	Text_t1901882714 * ___DisplayNameText_2;
	// UnityEngine.UI.Text BMJ.M360.Artiz.ArtizPreviewButton::DescriptionText
	Text_t1901882714 * ___DescriptionText_3;
	// UnityEngine.UI.Image BMJ.M360.Artiz.ArtizPreviewButton::PreviewImage
	Image_t2670269651 * ___PreviewImage_4;
	// UnityEngine.GameObject BMJ.M360.Artiz.ArtizPreviewButton::Hourglass
	GameObject_t1113636619 * ___Hourglass_5;
	// UnityEngine.GameObject BMJ.M360.Artiz.ArtizPreviewButton::PreviewHolder
	GameObject_t1113636619 * ___PreviewHolder_6;
	// UnityEngine.UI.Image BMJ.M360.Artiz.ArtizPreviewButton::LoadingBar
	Image_t2670269651 * ___LoadingBar_7;
	// System.String BMJ.M360.Artiz.ArtizPreviewButton::UniqueID
	String_t* ___UniqueID_8;
	// System.String BMJ.M360.Artiz.ArtizPreviewButton::SpriteThumbnailLink
	String_t* ___SpriteThumbnailLink_9;
	// System.String BMJ.M360.Artiz.ArtizPreviewButton::SpriteHDRILink
	String_t* ___SpriteHDRILink_10;
	// System.Int32 BMJ.M360.Artiz.ArtizPreviewButton::CategoryIndex
	int32_t ___CategoryIndex_11;
	// System.Int32 BMJ.M360.Artiz.ArtizPreviewButton::StartIndex
	int32_t ___StartIndex_12;
	// UnityEngine.Vector3 BMJ.M360.Artiz.ArtizPreviewButton::HDRIRotationOffset
	Vector3_t3722313464  ___HDRIRotationOffset_13;
	// UnityEngine.Coroutine BMJ.M360.Artiz.ArtizPreviewButton::ActiveRoutine
	Coroutine_t3829159415 * ___ActiveRoutine_14;
	// UnityEngine.Events.UnityAction BMJ.M360.Artiz.ArtizPreviewButton::OnClickAction
	UnityAction_t3245792599 * ___OnClickAction_15;
	// System.Boolean BMJ.M360.Artiz.ArtizPreviewButton::InitialButtonFlag
	bool ___InitialButtonFlag_16;

public:
	inline static int32_t get_offset_of_DisplayNameText_2() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___DisplayNameText_2)); }
	inline Text_t1901882714 * get_DisplayNameText_2() const { return ___DisplayNameText_2; }
	inline Text_t1901882714 ** get_address_of_DisplayNameText_2() { return &___DisplayNameText_2; }
	inline void set_DisplayNameText_2(Text_t1901882714 * value)
	{
		___DisplayNameText_2 = value;
		Il2CppCodeGenWriteBarrier((&___DisplayNameText_2), value);
	}

	inline static int32_t get_offset_of_DescriptionText_3() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___DescriptionText_3)); }
	inline Text_t1901882714 * get_DescriptionText_3() const { return ___DescriptionText_3; }
	inline Text_t1901882714 ** get_address_of_DescriptionText_3() { return &___DescriptionText_3; }
	inline void set_DescriptionText_3(Text_t1901882714 * value)
	{
		___DescriptionText_3 = value;
		Il2CppCodeGenWriteBarrier((&___DescriptionText_3), value);
	}

	inline static int32_t get_offset_of_PreviewImage_4() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___PreviewImage_4)); }
	inline Image_t2670269651 * get_PreviewImage_4() const { return ___PreviewImage_4; }
	inline Image_t2670269651 ** get_address_of_PreviewImage_4() { return &___PreviewImage_4; }
	inline void set_PreviewImage_4(Image_t2670269651 * value)
	{
		___PreviewImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewImage_4), value);
	}

	inline static int32_t get_offset_of_Hourglass_5() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___Hourglass_5)); }
	inline GameObject_t1113636619 * get_Hourglass_5() const { return ___Hourglass_5; }
	inline GameObject_t1113636619 ** get_address_of_Hourglass_5() { return &___Hourglass_5; }
	inline void set_Hourglass_5(GameObject_t1113636619 * value)
	{
		___Hourglass_5 = value;
		Il2CppCodeGenWriteBarrier((&___Hourglass_5), value);
	}

	inline static int32_t get_offset_of_PreviewHolder_6() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___PreviewHolder_6)); }
	inline GameObject_t1113636619 * get_PreviewHolder_6() const { return ___PreviewHolder_6; }
	inline GameObject_t1113636619 ** get_address_of_PreviewHolder_6() { return &___PreviewHolder_6; }
	inline void set_PreviewHolder_6(GameObject_t1113636619 * value)
	{
		___PreviewHolder_6 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewHolder_6), value);
	}

	inline static int32_t get_offset_of_LoadingBar_7() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___LoadingBar_7)); }
	inline Image_t2670269651 * get_LoadingBar_7() const { return ___LoadingBar_7; }
	inline Image_t2670269651 ** get_address_of_LoadingBar_7() { return &___LoadingBar_7; }
	inline void set_LoadingBar_7(Image_t2670269651 * value)
	{
		___LoadingBar_7 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingBar_7), value);
	}

	inline static int32_t get_offset_of_UniqueID_8() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___UniqueID_8)); }
	inline String_t* get_UniqueID_8() const { return ___UniqueID_8; }
	inline String_t** get_address_of_UniqueID_8() { return &___UniqueID_8; }
	inline void set_UniqueID_8(String_t* value)
	{
		___UniqueID_8 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueID_8), value);
	}

	inline static int32_t get_offset_of_SpriteThumbnailLink_9() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___SpriteThumbnailLink_9)); }
	inline String_t* get_SpriteThumbnailLink_9() const { return ___SpriteThumbnailLink_9; }
	inline String_t** get_address_of_SpriteThumbnailLink_9() { return &___SpriteThumbnailLink_9; }
	inline void set_SpriteThumbnailLink_9(String_t* value)
	{
		___SpriteThumbnailLink_9 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteThumbnailLink_9), value);
	}

	inline static int32_t get_offset_of_SpriteHDRILink_10() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___SpriteHDRILink_10)); }
	inline String_t* get_SpriteHDRILink_10() const { return ___SpriteHDRILink_10; }
	inline String_t** get_address_of_SpriteHDRILink_10() { return &___SpriteHDRILink_10; }
	inline void set_SpriteHDRILink_10(String_t* value)
	{
		___SpriteHDRILink_10 = value;
		Il2CppCodeGenWriteBarrier((&___SpriteHDRILink_10), value);
	}

	inline static int32_t get_offset_of_CategoryIndex_11() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___CategoryIndex_11)); }
	inline int32_t get_CategoryIndex_11() const { return ___CategoryIndex_11; }
	inline int32_t* get_address_of_CategoryIndex_11() { return &___CategoryIndex_11; }
	inline void set_CategoryIndex_11(int32_t value)
	{
		___CategoryIndex_11 = value;
	}

	inline static int32_t get_offset_of_StartIndex_12() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___StartIndex_12)); }
	inline int32_t get_StartIndex_12() const { return ___StartIndex_12; }
	inline int32_t* get_address_of_StartIndex_12() { return &___StartIndex_12; }
	inline void set_StartIndex_12(int32_t value)
	{
		___StartIndex_12 = value;
	}

	inline static int32_t get_offset_of_HDRIRotationOffset_13() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___HDRIRotationOffset_13)); }
	inline Vector3_t3722313464  get_HDRIRotationOffset_13() const { return ___HDRIRotationOffset_13; }
	inline Vector3_t3722313464 * get_address_of_HDRIRotationOffset_13() { return &___HDRIRotationOffset_13; }
	inline void set_HDRIRotationOffset_13(Vector3_t3722313464  value)
	{
		___HDRIRotationOffset_13 = value;
	}

	inline static int32_t get_offset_of_ActiveRoutine_14() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___ActiveRoutine_14)); }
	inline Coroutine_t3829159415 * get_ActiveRoutine_14() const { return ___ActiveRoutine_14; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveRoutine_14() { return &___ActiveRoutine_14; }
	inline void set_ActiveRoutine_14(Coroutine_t3829159415 * value)
	{
		___ActiveRoutine_14 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveRoutine_14), value);
	}

	inline static int32_t get_offset_of_OnClickAction_15() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___OnClickAction_15)); }
	inline UnityAction_t3245792599 * get_OnClickAction_15() const { return ___OnClickAction_15; }
	inline UnityAction_t3245792599 ** get_address_of_OnClickAction_15() { return &___OnClickAction_15; }
	inline void set_OnClickAction_15(UnityAction_t3245792599 * value)
	{
		___OnClickAction_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnClickAction_15), value);
	}

	inline static int32_t get_offset_of_InitialButtonFlag_16() { return static_cast<int32_t>(offsetof(ArtizPreviewButton_t1230103917, ___InitialButtonFlag_16)); }
	inline bool get_InitialButtonFlag_16() const { return ___InitialButtonFlag_16; }
	inline bool* get_address_of_InitialButtonFlag_16() { return &___InitialButtonFlag_16; }
	inline void set_InitialButtonFlag_16(bool value)
	{
		___InitialButtonFlag_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTIZPREVIEWBUTTON_T1230103917_H
#ifndef LAYOUTCONTENTSIZEFITTER_T3108834706_H
#define LAYOUTCONTENTSIZEFITTER_T3108834706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.LayoutContentSizeFitter
struct  LayoutContentSizeFitter_t3108834706  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 BMJ.M360.Artiz.LayoutContentSizeFitter::ChildCount
	int32_t ___ChildCount_2;
	// UnityEngine.UI.LayoutElement BMJ.M360.Artiz.LayoutContentSizeFitter::AttachedLayoutElement
	LayoutElement_t1785403678 * ___AttachedLayoutElement_3;

public:
	inline static int32_t get_offset_of_ChildCount_2() { return static_cast<int32_t>(offsetof(LayoutContentSizeFitter_t3108834706, ___ChildCount_2)); }
	inline int32_t get_ChildCount_2() const { return ___ChildCount_2; }
	inline int32_t* get_address_of_ChildCount_2() { return &___ChildCount_2; }
	inline void set_ChildCount_2(int32_t value)
	{
		___ChildCount_2 = value;
	}

	inline static int32_t get_offset_of_AttachedLayoutElement_3() { return static_cast<int32_t>(offsetof(LayoutContentSizeFitter_t3108834706, ___AttachedLayoutElement_3)); }
	inline LayoutElement_t1785403678 * get_AttachedLayoutElement_3() const { return ___AttachedLayoutElement_3; }
	inline LayoutElement_t1785403678 ** get_address_of_AttachedLayoutElement_3() { return &___AttachedLayoutElement_3; }
	inline void set_AttachedLayoutElement_3(LayoutElement_t1785403678 * value)
	{
		___AttachedLayoutElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedLayoutElement_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTCONTENTSIZEFITTER_T3108834706_H
#ifndef VRRAYCASTER_T2465929147_H
#define VRRAYCASTER_T2465929147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRRaycaster
struct  VRRaycaster_t2465929147  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.GraphicRaycaster VRRaycaster::m_Raycaster
	GraphicRaycaster_t2999697109 * ___m_Raycaster_2;
	// UnityEngine.EventSystems.PointerEventData VRRaycaster::m_PointerEventData
	PointerEventData_t3807901092 * ___m_PointerEventData_3;
	// UnityEngine.EventSystems.EventSystem VRRaycaster::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// System.Single VRRaycaster::timestamp
	float ___timestamp_5;
	// System.Boolean VRRaycaster::buttonhit
	bool ___buttonhit_6;
	// UnityEngine.GameObject VRRaycaster::lastHitObject
	GameObject_t1113636619 * ___lastHitObject_7;
	// UnityEngine.UI.Image VRRaycaster::countdownImage
	Image_t2670269651 * ___countdownImage_8;
	// System.Single VRRaycaster::waitTime
	float ___waitTime_9;
	// System.Boolean VRRaycaster::clear
	bool ___clear_10;

public:
	inline static int32_t get_offset_of_m_Raycaster_2() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___m_Raycaster_2)); }
	inline GraphicRaycaster_t2999697109 * get_m_Raycaster_2() const { return ___m_Raycaster_2; }
	inline GraphicRaycaster_t2999697109 ** get_address_of_m_Raycaster_2() { return &___m_Raycaster_2; }
	inline void set_m_Raycaster_2(GraphicRaycaster_t2999697109 * value)
	{
		___m_Raycaster_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Raycaster_2), value);
	}

	inline static int32_t get_offset_of_m_PointerEventData_3() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___m_PointerEventData_3)); }
	inline PointerEventData_t3807901092 * get_m_PointerEventData_3() const { return ___m_PointerEventData_3; }
	inline PointerEventData_t3807901092 ** get_address_of_m_PointerEventData_3() { return &___m_PointerEventData_3; }
	inline void set_m_PointerEventData_3(PointerEventData_t3807901092 * value)
	{
		___m_PointerEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_timestamp_5() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___timestamp_5)); }
	inline float get_timestamp_5() const { return ___timestamp_5; }
	inline float* get_address_of_timestamp_5() { return &___timestamp_5; }
	inline void set_timestamp_5(float value)
	{
		___timestamp_5 = value;
	}

	inline static int32_t get_offset_of_buttonhit_6() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___buttonhit_6)); }
	inline bool get_buttonhit_6() const { return ___buttonhit_6; }
	inline bool* get_address_of_buttonhit_6() { return &___buttonhit_6; }
	inline void set_buttonhit_6(bool value)
	{
		___buttonhit_6 = value;
	}

	inline static int32_t get_offset_of_lastHitObject_7() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___lastHitObject_7)); }
	inline GameObject_t1113636619 * get_lastHitObject_7() const { return ___lastHitObject_7; }
	inline GameObject_t1113636619 ** get_address_of_lastHitObject_7() { return &___lastHitObject_7; }
	inline void set_lastHitObject_7(GameObject_t1113636619 * value)
	{
		___lastHitObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastHitObject_7), value);
	}

	inline static int32_t get_offset_of_countdownImage_8() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___countdownImage_8)); }
	inline Image_t2670269651 * get_countdownImage_8() const { return ___countdownImage_8; }
	inline Image_t2670269651 ** get_address_of_countdownImage_8() { return &___countdownImage_8; }
	inline void set_countdownImage_8(Image_t2670269651 * value)
	{
		___countdownImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___countdownImage_8), value);
	}

	inline static int32_t get_offset_of_waitTime_9() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___waitTime_9)); }
	inline float get_waitTime_9() const { return ___waitTime_9; }
	inline float* get_address_of_waitTime_9() { return &___waitTime_9; }
	inline void set_waitTime_9(float value)
	{
		___waitTime_9 = value;
	}

	inline static int32_t get_offset_of_clear_10() { return static_cast<int32_t>(offsetof(VRRaycaster_t2465929147, ___clear_10)); }
	inline bool get_clear_10() const { return ___clear_10; }
	inline bool* get_address_of_clear_10() { return &___clear_10; }
	inline void set_clear_10(bool value)
	{
		___clear_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRRAYCASTER_T2465929147_H
#ifndef NAVIGATABLEHDRI_T1695953284_H
#define NAVIGATABLEHDRI_T1695953284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.NavigatableHDRI
struct  NavigatableHDRI_t1695953284  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.MeshRenderer> BMJ.M360.Artiz.NavigatableHDRI::HDRISpheres
	List_1_t2059084002 * ___HDRISpheres_2;
	// System.Single BMJ.M360.Artiz.NavigatableHDRI::SpeedModifier
	float ___SpeedModifier_3;
	// System.Int32 BMJ.M360.Artiz.NavigatableHDRI::StartIndex
	int32_t ___StartIndex_4;
	// UnityEngine.Texture2D BMJ.M360.Artiz.NavigatableHDRI::ActiveArrowTexture
	Texture2D_t3840446185 * ___ActiveArrowTexture_5;
	// UnityEngine.Texture2D BMJ.M360.Artiz.NavigatableHDRI::InactiveArrowTexture
	Texture2D_t3840446185 * ___InactiveArrowTexture_6;
	// System.Int32 BMJ.M360.Artiz.NavigatableHDRI::CurrentIndex
	int32_t ___CurrentIndex_7;
	// System.Int32 BMJ.M360.Artiz.NavigatableHDRI::TargetIndex
	int32_t ___TargetIndex_8;

public:
	inline static int32_t get_offset_of_HDRISpheres_2() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___HDRISpheres_2)); }
	inline List_1_t2059084002 * get_HDRISpheres_2() const { return ___HDRISpheres_2; }
	inline List_1_t2059084002 ** get_address_of_HDRISpheres_2() { return &___HDRISpheres_2; }
	inline void set_HDRISpheres_2(List_1_t2059084002 * value)
	{
		___HDRISpheres_2 = value;
		Il2CppCodeGenWriteBarrier((&___HDRISpheres_2), value);
	}

	inline static int32_t get_offset_of_SpeedModifier_3() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___SpeedModifier_3)); }
	inline float get_SpeedModifier_3() const { return ___SpeedModifier_3; }
	inline float* get_address_of_SpeedModifier_3() { return &___SpeedModifier_3; }
	inline void set_SpeedModifier_3(float value)
	{
		___SpeedModifier_3 = value;
	}

	inline static int32_t get_offset_of_StartIndex_4() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___StartIndex_4)); }
	inline int32_t get_StartIndex_4() const { return ___StartIndex_4; }
	inline int32_t* get_address_of_StartIndex_4() { return &___StartIndex_4; }
	inline void set_StartIndex_4(int32_t value)
	{
		___StartIndex_4 = value;
	}

	inline static int32_t get_offset_of_ActiveArrowTexture_5() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___ActiveArrowTexture_5)); }
	inline Texture2D_t3840446185 * get_ActiveArrowTexture_5() const { return ___ActiveArrowTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_ActiveArrowTexture_5() { return &___ActiveArrowTexture_5; }
	inline void set_ActiveArrowTexture_5(Texture2D_t3840446185 * value)
	{
		___ActiveArrowTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveArrowTexture_5), value);
	}

	inline static int32_t get_offset_of_InactiveArrowTexture_6() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___InactiveArrowTexture_6)); }
	inline Texture2D_t3840446185 * get_InactiveArrowTexture_6() const { return ___InactiveArrowTexture_6; }
	inline Texture2D_t3840446185 ** get_address_of_InactiveArrowTexture_6() { return &___InactiveArrowTexture_6; }
	inline void set_InactiveArrowTexture_6(Texture2D_t3840446185 * value)
	{
		___InactiveArrowTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___InactiveArrowTexture_6), value);
	}

	inline static int32_t get_offset_of_CurrentIndex_7() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___CurrentIndex_7)); }
	inline int32_t get_CurrentIndex_7() const { return ___CurrentIndex_7; }
	inline int32_t* get_address_of_CurrentIndex_7() { return &___CurrentIndex_7; }
	inline void set_CurrentIndex_7(int32_t value)
	{
		___CurrentIndex_7 = value;
	}

	inline static int32_t get_offset_of_TargetIndex_8() { return static_cast<int32_t>(offsetof(NavigatableHDRI_t1695953284, ___TargetIndex_8)); }
	inline int32_t get_TargetIndex_8() const { return ___TargetIndex_8; }
	inline int32_t* get_address_of_TargetIndex_8() { return &___TargetIndex_8; }
	inline void set_TargetIndex_8(int32_t value)
	{
		___TargetIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVIGATABLEHDRI_T1695953284_H
#ifndef SNAPPINGSCROLLRECT_T125492878_H
#define SNAPPINGSCROLLRECT_T125492878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.SnappingScrollRect
struct  SnappingScrollRect_t125492878  : public MonoBehaviour_t3962482529
{
public:
	// System.Single BMJ.M360.Artiz.SnappingScrollRect::Step
	float ___Step_2;
	// UnityEngine.UI.ScrollRect BMJ.M360.Artiz.SnappingScrollRect::AttachedScrollRect
	ScrollRect_t4137855814 * ___AttachedScrollRect_3;
	// System.Int32 BMJ.M360.Artiz.SnappingScrollRect::ContentAmount
	int32_t ___ContentAmount_4;
	// UnityEngine.Vector2 BMJ.M360.Artiz.SnappingScrollRect::TargetNormalizedPosition
	Vector2_t2156229523  ___TargetNormalizedPosition_5;
	// UnityEngine.Coroutine BMJ.M360.Artiz.SnappingScrollRect::ActiveUpdateRoutine
	Coroutine_t3829159415 * ___ActiveUpdateRoutine_6;

public:
	inline static int32_t get_offset_of_Step_2() { return static_cast<int32_t>(offsetof(SnappingScrollRect_t125492878, ___Step_2)); }
	inline float get_Step_2() const { return ___Step_2; }
	inline float* get_address_of_Step_2() { return &___Step_2; }
	inline void set_Step_2(float value)
	{
		___Step_2 = value;
	}

	inline static int32_t get_offset_of_AttachedScrollRect_3() { return static_cast<int32_t>(offsetof(SnappingScrollRect_t125492878, ___AttachedScrollRect_3)); }
	inline ScrollRect_t4137855814 * get_AttachedScrollRect_3() const { return ___AttachedScrollRect_3; }
	inline ScrollRect_t4137855814 ** get_address_of_AttachedScrollRect_3() { return &___AttachedScrollRect_3; }
	inline void set_AttachedScrollRect_3(ScrollRect_t4137855814 * value)
	{
		___AttachedScrollRect_3 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedScrollRect_3), value);
	}

	inline static int32_t get_offset_of_ContentAmount_4() { return static_cast<int32_t>(offsetof(SnappingScrollRect_t125492878, ___ContentAmount_4)); }
	inline int32_t get_ContentAmount_4() const { return ___ContentAmount_4; }
	inline int32_t* get_address_of_ContentAmount_4() { return &___ContentAmount_4; }
	inline void set_ContentAmount_4(int32_t value)
	{
		___ContentAmount_4 = value;
	}

	inline static int32_t get_offset_of_TargetNormalizedPosition_5() { return static_cast<int32_t>(offsetof(SnappingScrollRect_t125492878, ___TargetNormalizedPosition_5)); }
	inline Vector2_t2156229523  get_TargetNormalizedPosition_5() const { return ___TargetNormalizedPosition_5; }
	inline Vector2_t2156229523 * get_address_of_TargetNormalizedPosition_5() { return &___TargetNormalizedPosition_5; }
	inline void set_TargetNormalizedPosition_5(Vector2_t2156229523  value)
	{
		___TargetNormalizedPosition_5 = value;
	}

	inline static int32_t get_offset_of_ActiveUpdateRoutine_6() { return static_cast<int32_t>(offsetof(SnappingScrollRect_t125492878, ___ActiveUpdateRoutine_6)); }
	inline Coroutine_t3829159415 * get_ActiveUpdateRoutine_6() const { return ___ActiveUpdateRoutine_6; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveUpdateRoutine_6() { return &___ActiveUpdateRoutine_6; }
	inline void set_ActiveUpdateRoutine_6(Coroutine_t3829159415 * value)
	{
		___ActiveUpdateRoutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveUpdateRoutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPPINGSCROLLRECT_T125492878_H
#ifndef CUSTOMFITSCROLLER_T3272853734_H
#define CUSTOMFITSCROLLER_T3272853734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.CustomFitScroller
struct  CustomFitScroller_t3272853734  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform BMJ.M360.Artiz.CustomFitScroller::TargetRectTransform
	RectTransform_t3704657025 * ___TargetRectTransform_2;
	// UnityEngine.RectTransform BMJ.M360.Artiz.CustomFitScroller::StartRectTransform
	RectTransform_t3704657025 * ___StartRectTransform_3;
	// UnityEngine.RectTransform BMJ.M360.Artiz.CustomFitScroller::EndRectTransform
	RectTransform_t3704657025 * ___EndRectTransform_4;
	// UnityEngine.Vector2 BMJ.M360.Artiz.CustomFitScroller::TargetAnchor
	Vector2_t2156229523  ___TargetAnchor_5;
	// System.Single BMJ.M360.Artiz.CustomFitScroller::Step
	float ___Step_6;
	// System.Boolean BMJ.M360.Artiz.CustomFitScroller::Initial
	bool ___Initial_7;

public:
	inline static int32_t get_offset_of_TargetRectTransform_2() { return static_cast<int32_t>(offsetof(CustomFitScroller_t3272853734, ___TargetRectTransform_2)); }
	inline RectTransform_t3704657025 * get_TargetRectTransform_2() const { return ___TargetRectTransform_2; }
	inline RectTransform_t3704657025 ** get_address_of_TargetRectTransform_2() { return &___TargetRectTransform_2; }
	inline void set_TargetRectTransform_2(RectTransform_t3704657025 * value)
	{
		___TargetRectTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetRectTransform_2), value);
	}

	inline static int32_t get_offset_of_StartRectTransform_3() { return static_cast<int32_t>(offsetof(CustomFitScroller_t3272853734, ___StartRectTransform_3)); }
	inline RectTransform_t3704657025 * get_StartRectTransform_3() const { return ___StartRectTransform_3; }
	inline RectTransform_t3704657025 ** get_address_of_StartRectTransform_3() { return &___StartRectTransform_3; }
	inline void set_StartRectTransform_3(RectTransform_t3704657025 * value)
	{
		___StartRectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___StartRectTransform_3), value);
	}

	inline static int32_t get_offset_of_EndRectTransform_4() { return static_cast<int32_t>(offsetof(CustomFitScroller_t3272853734, ___EndRectTransform_4)); }
	inline RectTransform_t3704657025 * get_EndRectTransform_4() const { return ___EndRectTransform_4; }
	inline RectTransform_t3704657025 ** get_address_of_EndRectTransform_4() { return &___EndRectTransform_4; }
	inline void set_EndRectTransform_4(RectTransform_t3704657025 * value)
	{
		___EndRectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___EndRectTransform_4), value);
	}

	inline static int32_t get_offset_of_TargetAnchor_5() { return static_cast<int32_t>(offsetof(CustomFitScroller_t3272853734, ___TargetAnchor_5)); }
	inline Vector2_t2156229523  get_TargetAnchor_5() const { return ___TargetAnchor_5; }
	inline Vector2_t2156229523 * get_address_of_TargetAnchor_5() { return &___TargetAnchor_5; }
	inline void set_TargetAnchor_5(Vector2_t2156229523  value)
	{
		___TargetAnchor_5 = value;
	}

	inline static int32_t get_offset_of_Step_6() { return static_cast<int32_t>(offsetof(CustomFitScroller_t3272853734, ___Step_6)); }
	inline float get_Step_6() const { return ___Step_6; }
	inline float* get_address_of_Step_6() { return &___Step_6; }
	inline void set_Step_6(float value)
	{
		___Step_6 = value;
	}

	inline static int32_t get_offset_of_Initial_7() { return static_cast<int32_t>(offsetof(CustomFitScroller_t3272853734, ___Initial_7)); }
	inline bool get_Initial_7() const { return ___Initial_7; }
	inline bool* get_address_of_Initial_7() { return &___Initial_7; }
	inline void set_Initial_7(bool value)
	{
		___Initial_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMFITSCROLLER_T3272853734_H
#ifndef INFOPANELCONTROLLER_T539477300_H
#define INFOPANELCONTROLLER_T539477300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.InfoPanelController
struct  InfoPanelController_t539477300  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BMJ.M360.Artiz.InfoPanelController::InfoPanelObject
	GameObject_t1113636619 * ___InfoPanelObject_2;
	// UnityEngine.GameObject BMJ.M360.Artiz.InfoPanelController::PreviewImageObject
	GameObject_t1113636619 * ___PreviewImageObject_3;
	// UnityEngine.UI.Image BMJ.M360.Artiz.InfoPanelController::PreviewImage
	Image_t2670269651 * ___PreviewImage_4;
	// System.String BMJ.M360.Artiz.InfoPanelController::FacebookPageLink
	String_t* ___FacebookPageLink_5;
	// System.String BMJ.M360.Artiz.InfoPanelController::WebsitePageLink
	String_t* ___WebsitePageLink_6;
	// System.String BMJ.M360.Artiz.InfoPanelController::InstagramPageLink
	String_t* ___InstagramPageLink_7;

public:
	inline static int32_t get_offset_of_InfoPanelObject_2() { return static_cast<int32_t>(offsetof(InfoPanelController_t539477300, ___InfoPanelObject_2)); }
	inline GameObject_t1113636619 * get_InfoPanelObject_2() const { return ___InfoPanelObject_2; }
	inline GameObject_t1113636619 ** get_address_of_InfoPanelObject_2() { return &___InfoPanelObject_2; }
	inline void set_InfoPanelObject_2(GameObject_t1113636619 * value)
	{
		___InfoPanelObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___InfoPanelObject_2), value);
	}

	inline static int32_t get_offset_of_PreviewImageObject_3() { return static_cast<int32_t>(offsetof(InfoPanelController_t539477300, ___PreviewImageObject_3)); }
	inline GameObject_t1113636619 * get_PreviewImageObject_3() const { return ___PreviewImageObject_3; }
	inline GameObject_t1113636619 ** get_address_of_PreviewImageObject_3() { return &___PreviewImageObject_3; }
	inline void set_PreviewImageObject_3(GameObject_t1113636619 * value)
	{
		___PreviewImageObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewImageObject_3), value);
	}

	inline static int32_t get_offset_of_PreviewImage_4() { return static_cast<int32_t>(offsetof(InfoPanelController_t539477300, ___PreviewImage_4)); }
	inline Image_t2670269651 * get_PreviewImage_4() const { return ___PreviewImage_4; }
	inline Image_t2670269651 ** get_address_of_PreviewImage_4() { return &___PreviewImage_4; }
	inline void set_PreviewImage_4(Image_t2670269651 * value)
	{
		___PreviewImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewImage_4), value);
	}

	inline static int32_t get_offset_of_FacebookPageLink_5() { return static_cast<int32_t>(offsetof(InfoPanelController_t539477300, ___FacebookPageLink_5)); }
	inline String_t* get_FacebookPageLink_5() const { return ___FacebookPageLink_5; }
	inline String_t** get_address_of_FacebookPageLink_5() { return &___FacebookPageLink_5; }
	inline void set_FacebookPageLink_5(String_t* value)
	{
		___FacebookPageLink_5 = value;
		Il2CppCodeGenWriteBarrier((&___FacebookPageLink_5), value);
	}

	inline static int32_t get_offset_of_WebsitePageLink_6() { return static_cast<int32_t>(offsetof(InfoPanelController_t539477300, ___WebsitePageLink_6)); }
	inline String_t* get_WebsitePageLink_6() const { return ___WebsitePageLink_6; }
	inline String_t** get_address_of_WebsitePageLink_6() { return &___WebsitePageLink_6; }
	inline void set_WebsitePageLink_6(String_t* value)
	{
		___WebsitePageLink_6 = value;
		Il2CppCodeGenWriteBarrier((&___WebsitePageLink_6), value);
	}

	inline static int32_t get_offset_of_InstagramPageLink_7() { return static_cast<int32_t>(offsetof(InfoPanelController_t539477300, ___InstagramPageLink_7)); }
	inline String_t* get_InstagramPageLink_7() const { return ___InstagramPageLink_7; }
	inline String_t** get_address_of_InstagramPageLink_7() { return &___InstagramPageLink_7; }
	inline void set_InstagramPageLink_7(String_t* value)
	{
		___InstagramPageLink_7 = value;
		Il2CppCodeGenWriteBarrier((&___InstagramPageLink_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFOPANELCONTROLLER_T539477300_H
#ifndef SCROLLSNAPRECT_T3278334712_H
#define SCROLLSNAPRECT_T3278334712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollSnapRect
struct  ScrollSnapRect_t3278334712  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> ScrollSnapRect::m_PaginationChildren
	List_1_t4207451803 * ___m_PaginationChildren_2;
	// System.Int32 ScrollSnapRect::startingPage
	int32_t ___startingPage_3;
	// System.Single ScrollSnapRect::fastSwipeThresholdTime
	float ___fastSwipeThresholdTime_4;
	// System.Int32 ScrollSnapRect::fastSwipeThresholdDistance
	int32_t ___fastSwipeThresholdDistance_5;
	// System.Single ScrollSnapRect::decelerationRate
	float ___decelerationRate_6;
	// UnityEngine.GameObject ScrollSnapRect::prevButton
	GameObject_t1113636619 * ___prevButton_7;
	// UnityEngine.GameObject ScrollSnapRect::nextButton
	GameObject_t1113636619 * ___nextButton_8;
	// UnityEngine.Sprite ScrollSnapRect::unselectedPage
	Sprite_t280657092 * ___unselectedPage_9;
	// UnityEngine.Sprite ScrollSnapRect::selectedPage
	Sprite_t280657092 * ___selectedPage_10;
	// UnityEngine.Transform ScrollSnapRect::pageSelectionIcons
	Transform_t3600365921 * ___pageSelectionIcons_11;
	// System.Int32 ScrollSnapRect::_fastSwipeThresholdMaxLimit
	int32_t ____fastSwipeThresholdMaxLimit_12;
	// UnityEngine.UI.ScrollRect ScrollSnapRect::_scrollRectComponent
	ScrollRect_t4137855814 * ____scrollRectComponent_13;
	// UnityEngine.RectTransform ScrollSnapRect::_scrollRectRect
	RectTransform_t3704657025 * ____scrollRectRect_14;
	// UnityEngine.RectTransform ScrollSnapRect::_container
	RectTransform_t3704657025 * ____container_15;
	// System.Boolean ScrollSnapRect::_horizontal
	bool ____horizontal_16;
	// System.Int32 ScrollSnapRect::_pageCount
	int32_t ____pageCount_17;
	// System.Int32 ScrollSnapRect::_currentPage
	int32_t ____currentPage_18;
	// System.Boolean ScrollSnapRect::_lerp
	bool ____lerp_19;
	// UnityEngine.Vector2 ScrollSnapRect::_lerpTo
	Vector2_t2156229523  ____lerpTo_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> ScrollSnapRect::_pagePositions
	List_1_t3628304265 * ____pagePositions_21;
	// System.Boolean ScrollSnapRect::_dragging
	bool ____dragging_22;
	// System.Single ScrollSnapRect::_timeStamp
	float ____timeStamp_23;
	// UnityEngine.Vector2 ScrollSnapRect::_startPosition
	Vector2_t2156229523  ____startPosition_24;
	// System.Boolean ScrollSnapRect::_showPageSelection
	bool ____showPageSelection_25;
	// System.Int32 ScrollSnapRect::_previousPageSelectionIndex
	int32_t ____previousPageSelectionIndex_26;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> ScrollSnapRect::_pageSelectionImages
	List_1_t4142344393 * ____pageSelectionImages_27;
	// UnityEngine.RectTransform ScrollSnapRect::panel
	RectTransform_t3704657025 * ___panel_28;
	// UnityEngine.RectTransform ScrollSnapRect::panel1
	RectTransform_t3704657025 * ___panel1_29;
	// UnityEngine.RectTransform ScrollSnapRect::panel2
	RectTransform_t3704657025 * ___panel2_30;
	// UnityEngine.GameObject ScrollSnapRect::bgPanel
	GameObject_t1113636619 * ___bgPanel_31;
	// UnityEngine.GameObject ScrollSnapRect::bgPanel1
	GameObject_t1113636619 * ___bgPanel1_32;
	// UnityEngine.GameObject ScrollSnapRect::bgPanel2
	GameObject_t1113636619 * ___bgPanel2_33;
	// UnityEngine.Vector2 ScrollSnapRect::currentScale
	Vector2_t2156229523  ___currentScale_34;
	// UnityEngine.Vector2 ScrollSnapRect::othersScale
	Vector2_t2156229523  ___othersScale_35;
	// UnityEngine.Vector3 ScrollSnapRect::MinCache
	Vector3_t3722313464  ___MinCache_36;
	// UnityEngine.Vector3 ScrollSnapRect::MaxCache
	Vector3_t3722313464  ___MaxCache_37;

public:
	inline static int32_t get_offset_of_m_PaginationChildren_2() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___m_PaginationChildren_2)); }
	inline List_1_t4207451803 * get_m_PaginationChildren_2() const { return ___m_PaginationChildren_2; }
	inline List_1_t4207451803 ** get_address_of_m_PaginationChildren_2() { return &___m_PaginationChildren_2; }
	inline void set_m_PaginationChildren_2(List_1_t4207451803 * value)
	{
		___m_PaginationChildren_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PaginationChildren_2), value);
	}

	inline static int32_t get_offset_of_startingPage_3() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___startingPage_3)); }
	inline int32_t get_startingPage_3() const { return ___startingPage_3; }
	inline int32_t* get_address_of_startingPage_3() { return &___startingPage_3; }
	inline void set_startingPage_3(int32_t value)
	{
		___startingPage_3 = value;
	}

	inline static int32_t get_offset_of_fastSwipeThresholdTime_4() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___fastSwipeThresholdTime_4)); }
	inline float get_fastSwipeThresholdTime_4() const { return ___fastSwipeThresholdTime_4; }
	inline float* get_address_of_fastSwipeThresholdTime_4() { return &___fastSwipeThresholdTime_4; }
	inline void set_fastSwipeThresholdTime_4(float value)
	{
		___fastSwipeThresholdTime_4 = value;
	}

	inline static int32_t get_offset_of_fastSwipeThresholdDistance_5() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___fastSwipeThresholdDistance_5)); }
	inline int32_t get_fastSwipeThresholdDistance_5() const { return ___fastSwipeThresholdDistance_5; }
	inline int32_t* get_address_of_fastSwipeThresholdDistance_5() { return &___fastSwipeThresholdDistance_5; }
	inline void set_fastSwipeThresholdDistance_5(int32_t value)
	{
		___fastSwipeThresholdDistance_5 = value;
	}

	inline static int32_t get_offset_of_decelerationRate_6() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___decelerationRate_6)); }
	inline float get_decelerationRate_6() const { return ___decelerationRate_6; }
	inline float* get_address_of_decelerationRate_6() { return &___decelerationRate_6; }
	inline void set_decelerationRate_6(float value)
	{
		___decelerationRate_6 = value;
	}

	inline static int32_t get_offset_of_prevButton_7() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___prevButton_7)); }
	inline GameObject_t1113636619 * get_prevButton_7() const { return ___prevButton_7; }
	inline GameObject_t1113636619 ** get_address_of_prevButton_7() { return &___prevButton_7; }
	inline void set_prevButton_7(GameObject_t1113636619 * value)
	{
		___prevButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___prevButton_7), value);
	}

	inline static int32_t get_offset_of_nextButton_8() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___nextButton_8)); }
	inline GameObject_t1113636619 * get_nextButton_8() const { return ___nextButton_8; }
	inline GameObject_t1113636619 ** get_address_of_nextButton_8() { return &___nextButton_8; }
	inline void set_nextButton_8(GameObject_t1113636619 * value)
	{
		___nextButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___nextButton_8), value);
	}

	inline static int32_t get_offset_of_unselectedPage_9() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___unselectedPage_9)); }
	inline Sprite_t280657092 * get_unselectedPage_9() const { return ___unselectedPage_9; }
	inline Sprite_t280657092 ** get_address_of_unselectedPage_9() { return &___unselectedPage_9; }
	inline void set_unselectedPage_9(Sprite_t280657092 * value)
	{
		___unselectedPage_9 = value;
		Il2CppCodeGenWriteBarrier((&___unselectedPage_9), value);
	}

	inline static int32_t get_offset_of_selectedPage_10() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___selectedPage_10)); }
	inline Sprite_t280657092 * get_selectedPage_10() const { return ___selectedPage_10; }
	inline Sprite_t280657092 ** get_address_of_selectedPage_10() { return &___selectedPage_10; }
	inline void set_selectedPage_10(Sprite_t280657092 * value)
	{
		___selectedPage_10 = value;
		Il2CppCodeGenWriteBarrier((&___selectedPage_10), value);
	}

	inline static int32_t get_offset_of_pageSelectionIcons_11() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___pageSelectionIcons_11)); }
	inline Transform_t3600365921 * get_pageSelectionIcons_11() const { return ___pageSelectionIcons_11; }
	inline Transform_t3600365921 ** get_address_of_pageSelectionIcons_11() { return &___pageSelectionIcons_11; }
	inline void set_pageSelectionIcons_11(Transform_t3600365921 * value)
	{
		___pageSelectionIcons_11 = value;
		Il2CppCodeGenWriteBarrier((&___pageSelectionIcons_11), value);
	}

	inline static int32_t get_offset_of__fastSwipeThresholdMaxLimit_12() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____fastSwipeThresholdMaxLimit_12)); }
	inline int32_t get__fastSwipeThresholdMaxLimit_12() const { return ____fastSwipeThresholdMaxLimit_12; }
	inline int32_t* get_address_of__fastSwipeThresholdMaxLimit_12() { return &____fastSwipeThresholdMaxLimit_12; }
	inline void set__fastSwipeThresholdMaxLimit_12(int32_t value)
	{
		____fastSwipeThresholdMaxLimit_12 = value;
	}

	inline static int32_t get_offset_of__scrollRectComponent_13() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____scrollRectComponent_13)); }
	inline ScrollRect_t4137855814 * get__scrollRectComponent_13() const { return ____scrollRectComponent_13; }
	inline ScrollRect_t4137855814 ** get_address_of__scrollRectComponent_13() { return &____scrollRectComponent_13; }
	inline void set__scrollRectComponent_13(ScrollRect_t4137855814 * value)
	{
		____scrollRectComponent_13 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRectComponent_13), value);
	}

	inline static int32_t get_offset_of__scrollRectRect_14() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____scrollRectRect_14)); }
	inline RectTransform_t3704657025 * get__scrollRectRect_14() const { return ____scrollRectRect_14; }
	inline RectTransform_t3704657025 ** get_address_of__scrollRectRect_14() { return &____scrollRectRect_14; }
	inline void set__scrollRectRect_14(RectTransform_t3704657025 * value)
	{
		____scrollRectRect_14 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRectRect_14), value);
	}

	inline static int32_t get_offset_of__container_15() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____container_15)); }
	inline RectTransform_t3704657025 * get__container_15() const { return ____container_15; }
	inline RectTransform_t3704657025 ** get_address_of__container_15() { return &____container_15; }
	inline void set__container_15(RectTransform_t3704657025 * value)
	{
		____container_15 = value;
		Il2CppCodeGenWriteBarrier((&____container_15), value);
	}

	inline static int32_t get_offset_of__horizontal_16() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____horizontal_16)); }
	inline bool get__horizontal_16() const { return ____horizontal_16; }
	inline bool* get_address_of__horizontal_16() { return &____horizontal_16; }
	inline void set__horizontal_16(bool value)
	{
		____horizontal_16 = value;
	}

	inline static int32_t get_offset_of__pageCount_17() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____pageCount_17)); }
	inline int32_t get__pageCount_17() const { return ____pageCount_17; }
	inline int32_t* get_address_of__pageCount_17() { return &____pageCount_17; }
	inline void set__pageCount_17(int32_t value)
	{
		____pageCount_17 = value;
	}

	inline static int32_t get_offset_of__currentPage_18() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____currentPage_18)); }
	inline int32_t get__currentPage_18() const { return ____currentPage_18; }
	inline int32_t* get_address_of__currentPage_18() { return &____currentPage_18; }
	inline void set__currentPage_18(int32_t value)
	{
		____currentPage_18 = value;
	}

	inline static int32_t get_offset_of__lerp_19() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____lerp_19)); }
	inline bool get__lerp_19() const { return ____lerp_19; }
	inline bool* get_address_of__lerp_19() { return &____lerp_19; }
	inline void set__lerp_19(bool value)
	{
		____lerp_19 = value;
	}

	inline static int32_t get_offset_of__lerpTo_20() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____lerpTo_20)); }
	inline Vector2_t2156229523  get__lerpTo_20() const { return ____lerpTo_20; }
	inline Vector2_t2156229523 * get_address_of__lerpTo_20() { return &____lerpTo_20; }
	inline void set__lerpTo_20(Vector2_t2156229523  value)
	{
		____lerpTo_20 = value;
	}

	inline static int32_t get_offset_of__pagePositions_21() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____pagePositions_21)); }
	inline List_1_t3628304265 * get__pagePositions_21() const { return ____pagePositions_21; }
	inline List_1_t3628304265 ** get_address_of__pagePositions_21() { return &____pagePositions_21; }
	inline void set__pagePositions_21(List_1_t3628304265 * value)
	{
		____pagePositions_21 = value;
		Il2CppCodeGenWriteBarrier((&____pagePositions_21), value);
	}

	inline static int32_t get_offset_of__dragging_22() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____dragging_22)); }
	inline bool get__dragging_22() const { return ____dragging_22; }
	inline bool* get_address_of__dragging_22() { return &____dragging_22; }
	inline void set__dragging_22(bool value)
	{
		____dragging_22 = value;
	}

	inline static int32_t get_offset_of__timeStamp_23() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____timeStamp_23)); }
	inline float get__timeStamp_23() const { return ____timeStamp_23; }
	inline float* get_address_of__timeStamp_23() { return &____timeStamp_23; }
	inline void set__timeStamp_23(float value)
	{
		____timeStamp_23 = value;
	}

	inline static int32_t get_offset_of__startPosition_24() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____startPosition_24)); }
	inline Vector2_t2156229523  get__startPosition_24() const { return ____startPosition_24; }
	inline Vector2_t2156229523 * get_address_of__startPosition_24() { return &____startPosition_24; }
	inline void set__startPosition_24(Vector2_t2156229523  value)
	{
		____startPosition_24 = value;
	}

	inline static int32_t get_offset_of__showPageSelection_25() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____showPageSelection_25)); }
	inline bool get__showPageSelection_25() const { return ____showPageSelection_25; }
	inline bool* get_address_of__showPageSelection_25() { return &____showPageSelection_25; }
	inline void set__showPageSelection_25(bool value)
	{
		____showPageSelection_25 = value;
	}

	inline static int32_t get_offset_of__previousPageSelectionIndex_26() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____previousPageSelectionIndex_26)); }
	inline int32_t get__previousPageSelectionIndex_26() const { return ____previousPageSelectionIndex_26; }
	inline int32_t* get_address_of__previousPageSelectionIndex_26() { return &____previousPageSelectionIndex_26; }
	inline void set__previousPageSelectionIndex_26(int32_t value)
	{
		____previousPageSelectionIndex_26 = value;
	}

	inline static int32_t get_offset_of__pageSelectionImages_27() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ____pageSelectionImages_27)); }
	inline List_1_t4142344393 * get__pageSelectionImages_27() const { return ____pageSelectionImages_27; }
	inline List_1_t4142344393 ** get_address_of__pageSelectionImages_27() { return &____pageSelectionImages_27; }
	inline void set__pageSelectionImages_27(List_1_t4142344393 * value)
	{
		____pageSelectionImages_27 = value;
		Il2CppCodeGenWriteBarrier((&____pageSelectionImages_27), value);
	}

	inline static int32_t get_offset_of_panel_28() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___panel_28)); }
	inline RectTransform_t3704657025 * get_panel_28() const { return ___panel_28; }
	inline RectTransform_t3704657025 ** get_address_of_panel_28() { return &___panel_28; }
	inline void set_panel_28(RectTransform_t3704657025 * value)
	{
		___panel_28 = value;
		Il2CppCodeGenWriteBarrier((&___panel_28), value);
	}

	inline static int32_t get_offset_of_panel1_29() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___panel1_29)); }
	inline RectTransform_t3704657025 * get_panel1_29() const { return ___panel1_29; }
	inline RectTransform_t3704657025 ** get_address_of_panel1_29() { return &___panel1_29; }
	inline void set_panel1_29(RectTransform_t3704657025 * value)
	{
		___panel1_29 = value;
		Il2CppCodeGenWriteBarrier((&___panel1_29), value);
	}

	inline static int32_t get_offset_of_panel2_30() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___panel2_30)); }
	inline RectTransform_t3704657025 * get_panel2_30() const { return ___panel2_30; }
	inline RectTransform_t3704657025 ** get_address_of_panel2_30() { return &___panel2_30; }
	inline void set_panel2_30(RectTransform_t3704657025 * value)
	{
		___panel2_30 = value;
		Il2CppCodeGenWriteBarrier((&___panel2_30), value);
	}

	inline static int32_t get_offset_of_bgPanel_31() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___bgPanel_31)); }
	inline GameObject_t1113636619 * get_bgPanel_31() const { return ___bgPanel_31; }
	inline GameObject_t1113636619 ** get_address_of_bgPanel_31() { return &___bgPanel_31; }
	inline void set_bgPanel_31(GameObject_t1113636619 * value)
	{
		___bgPanel_31 = value;
		Il2CppCodeGenWriteBarrier((&___bgPanel_31), value);
	}

	inline static int32_t get_offset_of_bgPanel1_32() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___bgPanel1_32)); }
	inline GameObject_t1113636619 * get_bgPanel1_32() const { return ___bgPanel1_32; }
	inline GameObject_t1113636619 ** get_address_of_bgPanel1_32() { return &___bgPanel1_32; }
	inline void set_bgPanel1_32(GameObject_t1113636619 * value)
	{
		___bgPanel1_32 = value;
		Il2CppCodeGenWriteBarrier((&___bgPanel1_32), value);
	}

	inline static int32_t get_offset_of_bgPanel2_33() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___bgPanel2_33)); }
	inline GameObject_t1113636619 * get_bgPanel2_33() const { return ___bgPanel2_33; }
	inline GameObject_t1113636619 ** get_address_of_bgPanel2_33() { return &___bgPanel2_33; }
	inline void set_bgPanel2_33(GameObject_t1113636619 * value)
	{
		___bgPanel2_33 = value;
		Il2CppCodeGenWriteBarrier((&___bgPanel2_33), value);
	}

	inline static int32_t get_offset_of_currentScale_34() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___currentScale_34)); }
	inline Vector2_t2156229523  get_currentScale_34() const { return ___currentScale_34; }
	inline Vector2_t2156229523 * get_address_of_currentScale_34() { return &___currentScale_34; }
	inline void set_currentScale_34(Vector2_t2156229523  value)
	{
		___currentScale_34 = value;
	}

	inline static int32_t get_offset_of_othersScale_35() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___othersScale_35)); }
	inline Vector2_t2156229523  get_othersScale_35() const { return ___othersScale_35; }
	inline Vector2_t2156229523 * get_address_of_othersScale_35() { return &___othersScale_35; }
	inline void set_othersScale_35(Vector2_t2156229523  value)
	{
		___othersScale_35 = value;
	}

	inline static int32_t get_offset_of_MinCache_36() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___MinCache_36)); }
	inline Vector3_t3722313464  get_MinCache_36() const { return ___MinCache_36; }
	inline Vector3_t3722313464 * get_address_of_MinCache_36() { return &___MinCache_36; }
	inline void set_MinCache_36(Vector3_t3722313464  value)
	{
		___MinCache_36 = value;
	}

	inline static int32_t get_offset_of_MaxCache_37() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t3278334712, ___MaxCache_37)); }
	inline Vector3_t3722313464  get_MaxCache_37() const { return ___MaxCache_37; }
	inline Vector3_t3722313464 * get_address_of_MaxCache_37() { return &___MaxCache_37; }
	inline void set_MaxCache_37(Vector3_t3722313464  value)
	{
		___MaxCache_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLSNAPRECT_T3278334712_H
#ifndef RECEIVERESULT_T2215153068_H
#define RECEIVERESULT_T2215153068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReceiveResult
struct  ReceiveResult_t2215153068  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text ReceiveResult::SpeechRecognizedText
	Text_t1901882714 * ___SpeechRecognizedText_2;

public:
	inline static int32_t get_offset_of_SpeechRecognizedText_2() { return static_cast<int32_t>(offsetof(ReceiveResult_t2215153068, ___SpeechRecognizedText_2)); }
	inline Text_t1901882714 * get_SpeechRecognizedText_2() const { return ___SpeechRecognizedText_2; }
	inline Text_t1901882714 ** get_address_of_SpeechRecognizedText_2() { return &___SpeechRecognizedText_2; }
	inline void set_SpeechRecognizedText_2(Text_t1901882714 * value)
	{
		___SpeechRecognizedText_2 = value;
		Il2CppCodeGenWriteBarrier((&___SpeechRecognizedText_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECEIVERESULT_T2215153068_H
#ifndef TEST_T650638817_H
#define TEST_T650638817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Test
struct  Test_t650638817  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button Test::btn
	Button_t4055032469 * ___btn_2;

public:
	inline static int32_t get_offset_of_btn_2() { return static_cast<int32_t>(offsetof(Test_t650638817, ___btn_2)); }
	inline Button_t4055032469 * get_btn_2() const { return ___btn_2; }
	inline Button_t4055032469 ** get_address_of_btn_2() { return &___btn_2; }
	inline void set_btn_2(Button_t4055032469 * value)
	{
		___btn_2 = value;
		Il2CppCodeGenWriteBarrier((&___btn_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEST_T650638817_H
#ifndef DEBUGGER_T3226592815_H
#define DEBUGGER_T3226592815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Generic.Debugger
struct  Debugger_t3226592815  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_T3226592815_H
#ifndef SNAPSCROLLRECT_T3165954182_H
#define SNAPSCROLLRECT_T3165954182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SnapScrollRect
struct  SnapScrollRect_t3165954182  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SnapScrollRect::Step
	float ___Step_2;
	// UnityEngine.UI.ScrollRect SnapScrollRect::AttachedScrollRect
	ScrollRect_t4137855814 * ___AttachedScrollRect_3;
	// System.Int32 SnapScrollRect::ContentAmount
	int32_t ___ContentAmount_4;
	// UnityEngine.Vector2 SnapScrollRect::TargetNormalizedPosition
	Vector2_t2156229523  ___TargetNormalizedPosition_5;
	// UnityEngine.Coroutine SnapScrollRect::ActiveUpdateRoutine
	Coroutine_t3829159415 * ___ActiveUpdateRoutine_6;

public:
	inline static int32_t get_offset_of_Step_2() { return static_cast<int32_t>(offsetof(SnapScrollRect_t3165954182, ___Step_2)); }
	inline float get_Step_2() const { return ___Step_2; }
	inline float* get_address_of_Step_2() { return &___Step_2; }
	inline void set_Step_2(float value)
	{
		___Step_2 = value;
	}

	inline static int32_t get_offset_of_AttachedScrollRect_3() { return static_cast<int32_t>(offsetof(SnapScrollRect_t3165954182, ___AttachedScrollRect_3)); }
	inline ScrollRect_t4137855814 * get_AttachedScrollRect_3() const { return ___AttachedScrollRect_3; }
	inline ScrollRect_t4137855814 ** get_address_of_AttachedScrollRect_3() { return &___AttachedScrollRect_3; }
	inline void set_AttachedScrollRect_3(ScrollRect_t4137855814 * value)
	{
		___AttachedScrollRect_3 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedScrollRect_3), value);
	}

	inline static int32_t get_offset_of_ContentAmount_4() { return static_cast<int32_t>(offsetof(SnapScrollRect_t3165954182, ___ContentAmount_4)); }
	inline int32_t get_ContentAmount_4() const { return ___ContentAmount_4; }
	inline int32_t* get_address_of_ContentAmount_4() { return &___ContentAmount_4; }
	inline void set_ContentAmount_4(int32_t value)
	{
		___ContentAmount_4 = value;
	}

	inline static int32_t get_offset_of_TargetNormalizedPosition_5() { return static_cast<int32_t>(offsetof(SnapScrollRect_t3165954182, ___TargetNormalizedPosition_5)); }
	inline Vector2_t2156229523  get_TargetNormalizedPosition_5() const { return ___TargetNormalizedPosition_5; }
	inline Vector2_t2156229523 * get_address_of_TargetNormalizedPosition_5() { return &___TargetNormalizedPosition_5; }
	inline void set_TargetNormalizedPosition_5(Vector2_t2156229523  value)
	{
		___TargetNormalizedPosition_5 = value;
	}

	inline static int32_t get_offset_of_ActiveUpdateRoutine_6() { return static_cast<int32_t>(offsetof(SnapScrollRect_t3165954182, ___ActiveUpdateRoutine_6)); }
	inline Coroutine_t3829159415 * get_ActiveUpdateRoutine_6() const { return ___ActiveUpdateRoutine_6; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveUpdateRoutine_6() { return &___ActiveUpdateRoutine_6; }
	inline void set_ActiveUpdateRoutine_6(Coroutine_t3829159415 * value)
	{
		___ActiveUpdateRoutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveUpdateRoutine_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPSCROLLRECT_T3165954182_H
#ifndef ASSETSMANAGER_T3069477283_H
#define ASSETSMANAGER_T3069477283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.AssetsManager
struct  AssetsManager_t3069477283  : public InstancedMonoBehaviour_1_t520384403
{
public:
	// UnityEngine.GameObject BMJ.M360.Artiz.AssetsManager::SplashCanvas
	GameObject_t1113636619 * ___SplashCanvas_3;
	// UnityEngine.GameObject BMJ.M360.Artiz.AssetsManager::HomePage
	GameObject_t1113636619 * ___HomePage_4;
	// System.Boolean BMJ.M360.Artiz.AssetsManager::EditorClearCache
	bool ___EditorClearCache_5;
	// System.Collections.Generic.List`1<BMJ.M360.Artiz.ArtizPreviewProperty> BMJ.M360.Artiz.AssetsManager::PreviewPropertyList
	List_1_t2968708851 * ___PreviewPropertyList_6;
	// BMJ.M360.Artiz.AssetsManager/SerializablePreviewProperties BMJ.M360.Artiz.AssetsManager::SerializablePreviewPropertiesObject
	SerializablePreviewProperties_t119401764 * ___SerializablePreviewPropertiesObject_7;
	// UnityEngine.Coroutine BMJ.M360.Artiz.AssetsManager::ActiveRoutine
	Coroutine_t3829159415 * ___ActiveRoutine_8;
	// UnityEngine.Coroutine BMJ.M360.Artiz.AssetsManager::ActiveRoutine2
	Coroutine_t3829159415 * ___ActiveRoutine2_9;
	// System.String BMJ.M360.Artiz.AssetsManager::ConfigurationRequestURL
	String_t* ___ConfigurationRequestURL_10;
	// System.String BMJ.M360.Artiz.AssetsManager::AssetsVersion
	String_t* ___AssetsVersion_11;

public:
	inline static int32_t get_offset_of_SplashCanvas_3() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___SplashCanvas_3)); }
	inline GameObject_t1113636619 * get_SplashCanvas_3() const { return ___SplashCanvas_3; }
	inline GameObject_t1113636619 ** get_address_of_SplashCanvas_3() { return &___SplashCanvas_3; }
	inline void set_SplashCanvas_3(GameObject_t1113636619 * value)
	{
		___SplashCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___SplashCanvas_3), value);
	}

	inline static int32_t get_offset_of_HomePage_4() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___HomePage_4)); }
	inline GameObject_t1113636619 * get_HomePage_4() const { return ___HomePage_4; }
	inline GameObject_t1113636619 ** get_address_of_HomePage_4() { return &___HomePage_4; }
	inline void set_HomePage_4(GameObject_t1113636619 * value)
	{
		___HomePage_4 = value;
		Il2CppCodeGenWriteBarrier((&___HomePage_4), value);
	}

	inline static int32_t get_offset_of_EditorClearCache_5() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___EditorClearCache_5)); }
	inline bool get_EditorClearCache_5() const { return ___EditorClearCache_5; }
	inline bool* get_address_of_EditorClearCache_5() { return &___EditorClearCache_5; }
	inline void set_EditorClearCache_5(bool value)
	{
		___EditorClearCache_5 = value;
	}

	inline static int32_t get_offset_of_PreviewPropertyList_6() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___PreviewPropertyList_6)); }
	inline List_1_t2968708851 * get_PreviewPropertyList_6() const { return ___PreviewPropertyList_6; }
	inline List_1_t2968708851 ** get_address_of_PreviewPropertyList_6() { return &___PreviewPropertyList_6; }
	inline void set_PreviewPropertyList_6(List_1_t2968708851 * value)
	{
		___PreviewPropertyList_6 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewPropertyList_6), value);
	}

	inline static int32_t get_offset_of_SerializablePreviewPropertiesObject_7() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___SerializablePreviewPropertiesObject_7)); }
	inline SerializablePreviewProperties_t119401764 * get_SerializablePreviewPropertiesObject_7() const { return ___SerializablePreviewPropertiesObject_7; }
	inline SerializablePreviewProperties_t119401764 ** get_address_of_SerializablePreviewPropertiesObject_7() { return &___SerializablePreviewPropertiesObject_7; }
	inline void set_SerializablePreviewPropertiesObject_7(SerializablePreviewProperties_t119401764 * value)
	{
		___SerializablePreviewPropertiesObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___SerializablePreviewPropertiesObject_7), value);
	}

	inline static int32_t get_offset_of_ActiveRoutine_8() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___ActiveRoutine_8)); }
	inline Coroutine_t3829159415 * get_ActiveRoutine_8() const { return ___ActiveRoutine_8; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveRoutine_8() { return &___ActiveRoutine_8; }
	inline void set_ActiveRoutine_8(Coroutine_t3829159415 * value)
	{
		___ActiveRoutine_8 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveRoutine_8), value);
	}

	inline static int32_t get_offset_of_ActiveRoutine2_9() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___ActiveRoutine2_9)); }
	inline Coroutine_t3829159415 * get_ActiveRoutine2_9() const { return ___ActiveRoutine2_9; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveRoutine2_9() { return &___ActiveRoutine2_9; }
	inline void set_ActiveRoutine2_9(Coroutine_t3829159415 * value)
	{
		___ActiveRoutine2_9 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveRoutine2_9), value);
	}

	inline static int32_t get_offset_of_ConfigurationRequestURL_10() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___ConfigurationRequestURL_10)); }
	inline String_t* get_ConfigurationRequestURL_10() const { return ___ConfigurationRequestURL_10; }
	inline String_t** get_address_of_ConfigurationRequestURL_10() { return &___ConfigurationRequestURL_10; }
	inline void set_ConfigurationRequestURL_10(String_t* value)
	{
		___ConfigurationRequestURL_10 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigurationRequestURL_10), value);
	}

	inline static int32_t get_offset_of_AssetsVersion_11() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283, ___AssetsVersion_11)); }
	inline String_t* get_AssetsVersion_11() const { return ___AssetsVersion_11; }
	inline String_t** get_address_of_AssetsVersion_11() { return &___AssetsVersion_11; }
	inline void set_AssetsVersion_11(String_t* value)
	{
		___AssetsVersion_11 = value;
		Il2CppCodeGenWriteBarrier((&___AssetsVersion_11), value);
	}
};

struct AssetsManager_t3069477283_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`1<BMJ.M360.Artiz.ArtizPreviewProperty> BMJ.M360.Artiz.AssetsManager::<>f__am$cache0
	UnityAction_1_t2081470250 * ___U3CU3Ef__amU24cache0_12;
	// UnityEngine.Events.UnityAction BMJ.M360.Artiz.AssetsManager::<>f__am$cache1
	UnityAction_t3245792599 * ___U3CU3Ef__amU24cache1_13;
	// UnityEngine.Events.UnityAction`1<BMJ.M360.Artiz.ArtizPreviewProperty> BMJ.M360.Artiz.AssetsManager::<>f__am$cache2
	UnityAction_1_t2081470250 * ___U3CU3Ef__amU24cache2_14;
	// UnityEngine.Events.UnityAction BMJ.M360.Artiz.AssetsManager::<>f__am$cache3
	UnityAction_t3245792599 * ___U3CU3Ef__amU24cache3_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline UnityAction_1_t2081470250 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline UnityAction_1_t2081470250 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(UnityAction_1_t2081470250 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline UnityAction_t3245792599 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline UnityAction_t3245792599 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(UnityAction_t3245792599 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_14() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283_StaticFields, ___U3CU3Ef__amU24cache2_14)); }
	inline UnityAction_1_t2081470250 * get_U3CU3Ef__amU24cache2_14() const { return ___U3CU3Ef__amU24cache2_14; }
	inline UnityAction_1_t2081470250 ** get_address_of_U3CU3Ef__amU24cache2_14() { return &___U3CU3Ef__amU24cache2_14; }
	inline void set_U3CU3Ef__amU24cache2_14(UnityAction_1_t2081470250 * value)
	{
		___U3CU3Ef__amU24cache2_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_15() { return static_cast<int32_t>(offsetof(AssetsManager_t3069477283_StaticFields, ___U3CU3Ef__amU24cache3_15)); }
	inline UnityAction_t3245792599 * get_U3CU3Ef__amU24cache3_15() const { return ___U3CU3Ef__amU24cache3_15; }
	inline UnityAction_t3245792599 ** get_address_of_U3CU3Ef__amU24cache3_15() { return &___U3CU3Ef__amU24cache3_15; }
	inline void set_U3CU3Ef__amU24cache3_15(UnityAction_t3245792599 * value)
	{
		___U3CU3Ef__amU24cache3_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETSMANAGER_T3069477283_H
#ifndef SCROLLINGCONTROLLER_T1982690304_H
#define SCROLLINGCONTROLLER_T1982690304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.Scroller.ScrollingController
struct  ScrollingController_t1982690304  : public InstancedMonoBehaviour_1_t3728564720
{
public:
	// System.Collections.Generic.List`1<BMJ.M360.Artiz.Scroller.ScrollingController/CategorySelector> BMJ.M360.Artiz.Scroller.ScrollingController::CategorySelectors
	List_1_t3540556495 * ___CategorySelectors_3;

public:
	inline static int32_t get_offset_of_CategorySelectors_3() { return static_cast<int32_t>(offsetof(ScrollingController_t1982690304, ___CategorySelectors_3)); }
	inline List_1_t3540556495 * get_CategorySelectors_3() const { return ___CategorySelectors_3; }
	inline List_1_t3540556495 ** get_address_of_CategorySelectors_3() { return &___CategorySelectors_3; }
	inline void set_CategorySelectors_3(List_1_t3540556495 * value)
	{
		___CategorySelectors_3 = value;
		Il2CppCodeGenWriteBarrier((&___CategorySelectors_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINGCONTROLLER_T1982690304_H
#ifndef PREVIEWBUTTONSMANAGER2_T2759123039_H
#define PREVIEWBUTTONSMANAGER2_T2759123039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.PreviewButtonsManager2
struct  PreviewButtonsManager2_t2759123039  : public InstancedMonoBehaviour_1_t210030159
{
public:
	// UnityEngine.GameObject BMJ.M360.Artiz.PreviewButtonsManager2::PreviewPrefab
	GameObject_t1113636619 * ___PreviewPrefab_3;
	// UnityEngine.GameObject BMJ.M360.Artiz.PreviewButtonsManager2::PreviewSpacerPrefab
	GameObject_t1113636619 * ___PreviewSpacerPrefab_4;
	// UnityEngine.Transform BMJ.M360.Artiz.PreviewButtonsManager2::PreviewHolder
	Transform_t3600365921 * ___PreviewHolder_5;
	// UnityEngine.RectTransform BMJ.M360.Artiz.PreviewButtonsManager2::HeightReference
	RectTransform_t3704657025 * ___HeightReference_6;
	// System.Boolean BMJ.M360.Artiz.PreviewButtonsManager2::InitialButtonFlag
	bool ___InitialButtonFlag_7;

public:
	inline static int32_t get_offset_of_PreviewPrefab_3() { return static_cast<int32_t>(offsetof(PreviewButtonsManager2_t2759123039, ___PreviewPrefab_3)); }
	inline GameObject_t1113636619 * get_PreviewPrefab_3() const { return ___PreviewPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_PreviewPrefab_3() { return &___PreviewPrefab_3; }
	inline void set_PreviewPrefab_3(GameObject_t1113636619 * value)
	{
		___PreviewPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewPrefab_3), value);
	}

	inline static int32_t get_offset_of_PreviewSpacerPrefab_4() { return static_cast<int32_t>(offsetof(PreviewButtonsManager2_t2759123039, ___PreviewSpacerPrefab_4)); }
	inline GameObject_t1113636619 * get_PreviewSpacerPrefab_4() const { return ___PreviewSpacerPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_PreviewSpacerPrefab_4() { return &___PreviewSpacerPrefab_4; }
	inline void set_PreviewSpacerPrefab_4(GameObject_t1113636619 * value)
	{
		___PreviewSpacerPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewSpacerPrefab_4), value);
	}

	inline static int32_t get_offset_of_PreviewHolder_5() { return static_cast<int32_t>(offsetof(PreviewButtonsManager2_t2759123039, ___PreviewHolder_5)); }
	inline Transform_t3600365921 * get_PreviewHolder_5() const { return ___PreviewHolder_5; }
	inline Transform_t3600365921 ** get_address_of_PreviewHolder_5() { return &___PreviewHolder_5; }
	inline void set_PreviewHolder_5(Transform_t3600365921 * value)
	{
		___PreviewHolder_5 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewHolder_5), value);
	}

	inline static int32_t get_offset_of_HeightReference_6() { return static_cast<int32_t>(offsetof(PreviewButtonsManager2_t2759123039, ___HeightReference_6)); }
	inline RectTransform_t3704657025 * get_HeightReference_6() const { return ___HeightReference_6; }
	inline RectTransform_t3704657025 ** get_address_of_HeightReference_6() { return &___HeightReference_6; }
	inline void set_HeightReference_6(RectTransform_t3704657025 * value)
	{
		___HeightReference_6 = value;
		Il2CppCodeGenWriteBarrier((&___HeightReference_6), value);
	}

	inline static int32_t get_offset_of_InitialButtonFlag_7() { return static_cast<int32_t>(offsetof(PreviewButtonsManager2_t2759123039, ___InitialButtonFlag_7)); }
	inline bool get_InitialButtonFlag_7() const { return ___InitialButtonFlag_7; }
	inline bool* get_address_of_InitialButtonFlag_7() { return &___InitialButtonFlag_7; }
	inline void set_InitialButtonFlag_7(bool value)
	{
		___InitialButtonFlag_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWBUTTONSMANAGER2_T2759123039_H
#ifndef PREVIEWMANAGER_T2255819439_H
#define PREVIEWMANAGER_T2255819439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.PreviewManager
struct  PreviewManager_t2255819439  : public InstancedMonoBehaviour_1_t4001693855
{
public:
	// UnityEngine.Transform BMJ.M360.Artiz.PreviewManager::HDRIContentHolder
	Transform_t3600365921 * ___HDRIContentHolder_3;
	// UnityEngine.GameObject BMJ.M360.Artiz.PreviewManager::HDRIPrefab
	GameObject_t1113636619 * ___HDRIPrefab_4;
	// UnityEngine.GameObject BMJ.M360.Artiz.PreviewManager::LoadingHDRIPrefab
	GameObject_t1113636619 * ___LoadingHDRIPrefab_5;
	// UnityEngine.Material BMJ.M360.Artiz.PreviewManager::HDRIMaterial
	Material_t340375123 * ___HDRIMaterial_6;
	// UnityEngine.Shader BMJ.M360.Artiz.PreviewManager::CubemapShader
	Shader_t4151988712 * ___CubemapShader_7;
	// UnityEngine.Shader BMJ.M360.Artiz.PreviewManager::UnlitTransparentShader
	Shader_t4151988712 * ___UnlitTransparentShader_8;
	// System.Boolean BMJ.M360.Artiz.PreviewManager::InitialPreview
	bool ___InitialPreview_9;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle> BMJ.M360.Artiz.PreviewManager::HDRIReference
	Dictionary_2_t939163551 * ___HDRIReference_11;
	// System.Collections.Generic.List`1<System.String> BMJ.M360.Artiz.PreviewManager::LoadingReferenceTable
	List_1_t3319525431 * ___LoadingReferenceTable_12;

public:
	inline static int32_t get_offset_of_HDRIContentHolder_3() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___HDRIContentHolder_3)); }
	inline Transform_t3600365921 * get_HDRIContentHolder_3() const { return ___HDRIContentHolder_3; }
	inline Transform_t3600365921 ** get_address_of_HDRIContentHolder_3() { return &___HDRIContentHolder_3; }
	inline void set_HDRIContentHolder_3(Transform_t3600365921 * value)
	{
		___HDRIContentHolder_3 = value;
		Il2CppCodeGenWriteBarrier((&___HDRIContentHolder_3), value);
	}

	inline static int32_t get_offset_of_HDRIPrefab_4() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___HDRIPrefab_4)); }
	inline GameObject_t1113636619 * get_HDRIPrefab_4() const { return ___HDRIPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_HDRIPrefab_4() { return &___HDRIPrefab_4; }
	inline void set_HDRIPrefab_4(GameObject_t1113636619 * value)
	{
		___HDRIPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___HDRIPrefab_4), value);
	}

	inline static int32_t get_offset_of_LoadingHDRIPrefab_5() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___LoadingHDRIPrefab_5)); }
	inline GameObject_t1113636619 * get_LoadingHDRIPrefab_5() const { return ___LoadingHDRIPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_LoadingHDRIPrefab_5() { return &___LoadingHDRIPrefab_5; }
	inline void set_LoadingHDRIPrefab_5(GameObject_t1113636619 * value)
	{
		___LoadingHDRIPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingHDRIPrefab_5), value);
	}

	inline static int32_t get_offset_of_HDRIMaterial_6() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___HDRIMaterial_6)); }
	inline Material_t340375123 * get_HDRIMaterial_6() const { return ___HDRIMaterial_6; }
	inline Material_t340375123 ** get_address_of_HDRIMaterial_6() { return &___HDRIMaterial_6; }
	inline void set_HDRIMaterial_6(Material_t340375123 * value)
	{
		___HDRIMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___HDRIMaterial_6), value);
	}

	inline static int32_t get_offset_of_CubemapShader_7() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___CubemapShader_7)); }
	inline Shader_t4151988712 * get_CubemapShader_7() const { return ___CubemapShader_7; }
	inline Shader_t4151988712 ** get_address_of_CubemapShader_7() { return &___CubemapShader_7; }
	inline void set_CubemapShader_7(Shader_t4151988712 * value)
	{
		___CubemapShader_7 = value;
		Il2CppCodeGenWriteBarrier((&___CubemapShader_7), value);
	}

	inline static int32_t get_offset_of_UnlitTransparentShader_8() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___UnlitTransparentShader_8)); }
	inline Shader_t4151988712 * get_UnlitTransparentShader_8() const { return ___UnlitTransparentShader_8; }
	inline Shader_t4151988712 ** get_address_of_UnlitTransparentShader_8() { return &___UnlitTransparentShader_8; }
	inline void set_UnlitTransparentShader_8(Shader_t4151988712 * value)
	{
		___UnlitTransparentShader_8 = value;
		Il2CppCodeGenWriteBarrier((&___UnlitTransparentShader_8), value);
	}

	inline static int32_t get_offset_of_InitialPreview_9() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___InitialPreview_9)); }
	inline bool get_InitialPreview_9() const { return ___InitialPreview_9; }
	inline bool* get_address_of_InitialPreview_9() { return &___InitialPreview_9; }
	inline void set_InitialPreview_9(bool value)
	{
		___InitialPreview_9 = value;
	}

	inline static int32_t get_offset_of_HDRIReference_11() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___HDRIReference_11)); }
	inline Dictionary_2_t939163551 * get_HDRIReference_11() const { return ___HDRIReference_11; }
	inline Dictionary_2_t939163551 ** get_address_of_HDRIReference_11() { return &___HDRIReference_11; }
	inline void set_HDRIReference_11(Dictionary_2_t939163551 * value)
	{
		___HDRIReference_11 = value;
		Il2CppCodeGenWriteBarrier((&___HDRIReference_11), value);
	}

	inline static int32_t get_offset_of_LoadingReferenceTable_12() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439, ___LoadingReferenceTable_12)); }
	inline List_1_t3319525431 * get_LoadingReferenceTable_12() const { return ___LoadingReferenceTable_12; }
	inline List_1_t3319525431 ** get_address_of_LoadingReferenceTable_12() { return &___LoadingReferenceTable_12; }
	inline void set_LoadingReferenceTable_12(List_1_t3319525431 * value)
	{
		___LoadingReferenceTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingReferenceTable_12), value);
	}
};

struct PreviewManager_t2255819439_StaticFields
{
public:
	// System.Int32 BMJ.M360.Artiz.PreviewManager::CustomStartIndex
	int32_t ___CustomStartIndex_10;

public:
	inline static int32_t get_offset_of_CustomStartIndex_10() { return static_cast<int32_t>(offsetof(PreviewManager_t2255819439_StaticFields, ___CustomStartIndex_10)); }
	inline int32_t get_CustomStartIndex_10() const { return ___CustomStartIndex_10; }
	inline int32_t* get_address_of_CustomStartIndex_10() { return &___CustomStartIndex_10; }
	inline void set_CustomStartIndex_10(int32_t value)
	{
		___CustomStartIndex_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWMANAGER_T2255819439_H
#ifndef SCROLLINGCONTROLLER2_T2836624384_H
#define SCROLLINGCONTROLLER2_T2836624384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.Scroller.ScrollingController2
struct  ScrollingController2_t2836624384  : public InstancedMonoBehaviour_1_t287531504
{
public:
	// System.Collections.Generic.List`1<BMJ.M360.Artiz.Scroller.ScrollingController2/CategorySelector> BMJ.M360.Artiz.Scroller.ScrollingController2::CategorySelectors
	List_1_t1950397391 * ___CategorySelectors_3;

public:
	inline static int32_t get_offset_of_CategorySelectors_3() { return static_cast<int32_t>(offsetof(ScrollingController2_t2836624384, ___CategorySelectors_3)); }
	inline List_1_t1950397391 * get_CategorySelectors_3() const { return ___CategorySelectors_3; }
	inline List_1_t1950397391 ** get_address_of_CategorySelectors_3() { return &___CategorySelectors_3; }
	inline void set_CategorySelectors_3(List_1_t1950397391 * value)
	{
		___CategorySelectors_3 = value;
		Il2CppCodeGenWriteBarrier((&___CategorySelectors_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINGCONTROLLER2_T2836624384_H
#ifndef TOGGLEVIEWCONTROLLER_T44480253_H
#define TOGGLEVIEWCONTROLLER_T44480253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.ToggleViewController
struct  ToggleViewController_t44480253  : public InstancedMonoBehaviour_1_t1790354669
{
public:
	// BMJ.M360.Artiz.CustomFitScroller BMJ.M360.Artiz.ToggleViewController::AttachedCustomFitScroller
	CustomFitScroller_t3272853734 * ___AttachedCustomFitScroller_3;
	// UnityEngine.GameObject BMJ.M360.Artiz.ToggleViewController::ToggleDownButtonObject
	GameObject_t1113636619 * ___ToggleDownButtonObject_4;
	// UnityEngine.UI.Button BMJ.M360.Artiz.ToggleViewController::ToggleUpButton
	Button_t4055032469 * ___ToggleUpButton_5;
	// UnityEngine.GameObject BMJ.M360.Artiz.ToggleViewController::VRModeButton
	GameObject_t1113636619 * ___VRModeButton_6;
	// BMJ.M360.CustomCamera.AxisRotationCamera BMJ.M360.Artiz.ToggleViewController::AttachedAxisRotationCamera
	AxisRotationCamera_t2455264440 * ___AttachedAxisRotationCamera_7;

public:
	inline static int32_t get_offset_of_AttachedCustomFitScroller_3() { return static_cast<int32_t>(offsetof(ToggleViewController_t44480253, ___AttachedCustomFitScroller_3)); }
	inline CustomFitScroller_t3272853734 * get_AttachedCustomFitScroller_3() const { return ___AttachedCustomFitScroller_3; }
	inline CustomFitScroller_t3272853734 ** get_address_of_AttachedCustomFitScroller_3() { return &___AttachedCustomFitScroller_3; }
	inline void set_AttachedCustomFitScroller_3(CustomFitScroller_t3272853734 * value)
	{
		___AttachedCustomFitScroller_3 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedCustomFitScroller_3), value);
	}

	inline static int32_t get_offset_of_ToggleDownButtonObject_4() { return static_cast<int32_t>(offsetof(ToggleViewController_t44480253, ___ToggleDownButtonObject_4)); }
	inline GameObject_t1113636619 * get_ToggleDownButtonObject_4() const { return ___ToggleDownButtonObject_4; }
	inline GameObject_t1113636619 ** get_address_of_ToggleDownButtonObject_4() { return &___ToggleDownButtonObject_4; }
	inline void set_ToggleDownButtonObject_4(GameObject_t1113636619 * value)
	{
		___ToggleDownButtonObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleDownButtonObject_4), value);
	}

	inline static int32_t get_offset_of_ToggleUpButton_5() { return static_cast<int32_t>(offsetof(ToggleViewController_t44480253, ___ToggleUpButton_5)); }
	inline Button_t4055032469 * get_ToggleUpButton_5() const { return ___ToggleUpButton_5; }
	inline Button_t4055032469 ** get_address_of_ToggleUpButton_5() { return &___ToggleUpButton_5; }
	inline void set_ToggleUpButton_5(Button_t4055032469 * value)
	{
		___ToggleUpButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___ToggleUpButton_5), value);
	}

	inline static int32_t get_offset_of_VRModeButton_6() { return static_cast<int32_t>(offsetof(ToggleViewController_t44480253, ___VRModeButton_6)); }
	inline GameObject_t1113636619 * get_VRModeButton_6() const { return ___VRModeButton_6; }
	inline GameObject_t1113636619 ** get_address_of_VRModeButton_6() { return &___VRModeButton_6; }
	inline void set_VRModeButton_6(GameObject_t1113636619 * value)
	{
		___VRModeButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___VRModeButton_6), value);
	}

	inline static int32_t get_offset_of_AttachedAxisRotationCamera_7() { return static_cast<int32_t>(offsetof(ToggleViewController_t44480253, ___AttachedAxisRotationCamera_7)); }
	inline AxisRotationCamera_t2455264440 * get_AttachedAxisRotationCamera_7() const { return ___AttachedAxisRotationCamera_7; }
	inline AxisRotationCamera_t2455264440 ** get_address_of_AttachedAxisRotationCamera_7() { return &___AttachedAxisRotationCamera_7; }
	inline void set_AttachedAxisRotationCamera_7(AxisRotationCamera_t2455264440 * value)
	{
		___AttachedAxisRotationCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedAxisRotationCamera_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEVIEWCONTROLLER_T44480253_H
#ifndef AXISROTATIONCAMERA_T2455264440_H
#define AXISROTATIONCAMERA_T2455264440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.CustomCamera.AxisRotationCamera
struct  AxisRotationCamera_t2455264440  : public InstancedMonoBehaviour_1_t4201138856
{
public:
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::MobileRotationMultiplier
	float ___MobileRotationMultiplier_3;
	// UnityEngine.Camera BMJ.M360.CustomCamera.AxisRotationCamera::MainCamera
	Camera_t4157153871 * ___MainCamera_4;
	// UnityEngine.Camera BMJ.M360.CustomCamera.AxisRotationCamera::AttachedCamera
	Camera_t4157153871 * ___AttachedCamera_5;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::GyroSupportErrorBox
	GameObject_t1113636619 * ___GyroSupportErrorBox_6;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera::GyroSupport
	bool ___GyroSupport_7;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera::InCategory
	bool ___InCategory_8;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::HorizontalSpeed
	float ___HorizontalSpeed_9;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::VerticalSpeed
	float ___VerticalSpeed_10;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::CameraYaw
	float ___CameraYaw_11;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::CameraPitch
	float ___CameraPitch_12;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::YawClamp
	float ___YawClamp_13;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::YClamp
	float ___YClamp_14;
	// UnityEngine.Transform BMJ.M360.CustomCamera.AxisRotationCamera::Marker
	Transform_t3600365921 * ___Marker_15;
	// UnityEngine.Transform BMJ.M360.CustomCamera.AxisRotationCamera::CameraTransform
	Transform_t3600365921 * ___CameraTransform_16;
	// UnityEngine.Transform BMJ.M360.CustomCamera.AxisRotationCamera::LookSphere
	Transform_t3600365921 * ___LookSphere_17;
	// UnityEngine.Transform BMJ.M360.CustomCamera.AxisRotationCamera::LookTarget
	Transform_t3600365921 * ___LookTarget_18;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::LookTargetYCache
	float ___LookTargetYCache_19;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::LookTargetDistance
	float ___LookTargetDistance_20;
	// UnityEngine.Vector3 BMJ.M360.CustomCamera.AxisRotationCamera::LookTargetPoint
	Vector3_t3722313464  ___LookTargetPoint_21;
	// UnityEngine.Transform BMJ.M360.CustomCamera.AxisRotationCamera::TapSphere
	Transform_t3600365921 * ___TapSphere_22;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::TapTargetDistance
	float ___TapTargetDistance_23;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::TapYPosition
	float ___TapYPosition_24;
	// UnityEngine.Quaternion BMJ.M360.CustomCamera.AxisRotationCamera::TapRotationCache
	Quaternion_t2301928331  ___TapRotationCache_25;
	// UnityEngine.Vector3 BMJ.M360.CustomCamera.AxisRotationCamera::TapDelta
	Vector3_t3722313464  ___TapDelta_26;
	// UnityEngine.Transform BMJ.M360.CustomCamera.AxisRotationCamera::GyroSphere
	Transform_t3600365921 * ___GyroSphere_27;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::GyroTargetDistance
	float ___GyroTargetDistance_28;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::GyroYPosition
	float ___GyroYPosition_29;
	// UnityEngine.Quaternion BMJ.M360.CustomCamera.AxisRotationCamera::GyroRotationCache
	Quaternion_t2301928331  ___GyroRotationCache_30;
	// UnityEngine.Vector3 BMJ.M360.CustomCamera.AxisRotationCamera::GyroDelta
	Vector3_t3722313464  ___GyroDelta_31;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::DefaultFOV
	float ___DefaultFOV_32;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::Layout
	GameObject_t1113636619 * ___Layout_33;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::CardboardLayout
	GameObject_t1113636619 * ___CardboardLayout_34;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::VRSplash
	GameObject_t1113636619 * ___VRSplash_35;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::Dot
	GameObject_t1113636619 * ___Dot_36;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::VRRaycaster
	GameObject_t1113636619 * ___VRRaycaster_37;
	// System.String BMJ.M360.CustomCamera.AxisRotationCamera::CurrentXRDevice
	String_t* ___CurrentXRDevice_38;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::CrosshairObject
	GameObject_t1113636619 * ___CrosshairObject_39;
	// UnityEngine.UI.Image BMJ.M360.CustomCamera.AxisRotationCamera::CrosshairFace
	Image_t2670269651 * ___CrosshairFace_40;
	// UnityEngine.UI.Image BMJ.M360.CustomCamera.AxisRotationCamera::CrosshairRing
	Image_t2670269651 * ___CrosshairRing_41;
	// UnityEngine.UI.Text BMJ.M360.CustomCamera.AxisRotationCamera::CrosshairCountdownUI
	Text_t1901882714 * ___CrosshairCountdownUI_42;
	// UnityEngine.Color BMJ.M360.CustomCamera.AxisRotationCamera::ActiveColor
	Color_t2555686324  ___ActiveColor_43;
	// UnityEngine.Color BMJ.M360.CustomCamera.AxisRotationCamera::InactiveColor
	Color_t2555686324  ___InactiveColor_44;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera::CrosshairSupported
	bool ___CrosshairSupported_45;
	// UnityEngine.GameObject BMJ.M360.CustomCamera.AxisRotationCamera::LoadingObject
	GameObject_t1113636619 * ___LoadingObject_46;
	// UnityEngine.UI.Image BMJ.M360.CustomCamera.AxisRotationCamera::LoadingFace
	Image_t2670269651 * ___LoadingFace_47;
	// UnityEngine.UI.Text BMJ.M360.CustomCamera.AxisRotationCamera::LoadingProgress
	Text_t1901882714 * ___LoadingProgress_48;
	// System.Single BMJ.M360.CustomCamera.AxisRotationCamera::RotationMultiplier
	float ___RotationMultiplier_49;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera::Tap
	bool ___Tap_50;
	// UnityEngine.Coroutine BMJ.M360.CustomCamera.AxisRotationCamera::ActiveVRRoutine
	Coroutine_t3829159415 * ___ActiveVRRoutine_51;
	// UnityEngine.UI.ScrollRect BMJ.M360.CustomCamera.AxisRotationCamera::Scroll
	ScrollRect_t4137855814 * ___Scroll_52;
	// UnityEngine.UI.ScrollRect BMJ.M360.CustomCamera.AxisRotationCamera::Scroll1
	ScrollRect_t4137855814 * ___Scroll1_53;
	// UnityEngine.UI.ScrollRect BMJ.M360.CustomCamera.AxisRotationCamera::Scroll2
	ScrollRect_t4137855814 * ___Scroll2_54;
	// BMJ.M360.CustomCamera.AxisRotationCamera/VRState BMJ.M360.CustomCamera.AxisRotationCamera::CurrentVRState
	int32_t ___CurrentVRState_55;
	// BMJ.M360.CustomCamera.AxisRotationCamera/VRState BMJ.M360.CustomCamera.AxisRotationCamera::TargetVRState
	int32_t ___TargetVRState_56;
	// UnityEngine.Coroutine BMJ.M360.CustomCamera.AxisRotationCamera::ActiveCardboardRoutine
	Coroutine_t3829159415 * ___ActiveCardboardRoutine_57;
	// UnityEngine.Vector3 BMJ.M360.CustomCamera.AxisRotationCamera::vector
	Vector3_t3722313464  ___vector_58;
	// System.Boolean BMJ.M360.CustomCamera.AxisRotationCamera::tapCorrection
	bool ___tapCorrection_59;

public:
	inline static int32_t get_offset_of_MobileRotationMultiplier_3() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___MobileRotationMultiplier_3)); }
	inline float get_MobileRotationMultiplier_3() const { return ___MobileRotationMultiplier_3; }
	inline float* get_address_of_MobileRotationMultiplier_3() { return &___MobileRotationMultiplier_3; }
	inline void set_MobileRotationMultiplier_3(float value)
	{
		___MobileRotationMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_MainCamera_4() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___MainCamera_4)); }
	inline Camera_t4157153871 * get_MainCamera_4() const { return ___MainCamera_4; }
	inline Camera_t4157153871 ** get_address_of_MainCamera_4() { return &___MainCamera_4; }
	inline void set_MainCamera_4(Camera_t4157153871 * value)
	{
		___MainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___MainCamera_4), value);
	}

	inline static int32_t get_offset_of_AttachedCamera_5() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___AttachedCamera_5)); }
	inline Camera_t4157153871 * get_AttachedCamera_5() const { return ___AttachedCamera_5; }
	inline Camera_t4157153871 ** get_address_of_AttachedCamera_5() { return &___AttachedCamera_5; }
	inline void set_AttachedCamera_5(Camera_t4157153871 * value)
	{
		___AttachedCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___AttachedCamera_5), value);
	}

	inline static int32_t get_offset_of_GyroSupportErrorBox_6() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroSupportErrorBox_6)); }
	inline GameObject_t1113636619 * get_GyroSupportErrorBox_6() const { return ___GyroSupportErrorBox_6; }
	inline GameObject_t1113636619 ** get_address_of_GyroSupportErrorBox_6() { return &___GyroSupportErrorBox_6; }
	inline void set_GyroSupportErrorBox_6(GameObject_t1113636619 * value)
	{
		___GyroSupportErrorBox_6 = value;
		Il2CppCodeGenWriteBarrier((&___GyroSupportErrorBox_6), value);
	}

	inline static int32_t get_offset_of_GyroSupport_7() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroSupport_7)); }
	inline bool get_GyroSupport_7() const { return ___GyroSupport_7; }
	inline bool* get_address_of_GyroSupport_7() { return &___GyroSupport_7; }
	inline void set_GyroSupport_7(bool value)
	{
		___GyroSupport_7 = value;
	}

	inline static int32_t get_offset_of_InCategory_8() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___InCategory_8)); }
	inline bool get_InCategory_8() const { return ___InCategory_8; }
	inline bool* get_address_of_InCategory_8() { return &___InCategory_8; }
	inline void set_InCategory_8(bool value)
	{
		___InCategory_8 = value;
	}

	inline static int32_t get_offset_of_HorizontalSpeed_9() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___HorizontalSpeed_9)); }
	inline float get_HorizontalSpeed_9() const { return ___HorizontalSpeed_9; }
	inline float* get_address_of_HorizontalSpeed_9() { return &___HorizontalSpeed_9; }
	inline void set_HorizontalSpeed_9(float value)
	{
		___HorizontalSpeed_9 = value;
	}

	inline static int32_t get_offset_of_VerticalSpeed_10() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___VerticalSpeed_10)); }
	inline float get_VerticalSpeed_10() const { return ___VerticalSpeed_10; }
	inline float* get_address_of_VerticalSpeed_10() { return &___VerticalSpeed_10; }
	inline void set_VerticalSpeed_10(float value)
	{
		___VerticalSpeed_10 = value;
	}

	inline static int32_t get_offset_of_CameraYaw_11() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CameraYaw_11)); }
	inline float get_CameraYaw_11() const { return ___CameraYaw_11; }
	inline float* get_address_of_CameraYaw_11() { return &___CameraYaw_11; }
	inline void set_CameraYaw_11(float value)
	{
		___CameraYaw_11 = value;
	}

	inline static int32_t get_offset_of_CameraPitch_12() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CameraPitch_12)); }
	inline float get_CameraPitch_12() const { return ___CameraPitch_12; }
	inline float* get_address_of_CameraPitch_12() { return &___CameraPitch_12; }
	inline void set_CameraPitch_12(float value)
	{
		___CameraPitch_12 = value;
	}

	inline static int32_t get_offset_of_YawClamp_13() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___YawClamp_13)); }
	inline float get_YawClamp_13() const { return ___YawClamp_13; }
	inline float* get_address_of_YawClamp_13() { return &___YawClamp_13; }
	inline void set_YawClamp_13(float value)
	{
		___YawClamp_13 = value;
	}

	inline static int32_t get_offset_of_YClamp_14() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___YClamp_14)); }
	inline float get_YClamp_14() const { return ___YClamp_14; }
	inline float* get_address_of_YClamp_14() { return &___YClamp_14; }
	inline void set_YClamp_14(float value)
	{
		___YClamp_14 = value;
	}

	inline static int32_t get_offset_of_Marker_15() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Marker_15)); }
	inline Transform_t3600365921 * get_Marker_15() const { return ___Marker_15; }
	inline Transform_t3600365921 ** get_address_of_Marker_15() { return &___Marker_15; }
	inline void set_Marker_15(Transform_t3600365921 * value)
	{
		___Marker_15 = value;
		Il2CppCodeGenWriteBarrier((&___Marker_15), value);
	}

	inline static int32_t get_offset_of_CameraTransform_16() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CameraTransform_16)); }
	inline Transform_t3600365921 * get_CameraTransform_16() const { return ___CameraTransform_16; }
	inline Transform_t3600365921 ** get_address_of_CameraTransform_16() { return &___CameraTransform_16; }
	inline void set_CameraTransform_16(Transform_t3600365921 * value)
	{
		___CameraTransform_16 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTransform_16), value);
	}

	inline static int32_t get_offset_of_LookSphere_17() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LookSphere_17)); }
	inline Transform_t3600365921 * get_LookSphere_17() const { return ___LookSphere_17; }
	inline Transform_t3600365921 ** get_address_of_LookSphere_17() { return &___LookSphere_17; }
	inline void set_LookSphere_17(Transform_t3600365921 * value)
	{
		___LookSphere_17 = value;
		Il2CppCodeGenWriteBarrier((&___LookSphere_17), value);
	}

	inline static int32_t get_offset_of_LookTarget_18() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LookTarget_18)); }
	inline Transform_t3600365921 * get_LookTarget_18() const { return ___LookTarget_18; }
	inline Transform_t3600365921 ** get_address_of_LookTarget_18() { return &___LookTarget_18; }
	inline void set_LookTarget_18(Transform_t3600365921 * value)
	{
		___LookTarget_18 = value;
		Il2CppCodeGenWriteBarrier((&___LookTarget_18), value);
	}

	inline static int32_t get_offset_of_LookTargetYCache_19() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LookTargetYCache_19)); }
	inline float get_LookTargetYCache_19() const { return ___LookTargetYCache_19; }
	inline float* get_address_of_LookTargetYCache_19() { return &___LookTargetYCache_19; }
	inline void set_LookTargetYCache_19(float value)
	{
		___LookTargetYCache_19 = value;
	}

	inline static int32_t get_offset_of_LookTargetDistance_20() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LookTargetDistance_20)); }
	inline float get_LookTargetDistance_20() const { return ___LookTargetDistance_20; }
	inline float* get_address_of_LookTargetDistance_20() { return &___LookTargetDistance_20; }
	inline void set_LookTargetDistance_20(float value)
	{
		___LookTargetDistance_20 = value;
	}

	inline static int32_t get_offset_of_LookTargetPoint_21() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LookTargetPoint_21)); }
	inline Vector3_t3722313464  get_LookTargetPoint_21() const { return ___LookTargetPoint_21; }
	inline Vector3_t3722313464 * get_address_of_LookTargetPoint_21() { return &___LookTargetPoint_21; }
	inline void set_LookTargetPoint_21(Vector3_t3722313464  value)
	{
		___LookTargetPoint_21 = value;
	}

	inline static int32_t get_offset_of_TapSphere_22() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___TapSphere_22)); }
	inline Transform_t3600365921 * get_TapSphere_22() const { return ___TapSphere_22; }
	inline Transform_t3600365921 ** get_address_of_TapSphere_22() { return &___TapSphere_22; }
	inline void set_TapSphere_22(Transform_t3600365921 * value)
	{
		___TapSphere_22 = value;
		Il2CppCodeGenWriteBarrier((&___TapSphere_22), value);
	}

	inline static int32_t get_offset_of_TapTargetDistance_23() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___TapTargetDistance_23)); }
	inline float get_TapTargetDistance_23() const { return ___TapTargetDistance_23; }
	inline float* get_address_of_TapTargetDistance_23() { return &___TapTargetDistance_23; }
	inline void set_TapTargetDistance_23(float value)
	{
		___TapTargetDistance_23 = value;
	}

	inline static int32_t get_offset_of_TapYPosition_24() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___TapYPosition_24)); }
	inline float get_TapYPosition_24() const { return ___TapYPosition_24; }
	inline float* get_address_of_TapYPosition_24() { return &___TapYPosition_24; }
	inline void set_TapYPosition_24(float value)
	{
		___TapYPosition_24 = value;
	}

	inline static int32_t get_offset_of_TapRotationCache_25() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___TapRotationCache_25)); }
	inline Quaternion_t2301928331  get_TapRotationCache_25() const { return ___TapRotationCache_25; }
	inline Quaternion_t2301928331 * get_address_of_TapRotationCache_25() { return &___TapRotationCache_25; }
	inline void set_TapRotationCache_25(Quaternion_t2301928331  value)
	{
		___TapRotationCache_25 = value;
	}

	inline static int32_t get_offset_of_TapDelta_26() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___TapDelta_26)); }
	inline Vector3_t3722313464  get_TapDelta_26() const { return ___TapDelta_26; }
	inline Vector3_t3722313464 * get_address_of_TapDelta_26() { return &___TapDelta_26; }
	inline void set_TapDelta_26(Vector3_t3722313464  value)
	{
		___TapDelta_26 = value;
	}

	inline static int32_t get_offset_of_GyroSphere_27() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroSphere_27)); }
	inline Transform_t3600365921 * get_GyroSphere_27() const { return ___GyroSphere_27; }
	inline Transform_t3600365921 ** get_address_of_GyroSphere_27() { return &___GyroSphere_27; }
	inline void set_GyroSphere_27(Transform_t3600365921 * value)
	{
		___GyroSphere_27 = value;
		Il2CppCodeGenWriteBarrier((&___GyroSphere_27), value);
	}

	inline static int32_t get_offset_of_GyroTargetDistance_28() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroTargetDistance_28)); }
	inline float get_GyroTargetDistance_28() const { return ___GyroTargetDistance_28; }
	inline float* get_address_of_GyroTargetDistance_28() { return &___GyroTargetDistance_28; }
	inline void set_GyroTargetDistance_28(float value)
	{
		___GyroTargetDistance_28 = value;
	}

	inline static int32_t get_offset_of_GyroYPosition_29() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroYPosition_29)); }
	inline float get_GyroYPosition_29() const { return ___GyroYPosition_29; }
	inline float* get_address_of_GyroYPosition_29() { return &___GyroYPosition_29; }
	inline void set_GyroYPosition_29(float value)
	{
		___GyroYPosition_29 = value;
	}

	inline static int32_t get_offset_of_GyroRotationCache_30() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroRotationCache_30)); }
	inline Quaternion_t2301928331  get_GyroRotationCache_30() const { return ___GyroRotationCache_30; }
	inline Quaternion_t2301928331 * get_address_of_GyroRotationCache_30() { return &___GyroRotationCache_30; }
	inline void set_GyroRotationCache_30(Quaternion_t2301928331  value)
	{
		___GyroRotationCache_30 = value;
	}

	inline static int32_t get_offset_of_GyroDelta_31() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___GyroDelta_31)); }
	inline Vector3_t3722313464  get_GyroDelta_31() const { return ___GyroDelta_31; }
	inline Vector3_t3722313464 * get_address_of_GyroDelta_31() { return &___GyroDelta_31; }
	inline void set_GyroDelta_31(Vector3_t3722313464  value)
	{
		___GyroDelta_31 = value;
	}

	inline static int32_t get_offset_of_DefaultFOV_32() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___DefaultFOV_32)); }
	inline float get_DefaultFOV_32() const { return ___DefaultFOV_32; }
	inline float* get_address_of_DefaultFOV_32() { return &___DefaultFOV_32; }
	inline void set_DefaultFOV_32(float value)
	{
		___DefaultFOV_32 = value;
	}

	inline static int32_t get_offset_of_Layout_33() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Layout_33)); }
	inline GameObject_t1113636619 * get_Layout_33() const { return ___Layout_33; }
	inline GameObject_t1113636619 ** get_address_of_Layout_33() { return &___Layout_33; }
	inline void set_Layout_33(GameObject_t1113636619 * value)
	{
		___Layout_33 = value;
		Il2CppCodeGenWriteBarrier((&___Layout_33), value);
	}

	inline static int32_t get_offset_of_CardboardLayout_34() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CardboardLayout_34)); }
	inline GameObject_t1113636619 * get_CardboardLayout_34() const { return ___CardboardLayout_34; }
	inline GameObject_t1113636619 ** get_address_of_CardboardLayout_34() { return &___CardboardLayout_34; }
	inline void set_CardboardLayout_34(GameObject_t1113636619 * value)
	{
		___CardboardLayout_34 = value;
		Il2CppCodeGenWriteBarrier((&___CardboardLayout_34), value);
	}

	inline static int32_t get_offset_of_VRSplash_35() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___VRSplash_35)); }
	inline GameObject_t1113636619 * get_VRSplash_35() const { return ___VRSplash_35; }
	inline GameObject_t1113636619 ** get_address_of_VRSplash_35() { return &___VRSplash_35; }
	inline void set_VRSplash_35(GameObject_t1113636619 * value)
	{
		___VRSplash_35 = value;
		Il2CppCodeGenWriteBarrier((&___VRSplash_35), value);
	}

	inline static int32_t get_offset_of_Dot_36() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Dot_36)); }
	inline GameObject_t1113636619 * get_Dot_36() const { return ___Dot_36; }
	inline GameObject_t1113636619 ** get_address_of_Dot_36() { return &___Dot_36; }
	inline void set_Dot_36(GameObject_t1113636619 * value)
	{
		___Dot_36 = value;
		Il2CppCodeGenWriteBarrier((&___Dot_36), value);
	}

	inline static int32_t get_offset_of_VRRaycaster_37() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___VRRaycaster_37)); }
	inline GameObject_t1113636619 * get_VRRaycaster_37() const { return ___VRRaycaster_37; }
	inline GameObject_t1113636619 ** get_address_of_VRRaycaster_37() { return &___VRRaycaster_37; }
	inline void set_VRRaycaster_37(GameObject_t1113636619 * value)
	{
		___VRRaycaster_37 = value;
		Il2CppCodeGenWriteBarrier((&___VRRaycaster_37), value);
	}

	inline static int32_t get_offset_of_CurrentXRDevice_38() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CurrentXRDevice_38)); }
	inline String_t* get_CurrentXRDevice_38() const { return ___CurrentXRDevice_38; }
	inline String_t** get_address_of_CurrentXRDevice_38() { return &___CurrentXRDevice_38; }
	inline void set_CurrentXRDevice_38(String_t* value)
	{
		___CurrentXRDevice_38 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentXRDevice_38), value);
	}

	inline static int32_t get_offset_of_CrosshairObject_39() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CrosshairObject_39)); }
	inline GameObject_t1113636619 * get_CrosshairObject_39() const { return ___CrosshairObject_39; }
	inline GameObject_t1113636619 ** get_address_of_CrosshairObject_39() { return &___CrosshairObject_39; }
	inline void set_CrosshairObject_39(GameObject_t1113636619 * value)
	{
		___CrosshairObject_39 = value;
		Il2CppCodeGenWriteBarrier((&___CrosshairObject_39), value);
	}

	inline static int32_t get_offset_of_CrosshairFace_40() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CrosshairFace_40)); }
	inline Image_t2670269651 * get_CrosshairFace_40() const { return ___CrosshairFace_40; }
	inline Image_t2670269651 ** get_address_of_CrosshairFace_40() { return &___CrosshairFace_40; }
	inline void set_CrosshairFace_40(Image_t2670269651 * value)
	{
		___CrosshairFace_40 = value;
		Il2CppCodeGenWriteBarrier((&___CrosshairFace_40), value);
	}

	inline static int32_t get_offset_of_CrosshairRing_41() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CrosshairRing_41)); }
	inline Image_t2670269651 * get_CrosshairRing_41() const { return ___CrosshairRing_41; }
	inline Image_t2670269651 ** get_address_of_CrosshairRing_41() { return &___CrosshairRing_41; }
	inline void set_CrosshairRing_41(Image_t2670269651 * value)
	{
		___CrosshairRing_41 = value;
		Il2CppCodeGenWriteBarrier((&___CrosshairRing_41), value);
	}

	inline static int32_t get_offset_of_CrosshairCountdownUI_42() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CrosshairCountdownUI_42)); }
	inline Text_t1901882714 * get_CrosshairCountdownUI_42() const { return ___CrosshairCountdownUI_42; }
	inline Text_t1901882714 ** get_address_of_CrosshairCountdownUI_42() { return &___CrosshairCountdownUI_42; }
	inline void set_CrosshairCountdownUI_42(Text_t1901882714 * value)
	{
		___CrosshairCountdownUI_42 = value;
		Il2CppCodeGenWriteBarrier((&___CrosshairCountdownUI_42), value);
	}

	inline static int32_t get_offset_of_ActiveColor_43() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___ActiveColor_43)); }
	inline Color_t2555686324  get_ActiveColor_43() const { return ___ActiveColor_43; }
	inline Color_t2555686324 * get_address_of_ActiveColor_43() { return &___ActiveColor_43; }
	inline void set_ActiveColor_43(Color_t2555686324  value)
	{
		___ActiveColor_43 = value;
	}

	inline static int32_t get_offset_of_InactiveColor_44() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___InactiveColor_44)); }
	inline Color_t2555686324  get_InactiveColor_44() const { return ___InactiveColor_44; }
	inline Color_t2555686324 * get_address_of_InactiveColor_44() { return &___InactiveColor_44; }
	inline void set_InactiveColor_44(Color_t2555686324  value)
	{
		___InactiveColor_44 = value;
	}

	inline static int32_t get_offset_of_CrosshairSupported_45() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CrosshairSupported_45)); }
	inline bool get_CrosshairSupported_45() const { return ___CrosshairSupported_45; }
	inline bool* get_address_of_CrosshairSupported_45() { return &___CrosshairSupported_45; }
	inline void set_CrosshairSupported_45(bool value)
	{
		___CrosshairSupported_45 = value;
	}

	inline static int32_t get_offset_of_LoadingObject_46() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LoadingObject_46)); }
	inline GameObject_t1113636619 * get_LoadingObject_46() const { return ___LoadingObject_46; }
	inline GameObject_t1113636619 ** get_address_of_LoadingObject_46() { return &___LoadingObject_46; }
	inline void set_LoadingObject_46(GameObject_t1113636619 * value)
	{
		___LoadingObject_46 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingObject_46), value);
	}

	inline static int32_t get_offset_of_LoadingFace_47() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LoadingFace_47)); }
	inline Image_t2670269651 * get_LoadingFace_47() const { return ___LoadingFace_47; }
	inline Image_t2670269651 ** get_address_of_LoadingFace_47() { return &___LoadingFace_47; }
	inline void set_LoadingFace_47(Image_t2670269651 * value)
	{
		___LoadingFace_47 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingFace_47), value);
	}

	inline static int32_t get_offset_of_LoadingProgress_48() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___LoadingProgress_48)); }
	inline Text_t1901882714 * get_LoadingProgress_48() const { return ___LoadingProgress_48; }
	inline Text_t1901882714 ** get_address_of_LoadingProgress_48() { return &___LoadingProgress_48; }
	inline void set_LoadingProgress_48(Text_t1901882714 * value)
	{
		___LoadingProgress_48 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingProgress_48), value);
	}

	inline static int32_t get_offset_of_RotationMultiplier_49() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___RotationMultiplier_49)); }
	inline float get_RotationMultiplier_49() const { return ___RotationMultiplier_49; }
	inline float* get_address_of_RotationMultiplier_49() { return &___RotationMultiplier_49; }
	inline void set_RotationMultiplier_49(float value)
	{
		___RotationMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_Tap_50() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Tap_50)); }
	inline bool get_Tap_50() const { return ___Tap_50; }
	inline bool* get_address_of_Tap_50() { return &___Tap_50; }
	inline void set_Tap_50(bool value)
	{
		___Tap_50 = value;
	}

	inline static int32_t get_offset_of_ActiveVRRoutine_51() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___ActiveVRRoutine_51)); }
	inline Coroutine_t3829159415 * get_ActiveVRRoutine_51() const { return ___ActiveVRRoutine_51; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveVRRoutine_51() { return &___ActiveVRRoutine_51; }
	inline void set_ActiveVRRoutine_51(Coroutine_t3829159415 * value)
	{
		___ActiveVRRoutine_51 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveVRRoutine_51), value);
	}

	inline static int32_t get_offset_of_Scroll_52() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Scroll_52)); }
	inline ScrollRect_t4137855814 * get_Scroll_52() const { return ___Scroll_52; }
	inline ScrollRect_t4137855814 ** get_address_of_Scroll_52() { return &___Scroll_52; }
	inline void set_Scroll_52(ScrollRect_t4137855814 * value)
	{
		___Scroll_52 = value;
		Il2CppCodeGenWriteBarrier((&___Scroll_52), value);
	}

	inline static int32_t get_offset_of_Scroll1_53() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Scroll1_53)); }
	inline ScrollRect_t4137855814 * get_Scroll1_53() const { return ___Scroll1_53; }
	inline ScrollRect_t4137855814 ** get_address_of_Scroll1_53() { return &___Scroll1_53; }
	inline void set_Scroll1_53(ScrollRect_t4137855814 * value)
	{
		___Scroll1_53 = value;
		Il2CppCodeGenWriteBarrier((&___Scroll1_53), value);
	}

	inline static int32_t get_offset_of_Scroll2_54() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___Scroll2_54)); }
	inline ScrollRect_t4137855814 * get_Scroll2_54() const { return ___Scroll2_54; }
	inline ScrollRect_t4137855814 ** get_address_of_Scroll2_54() { return &___Scroll2_54; }
	inline void set_Scroll2_54(ScrollRect_t4137855814 * value)
	{
		___Scroll2_54 = value;
		Il2CppCodeGenWriteBarrier((&___Scroll2_54), value);
	}

	inline static int32_t get_offset_of_CurrentVRState_55() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___CurrentVRState_55)); }
	inline int32_t get_CurrentVRState_55() const { return ___CurrentVRState_55; }
	inline int32_t* get_address_of_CurrentVRState_55() { return &___CurrentVRState_55; }
	inline void set_CurrentVRState_55(int32_t value)
	{
		___CurrentVRState_55 = value;
	}

	inline static int32_t get_offset_of_TargetVRState_56() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___TargetVRState_56)); }
	inline int32_t get_TargetVRState_56() const { return ___TargetVRState_56; }
	inline int32_t* get_address_of_TargetVRState_56() { return &___TargetVRState_56; }
	inline void set_TargetVRState_56(int32_t value)
	{
		___TargetVRState_56 = value;
	}

	inline static int32_t get_offset_of_ActiveCardboardRoutine_57() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___ActiveCardboardRoutine_57)); }
	inline Coroutine_t3829159415 * get_ActiveCardboardRoutine_57() const { return ___ActiveCardboardRoutine_57; }
	inline Coroutine_t3829159415 ** get_address_of_ActiveCardboardRoutine_57() { return &___ActiveCardboardRoutine_57; }
	inline void set_ActiveCardboardRoutine_57(Coroutine_t3829159415 * value)
	{
		___ActiveCardboardRoutine_57 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveCardboardRoutine_57), value);
	}

	inline static int32_t get_offset_of_vector_58() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___vector_58)); }
	inline Vector3_t3722313464  get_vector_58() const { return ___vector_58; }
	inline Vector3_t3722313464 * get_address_of_vector_58() { return &___vector_58; }
	inline void set_vector_58(Vector3_t3722313464  value)
	{
		___vector_58 = value;
	}

	inline static int32_t get_offset_of_tapCorrection_59() { return static_cast<int32_t>(offsetof(AxisRotationCamera_t2455264440, ___tapCorrection_59)); }
	inline bool get_tapCorrection_59() const { return ___tapCorrection_59; }
	inline bool* get_address_of_tapCorrection_59() { return &___tapCorrection_59; }
	inline void set_tapCorrection_59(bool value)
	{
		___tapCorrection_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISROTATIONCAMERA_T2455264440_H
#ifndef VREXPLORERCONTROLLER_T1496324211_H
#define VREXPLORERCONTROLLER_T1496324211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.VR.VRExplorerController
struct  VRExplorerController_t1496324211  : public InstancedMonoBehaviour_1_t3242198627
{
public:
	// UnityEngine.Transform BMJ.M360.VR.VRExplorerController::InteriorHolder
	Transform_t3600365921 * ___InteriorHolder_3;
	// UnityEngine.GameObject BMJ.M360.VR.VRExplorerController::LoadingProgressBarHolder
	GameObject_t1113636619 * ___LoadingProgressBarHolder_4;
	// UnityEngine.UI.Image BMJ.M360.VR.VRExplorerController::LoadingProgressBar
	Image_t2670269651 * ___LoadingProgressBar_5;
	// System.Collections.Generic.List`1<System.String> BMJ.M360.VR.VRExplorerController::InteriorRequestList
	List_1_t3319525431 * ___InteriorRequestList_6;

public:
	inline static int32_t get_offset_of_InteriorHolder_3() { return static_cast<int32_t>(offsetof(VRExplorerController_t1496324211, ___InteriorHolder_3)); }
	inline Transform_t3600365921 * get_InteriorHolder_3() const { return ___InteriorHolder_3; }
	inline Transform_t3600365921 ** get_address_of_InteriorHolder_3() { return &___InteriorHolder_3; }
	inline void set_InteriorHolder_3(Transform_t3600365921 * value)
	{
		___InteriorHolder_3 = value;
		Il2CppCodeGenWriteBarrier((&___InteriorHolder_3), value);
	}

	inline static int32_t get_offset_of_LoadingProgressBarHolder_4() { return static_cast<int32_t>(offsetof(VRExplorerController_t1496324211, ___LoadingProgressBarHolder_4)); }
	inline GameObject_t1113636619 * get_LoadingProgressBarHolder_4() const { return ___LoadingProgressBarHolder_4; }
	inline GameObject_t1113636619 ** get_address_of_LoadingProgressBarHolder_4() { return &___LoadingProgressBarHolder_4; }
	inline void set_LoadingProgressBarHolder_4(GameObject_t1113636619 * value)
	{
		___LoadingProgressBarHolder_4 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingProgressBarHolder_4), value);
	}

	inline static int32_t get_offset_of_LoadingProgressBar_5() { return static_cast<int32_t>(offsetof(VRExplorerController_t1496324211, ___LoadingProgressBar_5)); }
	inline Image_t2670269651 * get_LoadingProgressBar_5() const { return ___LoadingProgressBar_5; }
	inline Image_t2670269651 ** get_address_of_LoadingProgressBar_5() { return &___LoadingProgressBar_5; }
	inline void set_LoadingProgressBar_5(Image_t2670269651 * value)
	{
		___LoadingProgressBar_5 = value;
		Il2CppCodeGenWriteBarrier((&___LoadingProgressBar_5), value);
	}

	inline static int32_t get_offset_of_InteriorRequestList_6() { return static_cast<int32_t>(offsetof(VRExplorerController_t1496324211, ___InteriorRequestList_6)); }
	inline List_1_t3319525431 * get_InteriorRequestList_6() const { return ___InteriorRequestList_6; }
	inline List_1_t3319525431 ** get_address_of_InteriorRequestList_6() { return &___InteriorRequestList_6; }
	inline void set_InteriorRequestList_6(List_1_t3319525431 * value)
	{
		___InteriorRequestList_6 = value;
		Il2CppCodeGenWriteBarrier((&___InteriorRequestList_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VREXPLORERCONTROLLER_T1496324211_H
#ifndef PREVIEWBUTTONSMANAGER_T4268419265_H
#define PREVIEWBUTTONSMANAGER_T4268419265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.PreviewButtonsManager
struct  PreviewButtonsManager_t4268419265  : public InstancedMonoBehaviour_1_t1719326385
{
public:
	// UnityEngine.GameObject BMJ.M360.Artiz.PreviewButtonsManager::PreviewPrefab
	GameObject_t1113636619 * ___PreviewPrefab_3;
	// UnityEngine.GameObject BMJ.M360.Artiz.PreviewButtonsManager::PreviewSpacerPrefab
	GameObject_t1113636619 * ___PreviewSpacerPrefab_4;
	// UnityEngine.Transform BMJ.M360.Artiz.PreviewButtonsManager::PreviewHolder
	Transform_t3600365921 * ___PreviewHolder_5;
	// UnityEngine.RectTransform BMJ.M360.Artiz.PreviewButtonsManager::HeightReference
	RectTransform_t3704657025 * ___HeightReference_6;
	// System.Boolean BMJ.M360.Artiz.PreviewButtonsManager::InitialButtonFlag
	bool ___InitialButtonFlag_7;

public:
	inline static int32_t get_offset_of_PreviewPrefab_3() { return static_cast<int32_t>(offsetof(PreviewButtonsManager_t4268419265, ___PreviewPrefab_3)); }
	inline GameObject_t1113636619 * get_PreviewPrefab_3() const { return ___PreviewPrefab_3; }
	inline GameObject_t1113636619 ** get_address_of_PreviewPrefab_3() { return &___PreviewPrefab_3; }
	inline void set_PreviewPrefab_3(GameObject_t1113636619 * value)
	{
		___PreviewPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewPrefab_3), value);
	}

	inline static int32_t get_offset_of_PreviewSpacerPrefab_4() { return static_cast<int32_t>(offsetof(PreviewButtonsManager_t4268419265, ___PreviewSpacerPrefab_4)); }
	inline GameObject_t1113636619 * get_PreviewSpacerPrefab_4() const { return ___PreviewSpacerPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_PreviewSpacerPrefab_4() { return &___PreviewSpacerPrefab_4; }
	inline void set_PreviewSpacerPrefab_4(GameObject_t1113636619 * value)
	{
		___PreviewSpacerPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewSpacerPrefab_4), value);
	}

	inline static int32_t get_offset_of_PreviewHolder_5() { return static_cast<int32_t>(offsetof(PreviewButtonsManager_t4268419265, ___PreviewHolder_5)); }
	inline Transform_t3600365921 * get_PreviewHolder_5() const { return ___PreviewHolder_5; }
	inline Transform_t3600365921 ** get_address_of_PreviewHolder_5() { return &___PreviewHolder_5; }
	inline void set_PreviewHolder_5(Transform_t3600365921 * value)
	{
		___PreviewHolder_5 = value;
		Il2CppCodeGenWriteBarrier((&___PreviewHolder_5), value);
	}

	inline static int32_t get_offset_of_HeightReference_6() { return static_cast<int32_t>(offsetof(PreviewButtonsManager_t4268419265, ___HeightReference_6)); }
	inline RectTransform_t3704657025 * get_HeightReference_6() const { return ___HeightReference_6; }
	inline RectTransform_t3704657025 ** get_address_of_HeightReference_6() { return &___HeightReference_6; }
	inline void set_HeightReference_6(RectTransform_t3704657025 * value)
	{
		___HeightReference_6 = value;
		Il2CppCodeGenWriteBarrier((&___HeightReference_6), value);
	}

	inline static int32_t get_offset_of_InitialButtonFlag_7() { return static_cast<int32_t>(offsetof(PreviewButtonsManager_t4268419265, ___InitialButtonFlag_7)); }
	inline bool get_InitialButtonFlag_7() const { return ___InitialButtonFlag_7; }
	inline bool* get_address_of_InitialButtonFlag_7() { return &___InitialButtonFlag_7; }
	inline void set_InitialButtonFlag_7(bool value)
	{
		___InitialButtonFlag_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREVIEWBUTTONSMANAGER_T4268419265_H
#ifndef FRACTIONCONTROLLER_T2625551139_H
#define FRACTIONCONTROLLER_T2625551139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BMJ.M360.Artiz.FractionController
struct  FractionController_t2625551139  : public InstancedMonoBehaviour_1_t76458259
{
public:
	// UnityEngine.UI.Text BMJ.M360.Artiz.FractionController::TopFraction
	Text_t1901882714 * ___TopFraction_3;
	// UnityEngine.UI.Text BMJ.M360.Artiz.FractionController::BottomFraction
	Text_t1901882714 * ___BottomFraction_4;

public:
	inline static int32_t get_offset_of_TopFraction_3() { return static_cast<int32_t>(offsetof(FractionController_t2625551139, ___TopFraction_3)); }
	inline Text_t1901882714 * get_TopFraction_3() const { return ___TopFraction_3; }
	inline Text_t1901882714 ** get_address_of_TopFraction_3() { return &___TopFraction_3; }
	inline void set_TopFraction_3(Text_t1901882714 * value)
	{
		___TopFraction_3 = value;
		Il2CppCodeGenWriteBarrier((&___TopFraction_3), value);
	}

	inline static int32_t get_offset_of_BottomFraction_4() { return static_cast<int32_t>(offsetof(FractionController_t2625551139, ___BottomFraction_4)); }
	inline Text_t1901882714 * get_BottomFraction_4() const { return ___BottomFraction_4; }
	inline Text_t1901882714 ** get_address_of_BottomFraction_4() { return &___BottomFraction_4; }
	inline void set_BottomFraction_4(Text_t1901882714 * value)
	{
		___BottomFraction_4 = value;
		Il2CppCodeGenWriteBarrier((&___BottomFraction_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRACTIONCONTROLLER_T2625551139_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2100[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2106[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2112[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U3CModuleU3E_t692745562), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (CardboardUI_t365955905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[4] = 
{
	CardboardUI_t365955905::get_offset_of_LogoImage_2(),
	CardboardUI_t365955905::get_offset_of_Selection_3(),
	CardboardUI_t365955905::get_offset_of_vector_4(),
	CardboardUI_t365955905::get_offset_of_vectors_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (Controller_t2994601017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[9] = 
{
	Controller_t2994601017::get_offset_of_Splash_2(),
	Controller_t2994601017::get_offset_of_HomePage_3(),
	Controller_t2994601017::get_offset_of_LandscapeSplash_4(),
	Controller_t2994601017::get_offset_of_LandscapePanel_5(),
	Controller_t2994601017::get_offset_of_ScrollView_6(),
	Controller_t2994601017::get_offset_of_MinCache_7(),
	Controller_t2994601017::get_offset_of_MaxCache_8(),
	Controller_t2994601017::get_offset_of_Toggle_9(),
	Controller_t2994601017::get_offset_of_orientation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U3CSplashToHomeU3Ec__Iterator0_t3519462395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[5] = 
{
	U3CSplashToHomeU3Ec__Iterator0_t3519462395::get_offset_of_time_0(),
	U3CSplashToHomeU3Ec__Iterator0_t3519462395::get_offset_of_U24this_1(),
	U3CSplashToHomeU3Ec__Iterator0_t3519462395::get_offset_of_U24current_2(),
	U3CSplashToHomeU3Ec__Iterator0_t3519462395::get_offset_of_U24disposing_3(),
	U3CSplashToHomeU3Ec__Iterator0_t3519462395::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (ScrollSnapRect_t3278334712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[36] = 
{
	ScrollSnapRect_t3278334712::get_offset_of_m_PaginationChildren_2(),
	ScrollSnapRect_t3278334712::get_offset_of_startingPage_3(),
	ScrollSnapRect_t3278334712::get_offset_of_fastSwipeThresholdTime_4(),
	ScrollSnapRect_t3278334712::get_offset_of_fastSwipeThresholdDistance_5(),
	ScrollSnapRect_t3278334712::get_offset_of_decelerationRate_6(),
	ScrollSnapRect_t3278334712::get_offset_of_prevButton_7(),
	ScrollSnapRect_t3278334712::get_offset_of_nextButton_8(),
	ScrollSnapRect_t3278334712::get_offset_of_unselectedPage_9(),
	ScrollSnapRect_t3278334712::get_offset_of_selectedPage_10(),
	ScrollSnapRect_t3278334712::get_offset_of_pageSelectionIcons_11(),
	ScrollSnapRect_t3278334712::get_offset_of__fastSwipeThresholdMaxLimit_12(),
	ScrollSnapRect_t3278334712::get_offset_of__scrollRectComponent_13(),
	ScrollSnapRect_t3278334712::get_offset_of__scrollRectRect_14(),
	ScrollSnapRect_t3278334712::get_offset_of__container_15(),
	ScrollSnapRect_t3278334712::get_offset_of__horizontal_16(),
	ScrollSnapRect_t3278334712::get_offset_of__pageCount_17(),
	ScrollSnapRect_t3278334712::get_offset_of__currentPage_18(),
	ScrollSnapRect_t3278334712::get_offset_of__lerp_19(),
	ScrollSnapRect_t3278334712::get_offset_of__lerpTo_20(),
	ScrollSnapRect_t3278334712::get_offset_of__pagePositions_21(),
	ScrollSnapRect_t3278334712::get_offset_of__dragging_22(),
	ScrollSnapRect_t3278334712::get_offset_of__timeStamp_23(),
	ScrollSnapRect_t3278334712::get_offset_of__startPosition_24(),
	ScrollSnapRect_t3278334712::get_offset_of__showPageSelection_25(),
	ScrollSnapRect_t3278334712::get_offset_of__previousPageSelectionIndex_26(),
	ScrollSnapRect_t3278334712::get_offset_of__pageSelectionImages_27(),
	ScrollSnapRect_t3278334712::get_offset_of_panel_28(),
	ScrollSnapRect_t3278334712::get_offset_of_panel1_29(),
	ScrollSnapRect_t3278334712::get_offset_of_panel2_30(),
	ScrollSnapRect_t3278334712::get_offset_of_bgPanel_31(),
	ScrollSnapRect_t3278334712::get_offset_of_bgPanel1_32(),
	ScrollSnapRect_t3278334712::get_offset_of_bgPanel2_33(),
	ScrollSnapRect_t3278334712::get_offset_of_currentScale_34(),
	ScrollSnapRect_t3278334712::get_offset_of_othersScale_35(),
	ScrollSnapRect_t3278334712::get_offset_of_MinCache_36(),
	ScrollSnapRect_t3278334712::get_offset_of_MaxCache_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (SnapScrollRect_t3165954182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[5] = 
{
	SnapScrollRect_t3165954182::get_offset_of_Step_2(),
	SnapScrollRect_t3165954182::get_offset_of_AttachedScrollRect_3(),
	SnapScrollRect_t3165954182::get_offset_of_ContentAmount_4(),
	SnapScrollRect_t3165954182::get_offset_of_TargetNormalizedPosition_5(),
	SnapScrollRect_t3165954182::get_offset_of_ActiveUpdateRoutine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (U3CUpdateRoutineU3Ec__Iterator0_t2386763820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[4] = 
{
	U3CUpdateRoutineU3Ec__Iterator0_t2386763820::get_offset_of_U24this_0(),
	U3CUpdateRoutineU3Ec__Iterator0_t2386763820::get_offset_of_U24current_1(),
	U3CUpdateRoutineU3Ec__Iterator0_t2386763820::get_offset_of_U24disposing_2(),
	U3CUpdateRoutineU3Ec__Iterator0_t2386763820::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (VRRaycaster_t2465929147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[9] = 
{
	VRRaycaster_t2465929147::get_offset_of_m_Raycaster_2(),
	VRRaycaster_t2465929147::get_offset_of_m_PointerEventData_3(),
	VRRaycaster_t2465929147::get_offset_of_m_EventSystem_4(),
	VRRaycaster_t2465929147::get_offset_of_timestamp_5(),
	VRRaycaster_t2465929147::get_offset_of_buttonhit_6(),
	VRRaycaster_t2465929147::get_offset_of_lastHitObject_7(),
	VRRaycaster_t2465929147::get_offset_of_countdownImage_8(),
	VRRaycaster_t2465929147::get_offset_of_waitTime_9(),
	VRRaycaster_t2465929147::get_offset_of_clear_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (ArtizPreviewButton_t1230103917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[15] = 
{
	ArtizPreviewButton_t1230103917::get_offset_of_DisplayNameText_2(),
	ArtizPreviewButton_t1230103917::get_offset_of_DescriptionText_3(),
	ArtizPreviewButton_t1230103917::get_offset_of_PreviewImage_4(),
	ArtizPreviewButton_t1230103917::get_offset_of_Hourglass_5(),
	ArtizPreviewButton_t1230103917::get_offset_of_PreviewHolder_6(),
	ArtizPreviewButton_t1230103917::get_offset_of_LoadingBar_7(),
	ArtizPreviewButton_t1230103917::get_offset_of_UniqueID_8(),
	ArtizPreviewButton_t1230103917::get_offset_of_SpriteThumbnailLink_9(),
	ArtizPreviewButton_t1230103917::get_offset_of_SpriteHDRILink_10(),
	ArtizPreviewButton_t1230103917::get_offset_of_CategoryIndex_11(),
	ArtizPreviewButton_t1230103917::get_offset_of_StartIndex_12(),
	ArtizPreviewButton_t1230103917::get_offset_of_HDRIRotationOffset_13(),
	ArtizPreviewButton_t1230103917::get_offset_of_ActiveRoutine_14(),
	ArtizPreviewButton_t1230103917::get_offset_of_OnClickAction_15(),
	ArtizPreviewButton_t1230103917::get_offset_of_InitialButtonFlag_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[5] = 
{
	U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164::get_offset_of_U3CThumbnailRequestU3E__0_0(),
	U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164::get_offset_of_U24this_1(),
	U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164::get_offset_of_U24current_2(),
	U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164::get_offset_of_U24disposing_3(),
	U3CLoadThumbnailRoutineU3Ec__Iterator0_t1787701164::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (ArtizPreviewProperty_t1496634109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[8] = 
{
	ArtizPreviewProperty_t1496634109::get_offset_of_UniqueID_0(),
	ArtizPreviewProperty_t1496634109::get_offset_of_DisplayName_1(),
	ArtizPreviewProperty_t1496634109::get_offset_of_Description_2(),
	ArtizPreviewProperty_t1496634109::get_offset_of_SpriteThumbnailLink_3(),
	ArtizPreviewProperty_t1496634109::get_offset_of_StartIndex_4(),
	ArtizPreviewProperty_t1496634109::get_offset_of_SpriteHDRILink_5(),
	ArtizPreviewProperty_t1496634109::get_offset_of_CategoryIndex_6(),
	ArtizPreviewProperty_t1496634109::get_offset_of_HDRIRotationOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (AssetsManager_t3069477283), -1, sizeof(AssetsManager_t3069477283_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[13] = 
{
	AssetsManager_t3069477283::get_offset_of_SplashCanvas_3(),
	AssetsManager_t3069477283::get_offset_of_HomePage_4(),
	AssetsManager_t3069477283::get_offset_of_EditorClearCache_5(),
	AssetsManager_t3069477283::get_offset_of_PreviewPropertyList_6(),
	AssetsManager_t3069477283::get_offset_of_SerializablePreviewPropertiesObject_7(),
	AssetsManager_t3069477283::get_offset_of_ActiveRoutine_8(),
	AssetsManager_t3069477283::get_offset_of_ActiveRoutine2_9(),
	AssetsManager_t3069477283::get_offset_of_ConfigurationRequestURL_10(),
	AssetsManager_t3069477283::get_offset_of_AssetsVersion_11(),
	AssetsManager_t3069477283_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	AssetsManager_t3069477283_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_13(),
	AssetsManager_t3069477283_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_14(),
	AssetsManager_t3069477283_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (SerializablePreviewProperties_t119401764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[2] = 
{
	SerializablePreviewProperties_t119401764::get_offset_of_Version_0(),
	SerializablePreviewProperties_t119401764::get_offset_of_PreviewPropertyList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[4] = 
{
	U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762::get_offset_of_U3CTimestampU3E__0_0(),
	U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762::get_offset_of_U24current_1(),
	U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762::get_offset_of_U24disposing_2(),
	U3CDebugUpdateRoutineU3Ec__Iterator0_t3894649762::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[7] = 
{
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_U3CGetAssetConfigurationRequestU3E__0_0(),
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_PreloadButtonAction_1(),
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_LoadPreviewAssetsAction_2(),
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_U24this_3(),
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_U24current_4(),
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_U24disposing_5(),
	U3CLoadPreviewButtonsRoutineU3Ec__Iterator1_t1217096687::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (CustomFitScroller_t3272853734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[6] = 
{
	CustomFitScroller_t3272853734::get_offset_of_TargetRectTransform_2(),
	CustomFitScroller_t3272853734::get_offset_of_StartRectTransform_3(),
	CustomFitScroller_t3272853734::get_offset_of_EndRectTransform_4(),
	CustomFitScroller_t3272853734::get_offset_of_TargetAnchor_5(),
	CustomFitScroller_t3272853734::get_offset_of_Step_6(),
	CustomFitScroller_t3272853734::get_offset_of_Initial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (U3CUpdateRoutineU3Ec__Iterator0_t3289854625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[4] = 
{
	U3CUpdateRoutineU3Ec__Iterator0_t3289854625::get_offset_of_U24this_0(),
	U3CUpdateRoutineU3Ec__Iterator0_t3289854625::get_offset_of_U24current_1(),
	U3CUpdateRoutineU3Ec__Iterator0_t3289854625::get_offset_of_U24disposing_2(),
	U3CUpdateRoutineU3Ec__Iterator0_t3289854625::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (Debugger_t3226592815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (FractionController_t2625551139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[2] = 
{
	FractionController_t2625551139::get_offset_of_TopFraction_3(),
	FractionController_t2625551139::get_offset_of_BottomFraction_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (HDRIRotator_t2414495732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[6] = 
{
	HDRIRotator_t2414495732::get_offset_of_PrimarySphere_2(),
	HDRIRotator_t2414495732::get_offset_of_FinalTargetPoint_3(),
	HDRIRotator_t2414495732::get_offset_of_CurrentTargetPoint_4(),
	HDRIRotator_t2414495732::get_offset_of_RotationRate_5(),
	HDRIRotator_t2414495732::get_offset_of_MovementVector_6(),
	HDRIRotator_t2414495732::get_offset_of_StartPoint_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (U3CUpdateRoutineU3Ec__Iterator0_t1920255547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[4] = 
{
	U3CUpdateRoutineU3Ec__Iterator0_t1920255547::get_offset_of_U24this_0(),
	U3CUpdateRoutineU3Ec__Iterator0_t1920255547::get_offset_of_U24current_1(),
	U3CUpdateRoutineU3Ec__Iterator0_t1920255547::get_offset_of_U24disposing_2(),
	U3CUpdateRoutineU3Ec__Iterator0_t1920255547::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (U3CRotateCameraU3Ec__Iterator1_t31197773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[4] = 
{
	U3CRotateCameraU3Ec__Iterator1_t31197773::get_offset_of_U3CRotateU3E__0_0(),
	U3CRotateCameraU3Ec__Iterator1_t31197773::get_offset_of_U24current_1(),
	U3CRotateCameraU3Ec__Iterator1_t31197773::get_offset_of_U24disposing_2(),
	U3CRotateCameraU3Ec__Iterator1_t31197773::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (InfoPanelController_t539477300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[6] = 
{
	InfoPanelController_t539477300::get_offset_of_InfoPanelObject_2(),
	InfoPanelController_t539477300::get_offset_of_PreviewImageObject_3(),
	InfoPanelController_t539477300::get_offset_of_PreviewImage_4(),
	InfoPanelController_t539477300::get_offset_of_FacebookPageLink_5(),
	InfoPanelController_t539477300::get_offset_of_WebsitePageLink_6(),
	InfoPanelController_t539477300::get_offset_of_InstagramPageLink_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (U3CSetupRoutineU3Ec__Iterator0_t1251959289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[6] = 
{
	U3CSetupRoutineU3Ec__Iterator0_t1251959289::get_offset_of_U24locvar0_0(),
	U3CSetupRoutineU3Ec__Iterator0_t1251959289::get_offset_of_U24locvar1_1(),
	U3CSetupRoutineU3Ec__Iterator0_t1251959289::get_offset_of_U24this_2(),
	U3CSetupRoutineU3Ec__Iterator0_t1251959289::get_offset_of_U24current_3(),
	U3CSetupRoutineU3Ec__Iterator0_t1251959289::get_offset_of_U24disposing_4(),
	U3CSetupRoutineU3Ec__Iterator0_t1251959289::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (LayoutContentSizeFitter_t3108834706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[2] = 
{
	LayoutContentSizeFitter_t3108834706::get_offset_of_ChildCount_2(),
	LayoutContentSizeFitter_t3108834706::get_offset_of_AttachedLayoutElement_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (LayoutVertical_t1167249937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[8] = 
{
	LayoutVertical_t1167249937::get_offset_of_Spacing_2(),
	LayoutVertical_t1167249937::get_offset_of_SourceTransform_3(),
	LayoutVertical_t1167249937::get_offset_of_TargetTransform_4(),
	LayoutVertical_t1167249937::get_offset_of_LocalPosition_5(),
	LayoutVertical_t1167249937::get_offset_of_SetupComplete_6(),
	LayoutVertical_t1167249937::get_offset_of_ChildTransform_7(),
	LayoutVertical_t1167249937::get_offset_of_ChildLayoutVertical_8(),
	LayoutVertical_t1167249937::get_offset_of_DestroyFlag_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (NavigatableHDRI_t1695953284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[7] = 
{
	NavigatableHDRI_t1695953284::get_offset_of_HDRISpheres_2(),
	NavigatableHDRI_t1695953284::get_offset_of_SpeedModifier_3(),
	NavigatableHDRI_t1695953284::get_offset_of_StartIndex_4(),
	NavigatableHDRI_t1695953284::get_offset_of_ActiveArrowTexture_5(),
	NavigatableHDRI_t1695953284::get_offset_of_InactiveArrowTexture_6(),
	NavigatableHDRI_t1695953284::get_offset_of_CurrentIndex_7(),
	NavigatableHDRI_t1695953284::get_offset_of_TargetIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (U3CUpdateRoutineU3Ec__Iterator0_t1392784015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[10] = 
{
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U3CNavigationButtonHitU3E__0_0(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U3CCountdownTimestampU3E__0_1(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U3CCurrentTimestampU3E__0_2(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U3CHitNavigationButtonU3E__0_3(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U3CRaycastU3E__1_4(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U3CHitInfoU3E__1_5(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U24this_6(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U24current_7(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U24disposing_8(),
	U3CUpdateRoutineU3Ec__Iterator0_t1392784015::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (NavigationButton_t3491644213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[3] = 
{
	NavigationButton_t3491644213::get_offset_of_TargetHDRI_2(),
	NavigationButton_t3491644213::get_offset_of_TargetIndex_3(),
	NavigationButton_t3491644213::get_offset_of_AttachedMeshRenderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (PreviewButtonsManager_t4268419265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[5] = 
{
	PreviewButtonsManager_t4268419265::get_offset_of_PreviewPrefab_3(),
	PreviewButtonsManager_t4268419265::get_offset_of_PreviewSpacerPrefab_4(),
	PreviewButtonsManager_t4268419265::get_offset_of_PreviewHolder_5(),
	PreviewButtonsManager_t4268419265::get_offset_of_HeightReference_6(),
	PreviewButtonsManager_t4268419265::get_offset_of_InitialButtonFlag_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (PreviewButtonsManager2_t2759123039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[5] = 
{
	PreviewButtonsManager2_t2759123039::get_offset_of_PreviewPrefab_3(),
	PreviewButtonsManager2_t2759123039::get_offset_of_PreviewSpacerPrefab_4(),
	PreviewButtonsManager2_t2759123039::get_offset_of_PreviewHolder_5(),
	PreviewButtonsManager2_t2759123039::get_offset_of_HeightReference_6(),
	PreviewButtonsManager2_t2759123039::get_offset_of_InitialButtonFlag_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (PreviewManager_t2255819439), -1, sizeof(PreviewManager_t2255819439_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2151[10] = 
{
	PreviewManager_t2255819439::get_offset_of_HDRIContentHolder_3(),
	PreviewManager_t2255819439::get_offset_of_HDRIPrefab_4(),
	PreviewManager_t2255819439::get_offset_of_LoadingHDRIPrefab_5(),
	PreviewManager_t2255819439::get_offset_of_HDRIMaterial_6(),
	PreviewManager_t2255819439::get_offset_of_CubemapShader_7(),
	PreviewManager_t2255819439::get_offset_of_UnlitTransparentShader_8(),
	PreviewManager_t2255819439::get_offset_of_InitialPreview_9(),
	PreviewManager_t2255819439_StaticFields::get_offset_of_CustomStartIndex_10(),
	PreviewManager_t2255819439::get_offset_of_HDRIReference_11(),
	PreviewManager_t2255819439::get_offset_of_LoadingReferenceTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (U3CLoadHDRIU3Ec__Iterator0_t21638791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[9] = 
{
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_SpriteHDRILink_0(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_U3CHDRIRequestU3E__0_1(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_HDRIRotationOffset_2(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_PreviewButton_3(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_StartIndex_4(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_U24this_5(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_U24current_6(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_U24disposing_7(),
	U3CLoadHDRIU3Ec__Iterator0_t21638791::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (RegularHDRI_t2998726886), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (ScrollingController_t1982690304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[1] = 
{
	ScrollingController_t1982690304::get_offset_of_CategorySelectors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (CategorySelector_t2068481753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[3] = 
{
	CategorySelector_t2068481753::get_offset_of_OnButton_0(),
	CategorySelector_t2068481753::get_offset_of_OffButton_1(),
	CategorySelector_t2068481753::get_offset_of_Scroller_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (ScrollingController2_t2836624384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	ScrollingController2_t2836624384::get_offset_of_CategorySelectors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (CategorySelector_t478322649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[3] = 
{
	CategorySelector_t478322649::get_offset_of_OnButton_0(),
	CategorySelector_t478322649::get_offset_of_OffButton_1(),
	CategorySelector_t478322649::get_offset_of_Scroller_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (SnappingScrollRect_t125492878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[5] = 
{
	SnappingScrollRect_t125492878::get_offset_of_Step_2(),
	SnappingScrollRect_t125492878::get_offset_of_AttachedScrollRect_3(),
	SnappingScrollRect_t125492878::get_offset_of_ContentAmount_4(),
	SnappingScrollRect_t125492878::get_offset_of_TargetNormalizedPosition_5(),
	SnappingScrollRect_t125492878::get_offset_of_ActiveUpdateRoutine_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (U3CUpdateRoutineU3Ec__Iterator0_t4291033512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[4] = 
{
	U3CUpdateRoutineU3Ec__Iterator0_t4291033512::get_offset_of_U24this_0(),
	U3CUpdateRoutineU3Ec__Iterator0_t4291033512::get_offset_of_U24current_1(),
	U3CUpdateRoutineU3Ec__Iterator0_t4291033512::get_offset_of_U24disposing_2(),
	U3CUpdateRoutineU3Ec__Iterator0_t4291033512::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (ToggleViewController_t44480253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[5] = 
{
	ToggleViewController_t44480253::get_offset_of_AttachedCustomFitScroller_3(),
	ToggleViewController_t44480253::get_offset_of_ToggleDownButtonObject_4(),
	ToggleViewController_t44480253::get_offset_of_ToggleUpButton_5(),
	ToggleViewController_t44480253::get_offset_of_VRModeButton_6(),
	ToggleViewController_t44480253::get_offset_of_AttachedAxisRotationCamera_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (AxisRotationCamera_t2455264440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[57] = 
{
	AxisRotationCamera_t2455264440::get_offset_of_MobileRotationMultiplier_3(),
	AxisRotationCamera_t2455264440::get_offset_of_MainCamera_4(),
	AxisRotationCamera_t2455264440::get_offset_of_AttachedCamera_5(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroSupportErrorBox_6(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroSupport_7(),
	AxisRotationCamera_t2455264440::get_offset_of_InCategory_8(),
	AxisRotationCamera_t2455264440::get_offset_of_HorizontalSpeed_9(),
	AxisRotationCamera_t2455264440::get_offset_of_VerticalSpeed_10(),
	AxisRotationCamera_t2455264440::get_offset_of_CameraYaw_11(),
	AxisRotationCamera_t2455264440::get_offset_of_CameraPitch_12(),
	AxisRotationCamera_t2455264440::get_offset_of_YawClamp_13(),
	AxisRotationCamera_t2455264440::get_offset_of_YClamp_14(),
	AxisRotationCamera_t2455264440::get_offset_of_Marker_15(),
	AxisRotationCamera_t2455264440::get_offset_of_CameraTransform_16(),
	AxisRotationCamera_t2455264440::get_offset_of_LookSphere_17(),
	AxisRotationCamera_t2455264440::get_offset_of_LookTarget_18(),
	AxisRotationCamera_t2455264440::get_offset_of_LookTargetYCache_19(),
	AxisRotationCamera_t2455264440::get_offset_of_LookTargetDistance_20(),
	AxisRotationCamera_t2455264440::get_offset_of_LookTargetPoint_21(),
	AxisRotationCamera_t2455264440::get_offset_of_TapSphere_22(),
	AxisRotationCamera_t2455264440::get_offset_of_TapTargetDistance_23(),
	AxisRotationCamera_t2455264440::get_offset_of_TapYPosition_24(),
	AxisRotationCamera_t2455264440::get_offset_of_TapRotationCache_25(),
	AxisRotationCamera_t2455264440::get_offset_of_TapDelta_26(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroSphere_27(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroTargetDistance_28(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroYPosition_29(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroRotationCache_30(),
	AxisRotationCamera_t2455264440::get_offset_of_GyroDelta_31(),
	AxisRotationCamera_t2455264440::get_offset_of_DefaultFOV_32(),
	AxisRotationCamera_t2455264440::get_offset_of_Layout_33(),
	AxisRotationCamera_t2455264440::get_offset_of_CardboardLayout_34(),
	AxisRotationCamera_t2455264440::get_offset_of_VRSplash_35(),
	AxisRotationCamera_t2455264440::get_offset_of_Dot_36(),
	AxisRotationCamera_t2455264440::get_offset_of_VRRaycaster_37(),
	AxisRotationCamera_t2455264440::get_offset_of_CurrentXRDevice_38(),
	AxisRotationCamera_t2455264440::get_offset_of_CrosshairObject_39(),
	AxisRotationCamera_t2455264440::get_offset_of_CrosshairFace_40(),
	AxisRotationCamera_t2455264440::get_offset_of_CrosshairRing_41(),
	AxisRotationCamera_t2455264440::get_offset_of_CrosshairCountdownUI_42(),
	AxisRotationCamera_t2455264440::get_offset_of_ActiveColor_43(),
	AxisRotationCamera_t2455264440::get_offset_of_InactiveColor_44(),
	AxisRotationCamera_t2455264440::get_offset_of_CrosshairSupported_45(),
	AxisRotationCamera_t2455264440::get_offset_of_LoadingObject_46(),
	AxisRotationCamera_t2455264440::get_offset_of_LoadingFace_47(),
	AxisRotationCamera_t2455264440::get_offset_of_LoadingProgress_48(),
	AxisRotationCamera_t2455264440::get_offset_of_RotationMultiplier_49(),
	AxisRotationCamera_t2455264440::get_offset_of_Tap_50(),
	AxisRotationCamera_t2455264440::get_offset_of_ActiveVRRoutine_51(),
	AxisRotationCamera_t2455264440::get_offset_of_Scroll_52(),
	AxisRotationCamera_t2455264440::get_offset_of_Scroll1_53(),
	AxisRotationCamera_t2455264440::get_offset_of_Scroll2_54(),
	AxisRotationCamera_t2455264440::get_offset_of_CurrentVRState_55(),
	AxisRotationCamera_t2455264440::get_offset_of_TargetVRState_56(),
	AxisRotationCamera_t2455264440::get_offset_of_ActiveCardboardRoutine_57(),
	AxisRotationCamera_t2455264440::get_offset_of_vector_58(),
	AxisRotationCamera_t2455264440::get_offset_of_tapCorrection_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (VRState_t4021279362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2162[4] = 
{
	VRState_t4021279362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[6] = 
{
	U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632::get_offset_of_U24locvar0_0(),
	U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632::get_offset_of_U24locvar1_1(),
	U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632::get_offset_of_U24this_2(),
	U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632::get_offset_of_U24current_3(),
	U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632::get_offset_of_U24disposing_4(),
	U3CMonitorVRStatusRoutineU3Ec__Iterator0_t1319999632::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (U3CVRRoutineU3Ec__Iterator1_t2077243264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[9] = 
{
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U3CVRRayCastU3E__0_0(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U3CtimestampU3E__0_1(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U3CbuttonhitU3E__0_2(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U3ClastHitObjectU3E__0_3(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U3CraycastHitU3E__1_4(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U24this_5(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U24current_6(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U24disposing_7(),
	U3CVRRoutineU3Ec__Iterator1_t2077243264::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (ReceiveResult_t2215153068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	ReceiveResult_t2215153068::get_offset_of_SpeechRecognizedText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (Test_t650638817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	Test_t650638817::get_offset_of_btn_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (VRExplorerController_t1496324211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[4] = 
{
	VRExplorerController_t1496324211::get_offset_of_InteriorHolder_3(),
	VRExplorerController_t1496324211::get_offset_of_LoadingProgressBarHolder_4(),
	VRExplorerController_t1496324211::get_offset_of_LoadingProgressBar_5(),
	VRExplorerController_t1496324211::get_offset_of_InteriorRequestList_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[7] = 
{
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_InteriorLink_0(),
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_U3CLoadInteriorRequestU3E__0_1(),
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_U3CTargetNameU3E__0_2(),
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_U24this_3(),
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_U24current_4(),
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_U24disposing_5(),
	U3CLoadInteriorRoutineU3Ec__Iterator0_t1972925413::get_offset_of_U24PC_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
